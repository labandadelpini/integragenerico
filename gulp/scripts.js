'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var browserSync = require('browser-sync');

var $ = require('gulp-load-plugins')();

gulp.task('scripts', function () {
	var filtro = $.filter(['app/components/devoops/devoops.js'],{restore:true});
  return gulp.src(path.join(conf.paths.src, '/app/**/*.js'))
    .pipe(filtro)
    .pipe($.eslint())
    .pipe(filtro.restore)
    .pipe($.eslint.format())
    .pipe(browserSync.reload({ stream: true }))
    .pipe($.size())
});
