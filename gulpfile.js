/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */

'use strict';

var gulp = require('gulp');
var wrench = require('wrench');
var plugins = require('gulp-load-plugins')();

/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
wrench.readdirSyncRecursive('./gulp').filter(function(file) {
  return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
  require('./gulp/' + file);
});


/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', ['dist']);


 
gulp.task('templateCache', function () {
  return gulp.src('src/app/**/*.html')
    .pipe(plugins.angularTemplatecache('templates.js',{root: 'integragenerico', module:'integragenerico'}))
    .pipe(gulp.dest('src/app'));
});

gulp.task('sass', function () {
  return gulp.src([
    'src/app/**/*.scss',
    '!src/app/components/devoops/**/*.scss',
    '!src/app/components/xeditable.scss'])
    .pipe(plugins.sass())
    .pipe(plugins.concatCss('integra-generico.css'))
    .pipe(gulp.dest('dist'))
    .pipe(plugins.minifyCss())
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest('dist'));
});

gulp.task('dist',['templateCache','sass'],function() {
  return gulp.src(['src/app/index.module.js','src/app/**/*.js'])
	.pipe(plugins.concat('integra-generico.js'))
    .pipe(gulp.dest('dist'))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(plugins.uglify())
    .pipe(gulp.dest('dist'))
    .pipe(plugins.notify({ message: 'Scripts task complete' }));
} );



