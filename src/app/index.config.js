(function () {
  'use strict';

  angular
    .module('integragenerico')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, $localStorageProvider, $sessionStorageProvider, $resourceProvider, $translateProvider, formlyConfigProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    function _defineProperty(obj, key, value) {
      if (key in obj) {
        Object.defineProperty(obj, key, {
          value: value,
          enumerable: true,
          configurable: true,
          writable: true
        });
      } else {
        obj[key] = value;
      }
      return obj;
    }

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;

    $localStorageProvider.setKeyPrefix('integra.');
    $sessionStorageProvider.setKeyPrefix('integra.');
    angular.extend($resourceProvider.defaults.actions, {
      update: {
        method: 'PUT'
      }
    });
    $translateProvider.useStaticFilesLoader({
      prefix: 'assets/translate/locale-',
      suffix: '.json'
    });
    $translateProvider.preferredLanguage('es');

    formlyConfigProvider.setType({
      name: 'typeahead',
      templateUrl: 'integragenerico/components/formly/typeahead.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'static',
      templateUrl: 'integragenerico/components/formly/texto-estatico/texto-estatico.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'tags',
      templateUrl: 'integragenerico/components/formly/etiquetas.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'tagsTexto',
      templateUrl: 'integragenerico/components/formly/etiquetas-texto.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'colorpicker',
      templateUrl: 'integragenerico/components/formly/color-picker.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'addon',
      templateUrl: 'integragenerico/components/formly/addon/addon.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    var controladorDatePicker = function ($scope, $parse) {
      $scope.crearMomento = function () {
        return new moment;
      };

      $scope.minimo = $parse($scope.to.minimo)($scope);
      $scope.maximo = $parse($scope.to.maximo)($scope);

      $scope.datepicker = {};
      $scope.datepicker.opened = false;
      $scope.datepicker.open = function ($event) {
        $scope.datepicker.opened = !$scope.datepicker.opened;
      };
    };

    controladorDatePicker.$inject = ['$scope', '$parse'];

    formlyConfigProvider.setType({
      name: 'datepicker',
      templateUrl: 'integragenerico/components/formly/datepicker.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
      controller: controladorDatePicker
    });

    formlyConfigProvider.setType({
      name: 'horizontalDatepicker',
      extends: 'datepicker',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalStatic',
      extends: 'static',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalAddon',
      extends: 'addon',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalColorPicker',
      extends: 'colorpicker',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setWrapper({
      name: 'horizontalBootstrapLabel',
      template: [
        '<label for="{{::id}}" class="{{to.anchoLabel ? to.anchoLabel : \'col-sm-2\'}} control-label">',
        '{{to.label}} {{to.required ? "*" : ""}}',
        '</label>',
        '<div class="{{to.ancho ? to.ancho : \'col-sm-10\'}}"> ',
        '<formly-transclude></formly-transclude>',
        '</div>'
      ].join(' ')
    });

    formlyConfigProvider.setType({
      name: 'file',
      templateUrl: 'integragenerico/components/formly/file-uploader.html',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'horizontalInput',
      extends: 'input',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalRadio',
      extends: 'radio',
      templateUrl: 'integragenerico/components/formly/radio-button.html',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalTextarea',
      extends: 'textarea',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalSelect',
      extends: 'select',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      defaultOptions: function defaultOptions(options) {
        var ngOptions = options.templateOptions.ngOptions || 'option[to.valueProp || \'value\'] as option[to.labelProp || \'name\'] group by option[to.groupProp || \'group\'] for option in to.options';
        return {
          ngModelAttrs: _defineProperty({}, ngOptions, {
            value: options.templateOptions.optionsAttr || 'ng-options'
          })
        };
      }
    });

    formlyConfigProvider.setType({
      name: 'horizontalTypeahead',
      extends: 'typeahead',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalTags',
      extends: 'tags',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalTagsTexto',
      extends: 'tagsTexto',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'momentPicker',
      extends: 'input',
      defaultOptions: {
        templateOptions: {
          momentPicker: true
        },
        ngModelAttrs: {
          momentPicker: {
            bound: 'moment-picker',
            attribute: 'moment-picker'
          }
        }
      }
    });

    formlyConfigProvider.setType({
      name: 'horizontalMomentPicker',
      extends: 'momentPicker',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType([{
      name: 'telefono',
      template: '<input-telefono ng-model="model[options.key]" options="options" ng-class="{\'has-error\': showError}"></input-telefono>',
      wrapper: ['bootstrapLabel', 'bootstrapHasError']
    }, {
      name: 'horizontalTelefono',
      extends: 'telefono',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    }, {
      name: 'conjuntoTelefonos',
      template: '<conjunto-telefonos ng-model="model[options.key]" options="options"></conjunto-telefonos>',
      wrapper: ['bootstrapLabel', 'bootstrapHasError']
    }, {
      name: 'horizontalConjuntoTelefonos',
      extends: 'conjuntoTelefonos',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    }]);

    formlyConfigProvider.setType({
      name: 'timepicker',
      templateUrl: 'integragenerico/components/formly/timepicker/timepicker.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalTimepicker',
      extends: 'timepicker',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'direccion',
      template: '<input-direccion ng-model="model[options.key]" options="options" ng-class="{\'has-error\': showError}"></input-direccion>',
      wrapper: ['bootstrapHasError']
    });
    formlyConfigProvider.setType({
      name: 'direccionNueva',
      template: '<input-direccion-nueva ng-model="model[options.key]" options="options" ng-class="{\'has-error\': showError}"></input-direccion-nueva>',
      wrapper: ['bootstrapHasError']
    });
    formlyConfigProvider.setType({
      name: 'lugarGeografico',
      template: '<input-lugar-geografico ng-model="model[options.key]" options="options" ng-class="{\'has-error\': showError}"></input-lugar-geografico>',
      wrapper: ['bootstrapHasError']
    });

    formlyConfigProvider.setWrapper([{
      templateUrl: 'integragenerico/components/formly/mensajes-error.html'
    }]);

    var controladorObjetoSeleccion = function ($scope, $parse, ServicioTablaConsulta) {
      $scope.abrirDialogoSeleccion = function () {
        ServicioTablaConsulta.abrirDialogoSeleccionar($scope.to.claseObjeto, function (objeto) {
          $parse($scope.options.key).assign($scope.model, objeto);
        }, function (cancelado) {

        }, $scope.to.funcionFiltro);
      };

      $scope.eliminarSeleccion = function () {
        $parse($scope.options.key).assign($scope.model, undefined);
      }

      $scope.getValor = function () {
        return $parse($scope.to.propiedadObjeto)($parse($scope.options.key)($scope.model));
      };
    };

    controladorObjetoSeleccion.$inject = ['$scope', '$parse', 'ServicioTablaConsulta'];

    formlyConfigProvider.setType({
      name: 'objetoSeleccion',
      templateUrl: 'integragenerico/components/formly/objeto-seleccion/objeto-seleccion.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
      controller: controladorObjetoSeleccion
    });

    formlyConfigProvider.setType({
      name: 'horizontalObjetoSeleccion',
      extends: 'objetoSeleccion',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    var controladorShowPassword = function ($scope, $parse) {
      $scope.tipoInput = 'password';
      $scope.cambiarTipo = function() {
        $scope.muestro = !$scope.muestro;
      };
      $scope.muestro = false;
    };

    controladorShowPassword.$inject = ['$scope', '$parse'];

    formlyConfigProvider.setType({
      name: 'showPasswordInput',
      templateUrl: 'integragenerico/components/formly/show-password/show-password.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
      controller: controladorShowPassword
    });

  }


})();
