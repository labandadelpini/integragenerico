(function() {
	'use strict';

	angular
	.module('integragenerico', [
    'ngAnimate', //Animaciones angular
    'ngCookies', //guardar datos en cookies con angular
    'ngTouch', //eventos tactiles con angular
    'ngSanitize', //limpiar los datos ingresados por el usuario con angular (xss)
    'ngMessages', //mostrar mensajes basados en las propiedades de los objetos con angular (se usa para mensajes de error de los form)
    'ngAria', //mostrar los atributos de los objetos en propiedades visibles x herramientas visibles de propiedades de accesibilidad
    'ngResource', //Accede recursos REST
    'ui.router', //maneja estados de la app
    'ui.bootstrap', //framework de componente de UI
    'toastr', //notificaciones toast con js
    'formly', //formularios a partir de arrays de campos
    'formlyBootstrap', //plantillas de bootstrap para formly
    'pascalprecht.translate', // permite la internacionalizacion de textos en la UI
    'ui.bootstrap.showErrors', //permite mostrar errores en formularios
    'ngTagsInput', //componente de etiquetas en formularios
//  'aj.crop', //componente para el tratamiento de imagenes (recortar)
    'ui.calendar', //componente para manejo de calendarios en UI
//    'highcharts-ng', //componente de graficos UI
    'ngStorage', //soporte para guardar datos en el local o sesion storage
    'base64', //soporte para codificar en base64
    'ngTable', //soporte para tablas dinámicas con angular.
    'xeditable', //soporte para editar en el lugar en angular.
    'angular-momentjs', // Módulo de momentjs
    'color.picker' //soporte para seleccionar un color
    ]);

		angular
		.module('devoops', []);

		angular
		.module('integragenerico.seguridad', [
			'ngCookies', //guardar datos en cookies con angular
			'ngResource', //Accede recursos REST
			'ngStorage' //soporte para guardar datos en el local o sesion storage
		]);

})();
