/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('integragenerico')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('$',$);
})();
