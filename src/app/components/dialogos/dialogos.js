(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name clienteWebBondisvmApp.ServicioDialogos
   * @description
   * # ServicioDialogos
   * Provee servicios para hacer mas simple la instanciacion de dialogos.
   */
  angular.module('integragenerico')
    .service('Dialogos', Dialogos);


  function Dialogos($uibModal, ConfigurarDialogos) {

    var vm = this;

    var dialogoTransicion = null;

    this.abrirDialogoTransicionEstados = function (config) {
      if (!dialogoTransicion) {
        dialogoTransicion = vm.abrirDialogoCargando(config);
      }
    };

    this.cerrarDialogoTransicionEstados = function () {
      if (dialogoTransicion) {
        dialogoTransicion.opened.then(function () {
          dialogoTransicion.close();
          dialogoTransicion = null;
        });

      }
    };

    /**
     * Crea un dialogo de cargando con un determinado titulo y lo
     * devuelve.
     * @param String prefijo el prefijo de las propiedades usadas para
     * traducir el texto del dialogo.
     * @param Function onOpen una funcion que se llamara cuando el
     * dialogo se abra.
     * @returns Object un manejador del dialogo que puede usarse para
     * interactuar con el.
     */
    this.abrirDialogoCargando = function (config) {
      if (!config) {
        config = {};
      }
      if (!config.prefijo) {
        config.prefijo = 'GLOBAL.CARGANDO';
      }
      var respuesta = $uibModal.open({
        templateUrl: 'integragenerico/components/dialogos/dialogo-cargando.html',
        controllerAs: 'vm',
        controller: function (prefijo) {
          var vm = this;
          vm.titulo = prefijo + '.TITULO';
          vm.contenido = prefijo + '.CONTENIDO';
        },
        resolve: {
          prefijo: function () {
            return config.prefijo;
          }
        },
        backdrop: ConfigurarDialogos.backdrop
      });
      if (config.onOpen) {
        respuesta.opened.then(config.onOpen);
      }
      return respuesta;
    };

    /**
     * Crea un dialogo de confirmacion con las propiedades pasadas y lo
     * devuelve.
     * @param String prefijo el prefijo de las propiedades desde donde
     * se obtendran los textos para el titulo, contenido, boton si y
     * boton no.
     * @param Boolean tieneTitulo indica si el cuadro de dialogo tiene titulo.
     * @param Boolean cancelar indica si el cuadro de dialogo tendra boton
     * cancelar.
     * @param Function onResult una funcion a ejecutar cuando se cierra
     * el dialogo
     * @returns Object un manejador del dialogo que puede usarse para
     * interactuar con el.
     */
    this.abrirDialogoConfirmacion = function (config) {
      var respuesta = $uibModal.open({
        templateUrl: 'integragenerico/components/dialogos/dialogo-confirmacion.html',
        controllerAs: 'vm',
        controller: 'DialogoConfirmacionCtrl',
        size: 'sm',
        backdrop: 'static',
        resolve: {
          tieneTitulo: function () {
            return config.tieneTitulo;
          },
          prefijo: function () {
            return config.prefijo;
          },
          cancelar: function () {
            return config.cancelar;
          },
          textoDinamico: function () {
            return config.textoDinamico;
          }
        }
      });
      respuesta.result.then(config.onResult, function (info) {
        if (config.callbackCancelar) {
          return config.callbackCancelar(info);
        }
      });
      return respuesta;
    };

    this.abrirDialogoMostrarImagen = function (titulo, img, info, onOpen) {
      var respuesta = $uibModal.open({
        templateUrl: 'integragenerico/components/dialogos/dialogo-mostrar-imagen.html',
        controllerAs: 'vm',
        controller: 'DialogoMostrarImagenCtrl',
        windowClass: 'box expanded expanded-padding',
        keyboard: false,
        backdrop: 'static',
        resolve: {
          titulo: function () {
            return titulo;
          },
          img: function () {
            return img;
          },
          info: function () {
            return info;
          }
        }
      });
      if (onOpen) {
        respuesta.opened.then(onOpen);
      }
      return respuesta;
    };

    this.abrirDialogoInformacion = function (config) {
      var respuesta = $uibModal.open({
        templateUrl: 'integragenerico/components/dialogos/dialogo-informacion.html',
        controllerAs: 'vm',
        controller: 'DialogoInformacionCtrl',
        size: 'sm',
        resolve: {
          tieneTitulo: function () {
            return config.tieneTitulo;
          },
          prefijo: function () {
            return config.prefijo;
          },
          textoDinamico: function () {
            return config.textoDinamico;
          }
        }
      });
      if (config.onOpen) {
        respuesta.opened.then(config.onOpen);
      }
      return respuesta;
    };
  }

})();
