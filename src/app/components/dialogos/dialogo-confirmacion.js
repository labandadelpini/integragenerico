(function () {
	'use strict';
	angular
	.module('integragenerico')
	.controller('DialogoConfirmacionCtrl', DialogoConfirmacionCtrl);
	function DialogoConfirmacionCtrl($scope, $uibModalInstance, prefijo, tieneTitulo, cancelar, textoDinamico ) {

		var vm = this;

		vm.tieneTitulo = tieneTitulo;
		vm.cancelar = cancelar;
		vm.textoDinamico = textoDinamico;
		vm.titulo = prefijo+'.TITULO';
		vm.contenido = vm.textoDinamico ? vm.textoDinamico : prefijo+'.CONTENIDO';
		vm.textoSi = prefijo+'.TEXTO_SI';
		vm.textoNo = prefijo+'.TEXTO_NO';
		vm.si = function() {
			$uibModalInstance.close(true);
		};

		vm.no = function() {
			$uibModalInstance.dismiss(false);
		};
	}
})();
