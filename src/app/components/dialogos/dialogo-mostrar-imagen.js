(function () {
	'use strict';
	angular
	.module('integragenerico')
	.controller('DialogoMostrarImagenCtrl', DialogoMostrarImagenCtrl);
	function DialogoMostrarImagenCtrl($scope, $modalInstance, titulo, img, info ) {

		var vm = this;
		vm.titulo = titulo;
		vm.img = img;
		vm.info = info;

		vm.cerrar = function() {
			$modalInstance.close(true);
		};
	}
})();