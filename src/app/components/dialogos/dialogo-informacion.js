(function () {
	'use strict';
	angular
	.module('integragenerico')
	.controller('DialogoInformacionCtrl', DialogoInformacionCtrl);
	function DialogoInformacionCtrl($scope, $uibModalInstance, prefijo, tieneTitulo, textoDinamico ) {

		var vm = this;

		vm.tieneTitulo = tieneTitulo;
		vm.textoDinamico = textoDinamico;
		vm.titulo = 'Información';
		vm.contenido = vm.textoDinamico ? vm.textoDinamico : prefijo+'.CONTENIDO';
		vm.textoCerrar = 'Cerrar';
		vm.cerrar = function() {
			$uibModalInstance.close(true);
		};
	}
})();
