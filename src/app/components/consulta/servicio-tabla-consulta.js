(function () {
  'use strict';

  angular
    .module('integragenerico').service('ServicioTablaConsulta', ServicioTablaConsulta);

  function ServicioTablaConsulta($injector, $state, $uibModal, NgTableParams, CamposListado, AccionesGenerales, Filtro, Paginacion, Orden, Dialogos, TransformacionString, ConfigurarListados, ConfigurarConfirmaciones) {

    var vm = this;

    vm.getTabla = getTabla;

    vm.abrirDialogoSeleccionar = abrirDialogoSeleccionar;

    function Tabla(clave) {
      var lista = {
        listar: listar,
        resultados: [],
        cargando: false,
        acciones: {},
        prefijo: TransformacionString.camelToUnderscore(clave)
      };
      lista.acciones.editar = function (row) {
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_SELECCIONAR',
          tieneTitulo: true,
          cancelar: true
        };
        Dialogos.abrirDialogoConfirmacion(config)
          .result.then(abrirDialogoTransicion)
          .then(function () {
            confirmarSeleccion(row);
          });
      };
      lista.acciones.nuevoModal = function () {
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_NUEVO',
          tieneTitulo: true,
          cancelar: true
        };
        if (ConfigurarConfirmaciones.alCrear) {
          Dialogos.abrirDialogoConfirmacion(config)
            .result.then(abrirDialogoTransicion)
            .then(function () {
              // confirmarEdicion('nuevo');
              abrirDialogoNuevo(clave, function () {
                lista.tableParams.reload();
              });
            });
        } else {
          abrirDialogoNuevo(clave, function () {
            lista.tableParams.reload();
          });
        }
      };
      return CamposListado.getCampos(clave).then(function (data) {
        data[data.length - 1].acciones = [];
        // vm.tabla.acciones.seleccionar = seleccionar(fila);

        data[data.length - 1].acciones.push({
          accion: "tabla.acciones.seleccionar(row)",
          claseBoton: "btn-primary",
          icono: "fa fa-thumb-tack"
        });

        lista.cols = data;

        return Orden.getOrdenPorDefecto(clave).then(function (orden) {
          lista.filtro = Filtro.getFiltro(lista);
          lista.filtrosActivos = false;
          lista.mostrarFiltros = mostrarFiltros;
          lista.ordenPorDefecto = orden;
          lista.tableParams = crearTabla(clave, lista);
          return AccionesGenerales.getAcciones(clave, true).then(function (acciones) {
            lista.accionesGenerales = acciones;
            return lista;
          });
        });
      });
    }

    function crearTabla(clave, lista) {
      var parametros = Paginacion.getParametros(clave);
      // console.log(lista);
      parametros.sorting = lista.ordenPorDefecto;
      // console.log(parametros);
      return new NgTableParams(parametros, {
        filterDelay: Filtro.getDelay(),
        paginationMinBlocks: Paginacion.getBloquesMinimos,
        getData: function (params) {
          lista.cargando = true;
          lista.resultados = lista.listar(clave, params, lista.filtro.filtro).then(function (resultados) {
            lista.cargando = false;
            return resultados;
          });
          return lista.resultados;
        }
      });
    }

    function listar(clave, params, filtro) {
      this.cargando = true;
      var parametros = params.parameters();
      var query = {};
      if (ConfigurarListados.filtra) {
        Filtro.generarFiltros(query, parametros.filter, filtro);
      }
      if (ConfigurarListados.ordena) {
        Orden.generarOrden(query, parametros.sorting);
      }
      if (ConfigurarListados.pagina) {
        Paginacion.generarPaginacion(query, parametros.page, parametros.count);
      }
      if (ConfigurarListados.pagina) {
        return $injector.get(clave).get(query).$promise.then(function (data) {
          params.total(data.totalElements);
          return data.content;
        });
      } else {
        return $injector.get(clave).query(query).$promise.then(function (data) {
          return data;
        });
      }
    }

    function getTabla(clave) {
      return new Tabla(clave);
    }

    function abrirDialogoTransicion() {
      return Dialogos.abrirDialogoTransicionEstados();
    }

    function abrirDialogoCargando() {
      return Dialogos.abrirDialogoCargando();
    }

    function mostrarFiltros() {
      this.filtrosActivos = !this.filtrosActivos;
    }

    function abrirDialogoSeleccionar(clave, callbackSeleccionado, callbackSalida, funcionFiltros) {
      var modalInstance = $uibModal.open({
        templateUrl: 'integragenerico/components/lista-elementos/vista-tabla-elementos-modal.html',
        size: 'lg',
        backdrop: 'static',
        controller: function ($uibModalInstance, tabla, funcionFiltros) {
          var vm = this;
          vm.tabla = tabla;
          if (funcionFiltros) {
            funcionFiltros(vm.tabla);
          }
          vm.tabla.acciones.seleccionar = function (fila) {
            $uibModalInstance.close(fila);
          }
        },
        controllerAs: 'vm',
        resolve: {
          tabla: function (ServicioTablaConsulta) {
            return ServicioTablaConsulta.getTabla(clave).then(function (tabla) {
              return tabla;
            });
          },
          funcionFiltros: function () {
            return funcionFiltros;
          }
        }
      });
      modalInstance.result.then(callbackSeleccionado, callbackSalida);
    }

    function abrirDialogoNuevo(clave, callbackCerrado) {
      var propiedades = {};
      Dialogos.abrirDialogoTransicionEstados();
      var modalInstance = $uibModal.open({
        templateUrl: 'integragenerico/components/editar-elemento/vista-editar-elemento-modal.html',
        backdrop: 'static',
        windowClass: 'app-modal-window',
        controller: function ($uibModalInstance, formulario) {
          var vm = this;
          vm.formulario = formulario;
          Dialogos.cerrarDialogoTransicionEstados();

        },
        controllerAs: 'vm',
        resolve: {
          formulario: function (ServicioEdicion) {
            return ServicioEdicion.getFormulario(clave, null, null, propiedades);
          }
        }
      });
      propiedades.modalInstance = modalInstance;
      modalInstance.result.then(function () {
        Dialogos.cerrarDialogoTransicionEstados();
        callbackCerrado();
      });
    }

  }

})();
