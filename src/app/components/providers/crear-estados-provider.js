(function() {
  'use strict';

  angular
    .module('integragenerico').provider('EstadosGenericos', EstadosGenericosProvider);

  function EstadosGenericosProvider($stateProvider) {

    this.$get = function() {

    };

    this.crearEstados = crearEstados;

    function crearEstados(nombreClave, nombreEstado, url) {
      var controladorListar = nombreClave + 'ListaCtrl';
      var controladorEditar = nombreClave + 'EditarCtrl';

      $stateProvider
        .state('app.' + nombreEstado, {
          url: url,
          template: '<ui-view></ui-view>',
          abstract: true
        })
        .state('app.' + nombreEstado + '.lista', {
          url: '/lista',
          templateUrl: 'integragenerico/components/lista-elementos/vista-tabla-elementos.html',
          controller: controladorListar,
          controllerAs: 'vm',
          resolve: {
            tabla: function(ServicioTabla) {
              return ServicioTabla.getTabla(nombreClave);
            }
          }
        })
        .state('app.' + nombreEstado + '.editar', {
          url: '/editar/:id/?soloLectura',
          templateUrl: 'integragenerico/components/editar-elemento/vista-editar-elemento.html',
          controller: controladorEditar,
          controllerAs: 'vm',
          resolve: {
            formulario: function($stateParams, ServicioEdicion) {
              return ServicioEdicion.getFormulario(nombreClave, $stateParams.id, $stateParams.soloLectura);
            }
          }
        });
    }


  }

})();
