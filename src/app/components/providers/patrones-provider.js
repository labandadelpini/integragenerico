(function() {
    'use strict';

    angular
    .module('integragenerico').provider('Patrones', PatronesProvider);

    function PatronesProvider() {

       var ubicacionRecurso;

       this.$get = function($http) {
        return {
            getPatron : function (clave) {
                return $http.get(ubicacionRecurso).then(function (data) {
                    return data.data[clave];
                });
            }
        };
    };

    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
        ubicacionRecurso = ubicacion;
    }


}

})();