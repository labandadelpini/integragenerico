(function() {
    'use strict';

    angular
    .module('integragenerico').provider('CamposEditar', CamposEditarProvider);

    function CamposEditarProvider() {

     var ubicacionRecurso;

     this.$get = function($http, $injector) {
        return {
            getCampos : function (clave) {
                var recurso = $injector.get(clave);
                if (recurso && recurso.json) {
                    return $http.get(recurso.json).then(function (data) {
                        return data.data['CamposEditar'];
                    });

                } else {
                    return $http.get(ubicacionRecurso).then(function (data) {
                        return data.data[clave];
                    });
                }
            }
        };
    };

    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
        ubicacionRecurso = ubicacion;
    }


}

})();