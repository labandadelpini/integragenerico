(function() {
    'use strict';

    angular
    .module('integragenerico').provider('Orden', OrdenProvider);

    function OrdenProvider() {

       var ubicacionRecurso;

       this.$get = function($http,$injector) {
        return {
            getOrdenPorDefecto : function (clave) {
                var recurso = $injector.get(clave);
                if (recurso && recurso.json) {
                    return $http.get(recurso.json).then(function (data) {
                        var defecto = data.data['Orden'];
                        var respuesta = {};
                        if (!defecto) {
                            return respuesta;
                        }
                        defecto.forEach(function(orden){
                            respuesta[orden.propiedad] = orden.desc ? 'desc' : 'asc';
                        });
                        return respuesta;
                    });
                } else {
                    return $http.get(ubicacionRecurso).then(function (data) {
                        var defecto = data.data[clave];
                        var respuesta = {};
                        if (!defecto) {
                            return respuesta;
                        }
                        defecto.forEach(function(orden){
                            respuesta[orden.propiedad] = orden.desc ? 'desc' : 'asc';
                        });
                        return respuesta;
                    });
                }
            },
            generarOrden : function (query, sorting) {
                var sort = [];
                for (var key in sorting) {
                    if (sorting.hasOwnProperty(key)) {
                        sort.push(key+','+sorting[key]);
                    }
                }
                if (sort.length === 0) {
					sort.push('id,asc');
				}
                query.sort = sort;
            }
        };
    };

    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
        ubicacionRecurso = ubicacion;
    }


}

})();