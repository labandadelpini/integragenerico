(function () {
  'use strict';

  angular
    .module('integragenerico').provider('ConfigurarDialogos', ConfigurarDialogosProvider);

  function ConfigurarDialogosProvider() {

    var configuraciones = {
      backdrop: true
    };

    this.$get = function () {
      return configuraciones;
    };
    this.setBackdrop = setBackdrop;


    function setBackdrop(backdrop) {
      configuraciones.backdrop = backdrop;
    }
  }

})();
