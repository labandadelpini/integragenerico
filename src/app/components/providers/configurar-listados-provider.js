(function() {
    'use strict';

    angular
    .module('integragenerico').provider('ConfigurarListados', ConfigurarListadosProvider);

    function ConfigurarListadosProvider() {

        var configuracion = {
            filtra : true,
            pagina : true,
            ordena : true
        };

        this.$get = function() {
            return configuracion;
        };


        this.setFiltra = setFiltra;
        this.setPagina = setPagina;
        this.setOrdena = setOrdena;

        function setFiltra(filtra) {
            configuracion.filtra = filtra;
        }
        function setPagina(pagina) {
            configuracion.pagina = pagina;
        }
        function setOrdena(ordena) {
            configuracion.ordena = ordena;
        }


    }

})();