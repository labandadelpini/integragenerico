(function() {
  'use strict';

  angular
    .module('integragenerico').provider('ConfigurarConfirmaciones', ConfigurarConfirmacionesProvider);

  function ConfigurarConfirmacionesProvider() {

    var confirmaciones = {
      alGuardar: true,
      alEditar: true,
      alCrear: true,
      alEliminar : true
    };

    this.$get = function() {
      return confirmaciones;
    };
    this.setAlGuardar = setAlGuardar;
    this.setAlEditar = setAlEditar;
    this.setAlCrear = setAlCrear;
    this.setAlEliminar = setAlEliminar;

    function setAlGuardar(alGuardar) {
      confirmaciones.alGuardar = alGuardar;
    }

    function setAlEditar(alEditar) {
      confirmaciones.alEditar = alEditar;
    }

    function setAlCrear(alCrear) {
      confirmaciones.alCrear = alCrear;
    }

    function setAlEliminar(alEliminar) {
      confirmaciones.alEliminar = alEliminar;
    }
  }

})();
