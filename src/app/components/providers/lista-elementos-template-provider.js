(function() {
  'use strict';
  angular
    .module('integragenerico').provider('ListaElementosTemplate', ListaElementosTemplateProvider);

  function ListaElementosTemplateProvider() {
    var ubicacionRecurso = 'integragenerico/components/lista-elementos/tabla-elementos.html';
    this.$get = function() {
      return {
        getTemplate: function() {
          return ubicacionRecurso;
        }
      };
    };
    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
      ubicacionRecurso = ubicacion;
    }
  }

})();
