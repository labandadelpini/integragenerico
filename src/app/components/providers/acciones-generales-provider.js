(function() {
  'use strict';

  angular
  .module('integragenerico').provider('AccionesGenerales', AccionesGeneralesProvider);

  function AccionesGeneralesProvider() {

   var ubicacionRecurso;
   var ubicacionAccionesColumnas;

   this.$get = function($http,$parse,$injector,$q) {

    function compilarAcciones(acciones) {
      acciones.forEach(function(accion){
        accion.ejecutarAccion = $parse(accion.accion);
      });
    }
    return {
      getAcciones : function (clave, esConsulta) {
        var recurso = $injector.get(clave);
        if (recurso && recurso.json) {
          return $http.get(recurso.json).then(function (data) {
            var acciones = !esConsulta ? data.data['AccionesGenerales'] : data.data['AccionesGeneralesConsulta'];
            if (acciones) {
              compilarAcciones(acciones);
              return acciones;
            }else {
              return [];
            }
          });
        } else {
          return $http.get(ubicacionRecurso).then(function (data) {
            var acciones = data.data[clave];
            if (acciones) {
              compilarAcciones(acciones);
              return acciones;
            }else {
              return [];
            }
          });
        }
      },
      getAccionesTabla : function() {
        if (!ubicacionAccionesColumnas) {
          return $q.when([]);
        } else {
          return $http.get(ubicacionAccionesColumnas).then(function (data) {
            var acciones = data.data;
            if (acciones) {
              compilarAcciones(acciones);
              return acciones;
            }else {
              return [];
            }
          });
        }
      }
    };
  };

  this.setUbicacionRecurso = setUbicacionRecurso;
  this.setUbicacionAccionesColumnas = setUbicacionAccionesColumnas;

  function setUbicacionRecurso(ubicacion) {
    ubicacionRecurso = ubicacion;
  }

  function setUbicacionAccionesColumnas(ubicacion) {
    ubicacionAccionesColumnas = ubicacion;
  }




}

})();
