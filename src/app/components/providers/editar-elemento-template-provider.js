(function() {
  'use strict';
  angular
    .module('integragenerico').provider('EditarElementosTemplate', EditarElementosTemplateProvider);

  function EditarElementosTemplateProvider() {
    var ubicacionRecurso = 'integragenerico/components/editar-elemento/formulario-editar-elemento.html';
    this.$get = function() {
      return {
        getTemplate: function() {
          return ubicacionRecurso;
        }
      };
    };
    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
      ubicacionRecurso = ubicacion;
    }
  }

})();
