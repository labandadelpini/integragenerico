(function() {
  'use strict';

  angular
  .module('integragenerico')
  .filter('porcentaje', Porcentaje)
  .filter('numeroEntero', NumeroEntero)
  .filter('numeroDecimal', NumeroDecimal);

  /** @ngInject */
  function Porcentaje($filter) {
    return function (valor) {
      return $filter('number')(valor, 2) + ' %';
    };
  }

  function NumeroEntero($filter) {
    return function (valor) {
      return $filter('number')(valor, 0);
    };
  }


  function NumeroDecimal($filter) {
    return function (valor) {
      return $filter('number')(valor, 2);
    };
  }
})();
