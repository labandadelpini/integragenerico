'use strict';

var util = angular.module('integragenerico');

/**
 * Directiva que detecta los eventos keyDown y KeyPress y si es un Enter (13)
 * llama a la funcion que recibe como parametro.
 * <input type="text" on-enter="actualizarFiltro()">
 */
util.directive('onEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if (event.which === 13) {
        scope.$apply(function () {
          scope.$eval(attrs.onEnter);
        });
        event.preventDefault();
      }
    });
  };
});

util.directive('selectOnFocus', function () {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      element.on('focus', function () {
        this.select();
      });
    }
  };
});

util.filter('reverse', function () {
  return function (items) {
    return items.slice().reverse();
  };
});

util.directive('showErrorDate', [
  '$timeout', 'showErrorsDatetimeConfig', '$interpolate',
  function ($timeout, showErrorsConfig, $interpolate) {
    var getShowSuccess, getTrigger, linkFn;
    getTrigger = function (options) {
      var trigger;
      trigger = showErrorsConfig.trigger;
      if (options && (options.trigger !== null)) {
        trigger = options.trigger;
      }
      return trigger;
    };
    getShowSuccess = function (options) {
      var showSuccess;
      showSuccess = showErrorsConfig.showSuccess;
      if (options && (options.showSuccess !== null)) {
        showSuccess = options.showSuccess;
      }
      return showSuccess;
    };
    linkFn = function (scope, el, attrs, formCtrl) {
      var blurred, inputEl, padre, padreNg, inputName, inputNgEl, options, showSuccess, toggleClasses, trigger;
      blurred = false;
      options = scope.$eval(attrs.showErrors);
      showSuccess = getShowSuccess(options);
      trigger = getTrigger(options);
      inputEl = el[0].querySelector('[name] .form-control');
      padre = el[0].querySelector('[name]');
      padreNg = angular.element(padre);
      inputNgEl = angular.element(inputEl);
      inputName = $interpolate(padreNg.attr('name') || '')(scope);
      if (!inputName) {
        throw "show-errors element has no child input elements with a 'name' attribute and a 'form-control' class";
      }
      inputNgEl.bind(trigger, function () {
        blurred = true;
        return toggleClasses(formCtrl.$invalid);
      });
      scope.$watch(function () {
        return formCtrl.$invalid;
      }, function (invalid) {
        if (!blurred) {
          return;
        }
        return toggleClasses(invalid);
      });
      scope.$on('show-errors-check-validity', function () {
        return toggleClasses(formCtrl[inputName].$invalid);
      });
      scope.$on('show-errors-reset', function () {
        return $timeout(function () {
          el.removeClass('has-error');
          el.removeClass('has-success');
          return blurred = false;
        }, 0, false);
      });
      return toggleClasses = function (invalid) {
        el.toggleClass('has-error', invalid);
        if (showSuccess) {
          return el.toggleClass('has-success', !invalid);
        }
      };
    };
    return {
      restrict: 'A',
      require: '^form',
      compile: function (elem, attrs) {
        if (attrs['showErrorDate'].indexOf('skipFormGroupCheck') === -1) {
          if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
            throw "show-errors element does not have the 'form-group' or 'input-group' class";
          }
        }
        return linkFn;
      }
    };
  }
]).provider('showErrorsDatetimeConfig', function () {
  var _showSuccess, _trigger;
  _showSuccess = false;
  _trigger = 'blur';
  this.showSuccess = function (showSuccess) {
    return _showSuccess = showSuccess;
  };
  this.trigger = function (trigger) {
    return _trigger = trigger;
  };
  this.$get = function () {
    return {
      showSuccess: _showSuccess,
      trigger: _trigger
    };
  };
});

util.directive('showErrorEtiqueta', [
  '$timeout', 'showErrorsEtiquetaConfig', '$interpolate',
  function ($timeout, showErrorsConfig, $interpolate) {
    var getShowSuccess, getTrigger, linkFn;
    getTrigger = function (options) {
      var trigger;
      trigger = showErrorsConfig.trigger;
      if (options && (options.trigger !== null)) {
        trigger = options.trigger;
      }
      return trigger;
    };
    getShowSuccess = function (options) {
      var showSuccess;
      showSuccess = showErrorsConfig.showSuccess;
      if (options && (options.showSuccess !== null)) {
        showSuccess = options.showSuccess;
      }
      return showSuccess;
    };
    linkFn = function (scope, el, attrs, formCtrl) {
      var blurred, inputEl, padre, padreNg, inputName, inputNgEl, options, showSuccess, toggleClasses, trigger;
      blurred = false;
      options = scope.$eval(attrs.showErrors);
      showSuccess = getShowSuccess(options);
      trigger = getTrigger(options);
      inputEl = el[0].querySelector('[name] tags-input');
      padre = el[0].querySelector('[name]');
      padreNg = angular.element(padre);
      inputNgEl = angular.element(inputEl);
      inputName = $interpolate(padreNg.attr('name') || '')(scope);
      if (!inputName) {
        throw "show-errors element has no child input elements with a 'name' attribute and a 'form-control' class";
      }
      inputNgEl.bind(trigger, function () {
        blurred = true;
        return toggleClasses(formCtrl.$invalid);
      });
      scope.$watch(function () {
        return formCtrl.$invalid;
      }, function (invalid) {
        if (!blurred) {
          return;
        }
        return toggleClasses(invalid);
      });
      scope.$on('show-errors-check-validity', function () {
        return toggleClasses(formCtrl[inputName].$invalid);
      });
      scope.$on('show-errors-reset', function () {
        return $timeout(function () {
          el.removeClass('has-error');
          el.removeClass('has-success');
          return blurred = false;
        }, 0, false);
      });
      return toggleClasses = function (invalid) {
        el.toggleClass('has-error', invalid);
        if (showSuccess) {
          return el.toggleClass('has-success', !invalid);
        }
      };
    };
    return {
      restrict: 'A',
      require: '^form',
      compile: function (elem, attrs) {
        if (attrs['showErrorEtiqueta'].indexOf('skipFormGroupCheck') === -1) {
          if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
            throw "show-errors element does not have the 'form-group' or 'input-group' class";
          }
        }
        return linkFn;
      }
    };
  }
]).provider('showErrorsEtiquetaConfig', function () {
  var _showSuccess, _trigger;
  _showSuccess = false;
  _trigger = 'blur';
  this.showSuccess = function (showSuccess) {
    return _showSuccess = showSuccess;
  };
  this.trigger = function (trigger) {
    return _trigger = trigger;
  };
  this.$get = function () {
    return {
      showSuccess: _showSuccess,
      trigger: _trigger
    };
  };
});

/**
 * Directiva que genera la tabla lista de objetos columnas.
 */
util.directive('tablaOrdenable', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      cambiarOrden: '=',
      esOrden: '=',
      columnas: '=',
      elementos: '=',
      acciones: '=',
      footer: '=',
      mostrarDialogoOrden: '='
    },
    templateUrl: 'integragenerico/components/util/tabla_ordenable.html',
    controller: ['$scope', '$parse', function ($scope, $parse) {
      $scope.cambiarOrdenImp = function (nombreCampo) {
        if ($scope.cambiarOrden !== undefined) {
          $scope.cambiarOrden(nombreCampo);
        } else if ($scope.$parent.cambiarOrden !== undefined) {
          $scope.$parent.cambiarOrden(nombreCampo);
        }
      };
      $scope.esOrdenImp = function (nombreCampo, reverso) {
        if ($scope.esOrden !== undefined) {
          return $scope.esOrden(nombreCampo, reverso);
        } else if ($scope.$parent.esOrden !== undefined) {
          return $scope.$parent.esOrden(nombreCampo, reverso);
        } else
          return undefined;
      };
      $scope.devolverValor = function (elemento, columna, campo) {
        var nombreFuncion = campo ? 'devolverValor' + campo : 'devolverValor';
        if (columna[nombreFuncion] === undefined) {
          if (campo) {
            columna[nombreFuncion] = $parse(columna[campo]);
          } else {
            columna[nombreFuncion] = $parse(columna.nombreCampo);
          }

        }
        return columna[nombreFuncion](elemento);
      };

    }]
  };
});

/**
 * Directiva que genera la fila de encabezado de una tabla en base a una lista de
 * objetos encabezado. Y cada uno de esos encabezados es un encabezado ordenable.
 */
util.directive('filaEncabezadoOrdenable', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      cambiarOrden: '=',
      esOrden: '=',
      columnas: '=',
      acciones: '=',
      idTabla: '=',
      mostrarDialogoOrden: '='
    },
    templateUrl: 'integragenerico/components/util/fila_encabezado_ordenable.html',
    controller: ['$scope', function ($scope) {
      $scope.cambiarOrdenImp = function (nombreCampo) {
        if ($scope.cambiarOrden !== undefined) {
          $scope.cambiarOrden(nombreCampo);
        } else if ($scope.$parent.cambiarOrden !== undefined) {
          $scope.$parent.cambiarOrden(nombreCampo);
        }
      };
      $scope.esOrdenImp = function (nombreCampo, reverso) {
        if ($scope.esOrden !== undefined) {
          return $scope.esOrden(nombreCampo, reverso);
        } else if ($scope.$parent.esOrden !== undefined) {
          return $scope.$parent.esOrden(nombreCampo, reverso);
        } else
          return undefined;
      };
    }]
  };
});

/**
 * Directiva que define un encabezado que ordena el contenido de la tabla en base
 * a la columna de ese encabezado. Ademas muestra una flechita indicando que columna
 * y en que direccion se esta ordenando
 */
util.directive('encabezadoOrdenable', function () {
  return {
    restrict: 'A',
    replace: false,
    scope: {
      campo: '=',
      cambiarOrden: '=',
      esOrden: '='
    },
    //        template: '<th>hola</th>'
    templateUrl: 'integragenerico/components/util/encabezado_ordenable.html',
    controller: ['$scope', function ($scope) {
      $scope.cambiarOrdenImp = function (nombreCampo) {
        if ($scope.cambiarOrden !== undefined) {
          $scope.cambiarOrden(nombreCampo);
        } else if ($scope.$parent.cambiarOrden !== undefined) {
          $scope.$parent.cambiarOrden(nombreCampo);
        }
      };
      $scope.esOrdenImp = function (nombreCampo, reverso) {
        if ($scope.esOrden !== undefined) {
          return $scope.esOrden(nombreCampo, reverso);
        } else if ($scope.$parent.esOrden !== undefined) {
          return $scope.$parent.esOrden(nombreCampo, reverso);
        } else
          return undefined;
      };
      $scope.ejecutarAccion = function (campo) {
        if (campo.tipo === 'Seleccionable') {
          return function () {};
        } else {
          return $scope.cambiarOrdenImp(campo.nombreCampo)
        }

      };
    }]
  };
});

util.directive('encabezadoSeleccionable', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      campo: '='
    },
    templateUrl: 'integragenerico/components/util/encabezado_seleccionable.html',
    controller: ['$scope', function ($scope) {
      //                $scope.$watch('campo.todosSeleccionados', function(newValue, oldValue) {
      //                    if (oldValue && !newValue) {
      //                        $scope.funcionSeleccionarTodos(false);
      //                    } else {
      //                        $scope.funcionSeleccionarTodos(true);
      //                    }
      //                });
      //                $scope.$watch('campo.funcionValidarSeleccionarTodos()', function(newValue, oldValue) {
      //                    $scope.campo.todosSeleccionados = newValue;
      //                });
    }]
  };
});

/**
 * Directiva que genera la tabla lista de objetos columnas.
 */
util.directive('paginacionConCantidadItems', function () {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      paginacion: '='
    },
    templateUrl: 'integragenerico/components/util/paginacion_con_cantidad_items.html'
  };
});

/**
 * Directiva que genera el filtro para la tabla.
 */
util.directive('filtroTabla', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      filtro: '='
    },
    templateUrl: 'integragenerico/components/util/filtro_tabla.html'
  };
});

/**
 * Directiva que genera el filtro para la tabla.
 */
util.directive('filtroSeleccion', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      filtro: '='
    },
    templateUrl: 'integragenerico/components/util/filtro-seleccion.html'
  };
});

util.directive('datepickerPopup', function () {
  return {
    restrict: 'EAC',
    require: 'ngModel',
    link: function (scope, element, attr, controller) {
      //remove the default formatter from the input directive to prevent conflict
      controller.$formatters.shift();
    }
  };
});

util.directive('feriado', function ($q) {
  return {
    require: 'ngModel',
    link: function (scope, elm, attrs, ctrl) {
      ctrl.$asyncValidators.feriado = function (modelValue, viewValue) {
        var promesa = $q.defer();
        scope.campo.restricciones.fechaDeshabilitada(modelValue).$promise.then(function (result) {
          if (result.esFeriado) {
            promesa.reject(false);
          } else {
            promesa.resolve(true);
          }
        });
        return promesa.promise;
      };
    }
  };
});

util.directive('elementoSeleccionable', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      elemento: '=',
      columna: '='
    },
    controller: ['$scope', '$parse', function ($scope, $parse) {
      var funcionPrivada = $parse($scope.columna.nombreCampo);
      $scope.funcion = function (nuevoValor) {
        if (angular.isDefined(nuevoValor)) {
          funcionPrivada.assign($scope.elemento, nuevoValor);
        }
        return funcionPrivada($scope.elemento);
      };
      $scope.$watch('valor', function () {
        $scope.funcion($scope.valor);
      });
      $scope.$watch('funcion()', function () {
        $scope.valor = $scope.funcion();
      });
    }],
    templateUrl: 'integragenerico/components/util/elemento-seleccionable.html'
  };
});

util.filter('numberFixedLen', function () {
  return function (n, len) {
    var num = parseInt(n, 10);
    len = parseInt(len, 10);
    if (isNaN(num) || isNaN(len)) {
      return n;
    }
    num = '' + num;
    while (num.length < len) {
      num = '0' + num;
    }
    return num;
  };
});

util.factory('Base64', function () {
  /* jshint ignore:start */

  var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

  return {
    encode: function (input) {
      var output = "";
      var chr1, chr2, chr3 = "";
      var enc1, enc2, enc3, enc4 = "";
      var i = 0;

      do {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
          enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
          enc4 = 64;
        }

        output = output +
          keyStr.charAt(enc1) +
          keyStr.charAt(enc2) +
          keyStr.charAt(enc3) +
          keyStr.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
      } while (i < input.length);

      return output;
    },
    decode: function (input) {
      var output = "";
      var chr1, chr2, chr3 = "";
      var enc1, enc2, enc3, enc4 = "";
      var i = 0;

      // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
      var base64test = /[^A-Za-z0-9\+\/\=]/g;
      if (base64test.exec(input)) {
        window.alert("There were invalid base64 characters in the input text.\n" +
          "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
          "Expect errors in decoding.");
      }
      input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

      do {
        enc1 = keyStr.indexOf(input.charAt(i++));
        enc2 = keyStr.indexOf(input.charAt(i++));
        enc3 = keyStr.indexOf(input.charAt(i++));
        enc4 = keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
          output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
          output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

      } while (i < input.length);

      return output;
    }
  };

  /* jshint ignore:end */
});
