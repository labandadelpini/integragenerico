(function() {
    'use strict';

    angular
    .module('integragenerico')
    .service('TransformacionString', TransformacionString);


    function TransformacionString() {

    	var vm = this;

    	vm.camelToUnderscore = camelToUnderscore;
        
    	function camelToUnderscore(string) {
    		var underscore = string.replace(/[A-Z]/, function(substring) {
    			return '_' + substring;
    		});
    		var mayus = underscore.toUpperCase();
    		if (mayus.indexOf('_') === 0) {
    			mayus = mayus.slice(1);
    		}
    		return mayus;
    	}


    }
    
})();