(function () {
    'use strict';

    angular
        .module('integragenerico')
        .service('UtilidadesGenerico', UtilidadesGenerico);


    function UtilidadesGenerico() {

        var vm = this;

        vm.eliminarAccionConsultar = eliminarAccionConsultar;

        function eliminarAccionConsultar(columnas) {
            columnas.forEach(function (columna) {
                if (columna.tipo === 'celda-acciones') {
                    columna.acciones.forEach(function (accion) {
                        if (accion.popover === 'Consultar') {
                            accion.mostrar = function () {
                                return false;
                            }
                        }
                    })
                }
            });
        }

    }

})();