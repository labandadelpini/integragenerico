(function () {
  'use strict';

  angular
    .module('integragenerico').service('ServicioTabla', ServicioTabla);

  function ServicioTabla($injector, $state, NgTableParams, CamposListado, AccionesGenerales, Filtro, Paginacion, Orden, Dialogos, TransformacionString, ConfigurarListados, ConfigurarConfirmaciones) {

    var vm = this;

    vm.getTabla = getTabla;

    function Tabla(clave) {
      var lista = {
        listar: listar,
        resultados: [],
        cargando: false,
        acciones: {},
        prefijo: TransformacionString.camelToUnderscore(clave)
      };
      lista.acciones.nuevo = function () {
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_NUEVO',
          tieneTitulo: true,
          cancelar: true
        };
        if (ConfigurarConfirmaciones.alCrear) {
          Dialogos.abrirDialogoConfirmacion(config)
            .result.then(abrirDialogoTransicion)
            .then(function () {
              confirmarEdicion('nuevo');
            });
        } else {
          confirmarEdicion('nuevo');
        }
      };
      lista.acciones.editar = function (row) {
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_EDITAR',
          tieneTitulo: true,
          cancelar: true
        };
        Dialogos.abrirDialogoConfirmacion(config)
          .result.then(abrirDialogoTransicion)
          .then(function () {
            confirmarEdicion(row.id);
          });
      };
      lista.acciones.consultar = function (row) {
        consultar(row.id);
      };
      lista.acciones.eliminar = function (row) {
        var dialogo;
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_ELIMINAR',
          tieneTitulo: true,
          cancelar: true
        };
        Dialogos.abrirDialogoConfirmacion(config)
          .result.then(function () {
            dialogo = abrirDialogoCargando();
            return true;
          })
          .then(function () {
            return eliminar(clave, row).$promise;
          }).then(function () {
            Dialogos.abrirDialogoConfirmacion({
              prefijo: lista.prefijo + '.DIALOGO_ELIMINADO',
              tieneTitulo: true,
              cancelar: false
            });
            lista.tableParams.reload();
            dialogo.close();
          });
      };
      return CamposListado.getCampos(clave).then(function (data) {
        return agregarAccionesPorDefecto(data).then(function (data) {
          lista.cols = data;

          return Orden.getOrdenPorDefecto(clave).then(function (orden) {
            lista.filtro = Filtro.getFiltro(lista);
            lista.filtrosActivos = false;
            lista.mostrarFiltros = mostrarFiltros;
            lista.ordenPorDefecto = orden;
            lista.tableParams = crearTabla(clave, lista);
            return AccionesGenerales.getAcciones(clave).then(function (acciones) {
              lista.accionesGenerales = acciones;
              return lista;
            });
          });
        });
      });
    }

    function crearTabla(clave, lista) {
      var parametros = Paginacion.getParametros(clave);
      // console.log(lista);
      parametros.sorting = lista.ordenPorDefecto;
      // console.log(parametros);
      return new NgTableParams(parametros, {
        filterDelay: Filtro.getDelay(),
        paginationMaxBlocks: Paginacion.getBloquesMaximos() ? Paginacion.getBloquesMaximos() : 3,
        paginationMinBlocks: Paginacion.getBloquesMinimos(),
        getData: function (params) {
          lista.cargando = true;
          lista.resultados = lista.listar(clave, params, lista.filtro.filtro).then(function (resultados) {
            lista.cargando = false;
            return resultados;
          });
          return lista.resultados;
        }
      });
    }

    function listar(clave, params, filtro) {
      this.cargando = true;
      var parametros = params.parameters();
      var query = {};
      if (ConfigurarListados.filtra) {
        Filtro.generarFiltros(query, parametros.filter, filtro);
      }
      if (ConfigurarListados.ordena) {
        Orden.generarOrden(query, parametros.sorting);
      }
      if (ConfigurarListados.pagina) {
        Paginacion.generarPaginacion(query, parametros.page, parametros.count);
      }
      if (ConfigurarListados.pagina) {
        return $injector.get(clave).get(query).$promise.then(function (data) {
          params.total(data.totalElements);
          return data.content;
        }, function (error) {
          console.log(error);
          if (error.data.status === 403) {
            Dialogos.abrirDialogoInformacion({
              prefijo: '',
              tieneTitulo: true,
              textoDinamico: 'No tiene permisos suficientes'
            });
            $state.go('app');
          }
          return [];
        });
      } else {
        return $injector.get(clave).query(query).$promise.then(function (data) {
          return data;
        });
      }
    }

    function getTabla(clave) {
      return new Tabla(clave);
    }

    function abrirDialogoTransicion() {
      return Dialogos.abrirDialogoTransicionEstados();
    }

    function abrirDialogoCargando() {
      return Dialogos.abrirDialogoCargando();
    }

    function confirmarEdicion(id) {
      $state.go('^.editar', {
        id: id
      });
    }

    function consultar(id) {
      $state.go('^.editar', {
        id: id,
        soloLectura: true
      });
    }

    function eliminar(clave, row) {
      return $injector.get(clave).remove({
        codigo: row.id
      });
    }

    function mostrarFiltros() {
      this.filtrosActivos = !this.filtrosActivos;
    }

    function agregarAccionesPorDefecto(columnas) {
      var columnaAcciones = undefined;
      columnas.forEach(function (columna) {
        if (columna.tipo === 'celda-acciones') {
          columnaAcciones = columna;
        }
      });
      if (!columnaAcciones) {
        columnaAcciones = {
          "tipo": "celda-acciones",
          "title": "Acciones",
          "acciones": []
        };
        columnas.push(columnaAcciones);
      }
      return AccionesGenerales.getAccionesTabla().then(function (acciones) {
        acciones.forEach(function (accion) {
          var accionEncontrada = undefined;
          columnaAcciones.acciones.forEach(function (accionExistente) {
            if (accionExistente.accion === accion.accion) {
              accionEncontrada = accionExistente;
            }
          });
          if (!accionEncontrada) {
            columnaAcciones.acciones.push(accion);
          }
        });
        return columnas;
      });
    }

  }

})();
