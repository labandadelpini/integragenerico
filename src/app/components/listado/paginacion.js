(function () {
  'use strict';

  angular
    .module('integragenerico').provider('Paginacion', PaginacionProvider);

  function PaginacionProvider() {

    var configuracion = {
      page: {
        page: 1,
        count: 25
      },
      bloquesMinimos: 2,
      bloquesMaximos: 3
    };

    this.configuracion = configuracion;

    var provider = {};
    provider.generarPaginacion = generarPaginacion;
    provider.getParametros = getParametros;
    provider.getBloquesMinimos = getBloquesMinimos;
    provider.getBloquesMaximos = getBloquesMaximos;

    function generarPaginacion(query, page, count) {
      query.page = page - 1;
      query.size = count;
      query.pagination = true;
    }

    function getParametros(clave) {
      return configuracion.page;
    }

    function getBloquesMinimos(clave) {
      return configuracion.bloquesMinimos;
    }

    function getBloquesMaximos(clave) {
        return configuracion.bloquesMaximos;
      }

    this.$get = function () {
      return provider;
    };
  }

})();
