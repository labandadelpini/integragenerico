(function () {
  'use strict';

  angular
    .module('integragenerico').service('Filtro', Filtro);

  function Filtro() {

    var vm = this;

    vm.getFiltro = getFiltro;

    vm.generarFiltros = generarFiltros;

    vm.getDelay = getDelay;

    function filtrar(lista) {
      lista.filtro.filtro = {};
      lista.cols.forEach(function (col) {
        if (col.filter && (!col.tipo || col.tipo === 'celda-estatica')) {
          if (col.filterField) {
            lista.filtro.filtro[col.filterField] = lista.filtro.texto;
          } else {
            lista.filtro.filtro[col.field] = lista.filtro.texto;
          }
        }
      });
      lista.tableParams.reload();
    }

    function generarFiltros(query, filter, filtroGeneral) {
      for (var key in filtroGeneral) {
        if (filtroGeneral.hasOwnProperty(key)) {
          query[key] = filtroGeneral[key];
        }
      }
      var banderaFiltroParticular = false;
      for (key in filter) {
        if (filter.hasOwnProperty(key) && filter[key]) {
          query[key] = filter[key];
          banderaFiltroParticular = true;
        }
      }
      if (banderaFiltroParticular) {
        query.strategy = 'AND';
      }
    }

    function getDelay() {
      return 1000;
    }

    function getFiltro(lista) {
      return {
        texto: '',
        claseBoton: 'btn btn-primary',
        filtro: {},
        filtrando: false,
        campos: [{
          key: 'texto',
          type: 'horizontalInput',
          className: '',
          id: 'buscador-generico',
          templateOptions: {
            label: 'Filtro ',
            placeholder: 'Ingrese su búsqueda...',
            type: 'text',
            ancho: 'col-sm-11',
            anchoLabel: 'col-sm-1',
            maxlength: 250,
            addonRight: {
              class: 'fa fa-close',
              onClick: function () {
                lista.filtro.texto = '';
              }
            }
          },
          watcher: {
            listener: function (field, newValue, oldValue) {
              if (newValue !== oldValue) {
                filtrar(lista);
              }
            }
          },
          modelOptions: {
            debounce: getDelay()
          }
        }]
      };
    }
  }

})();
