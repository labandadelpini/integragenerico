(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
	angular.module('integragenerico')
	.directive('longitudTelefono', longitudTelefono);

	function longitudTelefono() {
		return {
        	restrict: 'A',
        	require: '^form',
        	link: function(scope, element, attrs, parentFormCtrl) {

        		var longitudTelefono

        		scope.$watch(attrs.longitudTelefono, function(newValue) {
        			longitudTelefono = newValue;
        			validar();
        		});

        		scope.$watch('vm.form.prefijo.$viewValue', function(newValue) {
        			validar();
        		})

        		scope.$watch('vm.form.sufijo.$viewValue', function(newValue) {
        			validar();
        		})

        		function validar() {
        			var validez = esValido();
        			if (parentFormCtrl.prefijo) {
        				parentFormCtrl.prefijo.$setValidity('longitudTelefono', validez);
        			}
        			if (parentFormCtrl.sufijo) {
        				parentFormCtrl.sufijo.$setValidity('longitudTelefono', validez);
        			}
        		}

        		function esValido() {
        			if (!scope.vm || !scope.vm.form || !scope.vm.form.prefijo || !scope.vm.form.sufijo) return true;
        			var longitudPrefijo = scope.vm.form.prefijo.$viewValue ? scope.vm.form.prefijo.$viewValue.length : 0;
        			var longitudSufijo = scope.vm.form.sufijo.$viewValue ? scope.vm.form.sufijo.$viewValue.length : 0;
        			return (longitudPrefijo + longitudSufijo === longitudTelefono) || scope.vm.form.prefijo.$pristine || scope.vm.form.sufijo.$pristine;
        		}
        	}
    	};
	}

})();
