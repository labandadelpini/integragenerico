(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name bolsaEmpleo.controller:AboutCtrl
	* @description
	* # AboutCtrl
	* Controller of the bolsaEmpleo
	*/
	angular.module('integragenerico')
	.controller('InputTelefonoCtrl', InputTelefonoCtrl);

	function InputTelefonoCtrl($scope, orderByFilter, TipoTelefono, CodigoInternacional) {

		var vm = this;

		vm.seleccionarTipo = seleccionarTipo;

		var codigoInternacional;

		iniciar();

		$scope.$watch(getModelo, completarTelefono);

		$scope.$watch(getModelo, actualizarFormatos, true);

		function getModelo() {
			return vm.model;
		}

		function completarTelefono() {
			vm.telefono = vm.model;
			if (!vm.telefono) {
				vm.telefono = {};
				vm.model = vm.telefono;
			}
			completarTipoTelefono();
			completarCodigoInternacional();
		}

		function completarTipoTelefono() {
			if (!vm.telefono.tipo && vm.tipos) {
				seleccionarTipo(vm.tipos[0]);
			}
		}

		function completarCodigoInternacional() {
			if (!vm.telefono.codigoInternacional && codigoInternacional) {
				seleccionarCodigoInternacional(codigoInternacional);
			}
		}

		function actualizarFormatos() {
			if (vm.telefono && vm.telefono.tipo) {
				vm.telefono.formatoHumano =
				(vm.telefono.tipo.maximoPrefijo ? '('
				+ vm.telefono.tipo.prefijoHumano + vm.telefono.prefijo + ')' : '')
				+ vm.telefono.tipo.divisorHumano + vm.telefono.sufijo;
				vm.telefono.formatoInternacional = vm.telefono.tipo.prefijoInternacional
				+ vm.telefono.prefijo + vm.telefono.sufijo;
			}
		}

		function iniciar() {
			CodigoInternacional.query({codigo: '+54'},function(codigos) {
				codigoInternacional = codigos[0];
			});
			TipoTelefono.query(function(tipos) {
				vm.tipos = orderByFilter(tipos,'orden');
				completarTipoTelefono();
			});
		}

		function seleccionarTipo(tipo) {
			vm.telefono.tipo = tipo;
			vm.menuVisible = false;
		}

		function seleccionarCodigoInternacional(codigoInternacional) {
			vm.telefono.codigoInternacional = codigoInternacional;
		}

		vm.mostrarMenu = function() {
      vm.menuVisible = !vm.menuVisible;
    };

	}

})();
