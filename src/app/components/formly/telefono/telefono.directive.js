(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
	angular.module('integragenerico')
	.directive('inputTelefono', inputTelefono);

	function inputTelefono() {
		return {
			restrict: 'E',
			templateUrl: 'integragenerico/components/formly/telefono/telefono.html',
			controller: 'InputTelefonoCtrl as vm',
			bindToController: true,
			scope: {
				model: '=ngModel',
				options: '='
			}
		};
	}

})();
