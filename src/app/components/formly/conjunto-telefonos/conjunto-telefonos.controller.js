(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name bolsaEmpleo.controller:AboutCtrl
	* @description
	* # AboutCtrl
	* Controller of the bolsaEmpleo
	*/
	angular.module('integragenerico')
	.controller('ConjuntoTelefonosCtrl', ConjuntoTelefonosCtrl);

	function ConjuntoTelefonosCtrl($scope) {

		var vm = this;

		vm.agregarTelefono = agregarTelefono;

		vm.quitarTelefono = quitarTelefono;

		$scope.$watch(angular.bind(vm, function () {
			return vm.telefonos;
		}), function() {
			if (!vm.telefonos) {
				vm.telefonos = [];
			}
		});

		function agregarTelefono() {
			vm.telefonos.push({});
		}

		function quitarTelefono(index) {
			vm.telefonos.splice(index,1);
		}

	}

})();
