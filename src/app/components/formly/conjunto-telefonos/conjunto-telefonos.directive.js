(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
	angular.module('integragenerico')
	.directive('conjuntoTelefonos', conjuntoTelefonos);

	function conjuntoTelefonos() {
		return {
			restrict: 'E',
			templateUrl: 'integragenerico/components/formly/conjunto-telefonos/conjunto-telefonos.html',
			controller: 'ConjuntoTelefonosCtrl as vm',
			bindToController: true,
			scope: {
				telefonos: '=ngModel',
				options: '='
			}
		};
	}

})();
