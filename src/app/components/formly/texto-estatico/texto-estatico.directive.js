'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
 angular.module('integragenerico')
 .directive('textoEstatico', function() {
 	return {
 		restrict: 'A',
 		link: function(scope, element, attributes) {
 			attributes.$set('ngBind', attributes.ngModel);
 		}
 	};
 });
