(function() {
	'use strict';

	angular.module('integragenerico')
	.directive('inputDireccion', inputDireccion);

	function inputDireccion() {
		return {
			restrict: 'E',
			templateUrl: 'integragenerico/components/formly/direccion/direccion.html',
			controller: 'InputDireccionCtrl as vm',
			bindToController: true,
			scope: {
				model: '=ngModel',
				options: '='
			}
		};
	}

})();
