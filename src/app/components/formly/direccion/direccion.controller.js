(function() {
  'use strict';

  angular.module('integragenerico')
  .controller('InputDireccionCtrl', InputDireccionCtrl);

  function InputDireccionCtrl($scope, orderByFilter, LugarGeografico, TipoLugarGeografico) {

    var vm = this;

    vm.jerarquia = {};

    vm.titulo = 'Dirección';

    vm.paises = [];
    console.log(vm.model);

    var tipos = TipoLugarGeografico.query();

    LugarGeografico.query({'tipo.nombre': 'País'}).$promise.then(function(data) {
      var paises = orderByFilter(data,'nombre');
      paises.push({
        id: 0,
        nombre: 'otro'
      });
      vm.paises.length = 0;
      angular.copy(paises,vm.paises);
    });

    vm.provincias = [];

    vm.departamentos = [];

    vm.localidades = [];

    $scope.$watch(function() {
      return vm.model.lugar;
    }, actualizarLugarSeleccionado);

    $scope.$watch(function() {
      return vm.jerarquia.pais;
    }, buscarProvincias);

    $scope.$watch(function() {
      return vm.jerarquia.provincia;
    }, buscarDepartamentos);

    $scope.$watch(function() {
      return vm.jerarquia.departamento;
    }, buscarLocalidades);

    $scope.$watch(function() {
      return vm.jerarquia.localidad;
    }, actualizarLugarGeografico);

    $scope.$watch(function() {
      return vm.jerarquia.nuevoPais;
    }, actualizarLugarGeografico);

    $scope.$watch(function() {
      return vm.jerarquia.nuevoDepartamento;
    }, actualizarLugarGeografico);

    $scope.$watch(function() {
      return vm.jerarquia.nuevaLocalidad;
    }, actualizarLugarGeografico);

    function getTipo(nombre) {
      var tipo;
      tipos.forEach(function(elemento) {
        if (elemento.nombre === nombre) {
          tipo = elemento;
        }
      });
      return tipo;
    }

    function actualizarLugarSeleccionado() {
      if (vm.model.lugar.lugar) {
          if (vm.model.lugar.lugar.tipo.nombre === 'Localidad') {
            vm.jerarquia.pais = vm.model.lugar.lugar.padre.padre.padre;
            vm.jerarquia.provincia = vm.model.lugar.lugar.padre.padre;
            vm.jerarquia.departamento = vm.model.lugar.lugar.padre;
            vm.jerarquia.localidad = vm.model.lugar.lugar;
            // console.log(JSON.stringify(vm.jerarquia.provincia));
          }
      }
    }

    function actualizarLugarGeografico() {
      if (!vm.model) return;
      if (!vm.model.lugar) {
        vm.model.lugar = {};
      }
      if (vm.jerarquia.localidad && vm.jerarquia.localidad.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.localidad.nombre;
        vm.model.lugar.lugar = vm.jerarquia.localidad;
        vm.model.lugar.tipo = vm.jerarquia.localidad.tipo;
        vm.model.lugar.padreOriginal = vm.jerarquia.localidad.padre;
        vm.model.lugar.padre = null;
      } else if (vm.jerarquia.departamento && vm.jerarquia.departamento.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = vm.jerarquia.departamento;
        vm.model.lugar.padre = null;
      } else if (vm.jerarquia.provincia && vm.jerarquia.provincia.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = null;
        vm.model.lugar.padre = {
          nombreLugar: vm.jerarquia.nuevoDepartamento,
          lugar: null,
          tipo: getTipo('Departamento'),
          padreOriginal: vm.jerarquia.provincia,
          padre: null
        };
      } else if (vm.jerarquia.pais && vm.jerarquia.pais.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = vm.jerarquia.pais;
        vm.model.lugar.padre = null;
      } else {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = null;
        vm.model.lugar.padre = {
          nombreLugar: vm.jerarquia.nuevoPais,
          lugar: null,
          tipo: getTipo('País'),
          padreOriginal: null,
          padre: null
        };
      }
    }

    function buscarProvincias() {
      if (vm.jerarquia.pais && vm.jerarquia.pais.tieneHijos) {
        var provincias = LugarGeografico.query({'padre.id': vm.jerarquia.pais.id});
        provincias.$promise.then(function(data) {
          provincias = orderByFilter(data,'nombre');
          if (vm.jerarquia.pais.crearHijos) {
            provincias.push({
              id: 0,
              nombre: 'otro'
            });
          }
          angular.copy(provincias,vm.provincias);
          actualizarLugarGeografico();
        });
      } else {
        vm.jerarquia.provincia = undefined;
        vm.provincias.length = 0;
      }
    }

    function buscarDepartamentos() {
      if (vm.jerarquia.provincia && vm.jerarquia.provincia.tieneHijos) {
        var departamentos = LugarGeografico.query({'padre.id': vm.jerarquia.provincia.id});
        departamentos.$promise.then(function(data) {
          departamentos = orderByFilter(data,'nombre');
          if (vm.jerarquia.provincia.crearHijos) {
            departamentos.push({
              id: 0,
              nombre: 'otro'
            });
          }
          angular.copy(departamentos,vm.departamentos);
          actualizarLugarGeografico();
        });
      } else {
        vm.jerarquia.departamento = undefined;
        vm.departamentos.length = 0;
      }
    }

    function buscarLocalidades() {
      if (vm.jerarquia.departamento && vm.jerarquia.departamento.tieneHijos) {
        var localidades = LugarGeografico.query({'padre.id': vm.jerarquia.departamento.id});
        localidades.$promise.then(function(data) {
          localidades = orderByFilter(data,'nombre');
          if (vm.jerarquia.departamento.crearHijos) {
            localidades.push({
              id: 0,
              nombre: 'otro'
            });
          }
          angular.copy(localidades,vm.localidades);
          actualizarLugarGeografico();
        });
      } else {
        vm.jerarquia.localidad = undefined;
        vm.localidades.length = 0;
      }
    }

    vm.campos = [
      {
        key: 'calle',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Calle:',
          placeholder: 'Calle',
          required: true
        }
      },
      {
        key: 'numero',
        type: 'horizontalInput',
        templateOptions: {
          type: 'number',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Número:',
          placeholder: 'Número',
          required: true
        }
      },
      {
        key: 'piso',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Piso:',
          placeholder: 'Piso'
        }
      },
      {
        key: 'departamento',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Departamento:',
          placeholder: 'Departamento'
        }
      },
      {
        key:'pais',
        type:'horizontalSelect',
        model: vm.jerarquia,
        templateOptions:{
          label:'País:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true,
          options: vm.paises,
          ngOptions:'pais as pais.nombre for pais in to.options track by pais.id'
        }
      },
      {
        key:'nuevoPais',
        type:'horizontalInput',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.pais && !vm.jerarquia.pais.id);
        },
        templateOptions:{
          label:'Nombre de País:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true
        }
      },
      {
        key:'provincia',
        type:'horizontalSelect',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.pais && vm.jerarquia.pais.id && vm.jerarquia.pais.tieneHijos);
        },
        templateOptions:{
          label:'Provincia:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true,
          options: vm.provincias,
          ngOptions:'provincia as provincia.nombre for provincia in to.options track by provincia.id'
        }
      },
      {
        key:'departamento',
        type:'horizontalSelect',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.provincia && vm.jerarquia.provincia.id && vm.jerarquia.provincia.tieneHijos);
        },
        templateOptions:{
          label:'Departamento/Partido:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true,
          options: vm.departamentos,
          ngOptions:'departamento as departamento.nombre for departamento in to.options track by departamento.id'
        }
      },
      {
        key:'nuevoDepartamento',
        type:'horizontalInput',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.departamento && !vm.jerarquia.departamento.id);
        },
        templateOptions:{
          label:'Nombre de Partido/Departamento:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true
        }
      },
      {
        key:'localidad',
        type:'horizontalSelect',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.departamento && vm.jerarquia.departamento.id && vm.jerarquia.departamento.tieneHijos);
        },
        templateOptions:{
          label:'Localidad:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true,
          options: vm.localidades,
          ngOptions:'localidad as localidad.nombre for localidad in to.options track by localidad.id'
        }
      },
      {
        key:'nuevaLocalidad',
        type:'horizontalInput',
        model: vm.jerarquia,
        hideExpression: function() {
          if (vm.jerarquia.pais && !vm.jerarquia.pais.id) return false;
          if (vm.jerarquia.provincia && !vm.jerarquia.provincia.id) return false;
          if (vm.jerarquia.departamento && !vm.jerarquia.departamento.id) return false;
          return !(vm.jerarquia.localidad && !vm.jerarquia.localidad.id);
        },
        templateOptions:{
          label:'Nombre de Localidad:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true
        }
      }
    ];

    $scope.$watch('vm.options.templateOptions.disabled', function(newValue){
      vm.campos.forEach(function(campo){
        if (!campo.templateOptions) {
          campo.templateOptions = {};
        }
        campo.templateOptions.disabled = newValue;
      });
    });



  }

})();
