(function () {
  'use strict';

  angular.module('integragenerico')
    .controller('InputDireccionNuevaCtrl', InputDireccionNuevaCtrl);

  function InputDireccionNuevaCtrl($scope, orderByFilter, ServicioListasRelacionadas, TipoLugarGeografico) {

    var vm = this;

    vm.jerarquia = {};

    vm.titulo = 'Dirección Nuevo';

   
    // $scope.$watch(function () {
    //   return vm.model.lugar;
    // }, actualizarLugarSeleccionado);

    // $scope.$watch(function () {
    //   return vm.jerarquia.localidad;
    // }, actualizarLugarGeografico);



    function actualizarLugarGeografico() {
      if (!vm.model) return;
      if (!vm.model.lugar) {
        vm.model.lugar = {};
      }
      if (vm.jerarquia.localidad && vm.jerarquia.localidad.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.localidad.nombre;
        vm.model.lugar.lugar = vm.jerarquia.localidad;
        vm.model.lugar.tipo = vm.jerarquia.localidad.tipo;
        vm.model.lugar.padreOriginal = vm.jerarquia.localidad.padre;
        vm.model.lugar.padre = null;
      } else if (vm.jerarquia.departamento && vm.jerarquia.departamento.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = vm.jerarquia.departamento;
        vm.model.lugar.padre = null;
      } else if (vm.jerarquia.provincia && vm.jerarquia.provincia.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = null;
        vm.model.lugar.padre = {
          nombreLugar: vm.jerarquia.nuevoDepartamento,
          lugar: null,
          tipo: getTipo('Departamento'),
          padreOriginal: vm.jerarquia.provincia,
          padre: null
        };
      } else if (vm.jerarquia.pais && vm.jerarquia.pais.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = vm.jerarquia.pais;
        vm.model.lugar.padre = null;
      } else {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = null;
        vm.model.lugar.padre = {
          nombreLugar: vm.jerarquia.nuevoPais,
          lugar: null,
          tipo: getTipo('País'),
          padreOriginal: null,
          padre: null
        };
      }
    }

   

    vm.campos = [
      {
        key: 'calle',
        id: 'calle',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho: 'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Calle:',
          placeholder: 'Calle',
          requiredPadre: true
        }
      },
      {
        key: 'numero',
        id: 'numero',
        type: 'horizontalInput',
        templateOptions: {
          type: 'number',
          ancho: 'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Número:',
          placeholder: 'Número',
          requiredPadre: true
        }
      },
      {
        key: 'piso',
        id: 'piso',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho: 'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Piso:',
          placeholder: 'Piso'
        }
      },
      {
        key: 'departamento',
        id: 'departamento',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho: 'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Departamento:',
          placeholder: 'Departamento'
        }
      }, {
        key: "localidad",
        id : "localidad",
        type: "horizontalTypeahead",
        data: {
          lista: "lazy",
          servicio: "Localidad"
        },
        templateOptions: {
          label: "Localidad:",
          placeholder: "",
          ancho: "col-sm-8",
          anchoLabel: "col-sm-4",
          propiedad: "nombreCompletoMostrar",
          propiedadFiltro: ["nombre", "codigoPostal"],
          ngOptions: "item as item[to.propiedad] for item in to.funcion($viewValue) | limitTo:8",
          espera: 300,
          permitirNuevo : false,
          asincrono: true,
          requiredPadre: true
        }
      }
    ];

    var formulario = {campos : vm.campos};
    ServicioListasRelacionadas.completarListasRelacionadas(formulario);

    $scope.$watch('vm.options.templateOptions.disabled', function (newValue) {
      vm.campos.forEach(function (campo) {
        if (!campo.templateOptions) {
          campo.templateOptions = {};
        }
        campo.templateOptions.disabled = newValue;
      });
    });

    $scope.$watch('vm.options.templateOptions.disabled', function (newValue) {
      vm.campos.forEach(function (campo) {
        if (!campo.templateOptions) {
          campo.templateOptions = {};
        }
        campo.templateOptions.disabled = newValue;
      });
    });

    $scope.$watch('vm.options.templateOptions.required', function (newValue) {
      vm.campos.forEach(function (campo) {
        if (campo.templateOptions && campo.templateOptions.requiredPadre) {
          campo.templateOptions.required = newValue;
        }
      });
    });



  }

})();
