(function() {
	'use strict';

	angular.module('integragenerico')
	.directive('inputDireccionNueva', inputDireccionNueva);

	function inputDireccionNueva() {
		return {
			restrict: 'E',
			templateUrl: 'integragenerico/components/formly/direccion-nueva/direccion-nueva.html',
			controller: 'InputDireccionNuevaCtrl as vm',
			bindToController: true,
			scope: {
				model: '=ngModel',
				options: '='
			}
		};
	}

})();
