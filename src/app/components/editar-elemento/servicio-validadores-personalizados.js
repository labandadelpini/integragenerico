(function() {
	'use strict';

	angular
	.module('integragenerico').service('ServicioValidadoresPersonalizados', ServicioValidadoresPersonalizados);

	function ServicioValidadoresPersonalizados($injector, $q) {

		var vm = this;

		vm.agregarValidadores = agregarValidadores;

		function agregarValidadores(campos) {
			campos.forEach(function(campo){
				agregarValidador(campo);
			});
		}

		function agregarValidador(campo) {
			//Si no existe creo
			if (!campo.asyncValidators) {
				campo.asyncValidators = {};
			}
			//pregunto si tiene validadores
			if (campo.templateOptions) {
				agregarValidadorUnico(campo);
			}
		}

		function agregarValidadorUnico(campo) {
			if (!campo.templateOptions.unico){
				return;	
			} 

			campo.asyncValidators.unico = {
				expression : function($viewValue, $modelValue){
					var valor = $modelValue || $viewValue;
					var servicio = $injector.get(campo.templateOptions.unico.servicio);
					var promesa =  servicio.unico({'codigo':valor}).$promise.then( 
						function(data){
							if (!data.estado) {
								return $q.reject('Repetido');
							}else {
								return data.estado;
							}
						});
					return promesa;
				},
				message: campo.templateOptions.unico.mensaje
			};
		}
	} 

})();