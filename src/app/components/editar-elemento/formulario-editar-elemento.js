(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
	angular.module('integragenerico')
	.directive('formularioEditarElemento', formularioEditarElemento);

	function formularioEditarElemento(EditarElementosTemplate) {
		return {
			restrict: 'E',
			templateUrl: EditarElementosTemplate.getTemplate(),
			scope: {
				formulario: '='
			}
		};
	}

})();
