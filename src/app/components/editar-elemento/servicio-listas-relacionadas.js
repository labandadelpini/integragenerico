(function () {
    'use strict';

    angular
        .module('integragenerico').service('ServicioListasRelacionadas', ServicioListasRelacionadas);

    function ServicioListasRelacionadas($q, $timeout, $injector, filterFilter) {

        var vm = this;

        vm.completarListasRelacionadas = completarListasRelacionadas;

        function completarListasRelacionadas(formulario) {
            var campos = formulario.campos;
            var promesas = [];
            campos.forEach(function (campo) {
                if (campo.data && campo.data.lista === 'eager') {
                    var query = {};
                    // console.log(campo.data);
                    if (campo.data.sort) {
                        query.sort = campo.data.sort;
                    }
                    if (campo.data.size) {
                        query.page = 0;
                        query.pagination = true;
                        query.size = campo.data.size;
                    }
                    var servicio = $injector.get(campo.data.servicio);
                    var resultados = [];
                    if (query.sort) {
                        resultados = servicio.get(query, function (rta) {
                            var data = rta.content;
                            //Si tiene elementos, seteo el primero x defecto
                            if (formulario.modo === 'nuevo' && !campo.data.isArray && data.length > 0) {
                                formulario.elemento[campo.key] = data[0];
                            }
                            campo.templateOptions.options = data;
                            return data;
                        });
                    } else {
                        resultados = servicio.query(function (data) {
                            //Si tiene elementos, seteo el primero x defecto
                            if (formulario.modo === 'nuevo' && !campo.data.isArray && data.length > 0) {
                                formulario.elemento[campo.key] = data[0];
                            }
                            campo.templateOptions.options = data;
                            return data;
                        });
                    }

                    campo.templateOptions.funcion = function (query) {
                        var filtro = {};
                        filtro[campo.templateOptions.propiedadFiltro] = query;
                        return filterFilter(resultados, query);
                    }
                    promesas.push(resultados.$promise);
                } else if (campo.data && campo.data.lista === 'lazy') {
                    var servicio = $injector.get(campo.data.servicio);
                    campo.templateOptions.funcion = function (filtro) {
                        var query = {};
                        if (Array.isArray(campo.templateOptions.propiedadFiltro)) {
                            campo.templateOptions.propiedadFiltro.forEach(function (filtroIndividual) {
                                query[filtroIndividual] = filtro;
                            });
                        } else {
                            query[campo.templateOptions.propiedadFiltro] = filtro;
                        }
                        return servicio.query(query).$promise;
                    };
                }
            });
            promesas.push($timeout(function () {
                return true;
            }));
            return $q.all(promesas).then(function () {
                //console.log(formulario);
                return formulario;
            });
        }

    }

})();
