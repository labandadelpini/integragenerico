(function () {
  'use strict';

  angular
    .module('integragenerico').service('ServicioEdicion', ServicioEdicion);

  function ServicioEdicion($window, $q, $injector, CamposEditar, ServicioListasRelacionadas, Dialogos, TransformacionString, ServicioValidadoresPersonalizados, ServicioPatrones, ConfigurarConfirmaciones) {

    var vm = this;

    vm.getFormulario = getFormulario;

    function Formulario(clave, id, soloLectura, modalInstance) {
      var promesas = [];
      var formulario = {
        opciones: {},
        soloLectura: soloLectura
      };
      if (modalInstance) {
        formulario.modalInstance = modalInstance;
      }

      formulario.servicio = $injector.get(clave);
      formulario.guardar = guardar;
      formulario.cancelar = cancelar;
      formulario.volver = volver;
      formulario.prefijo = TransformacionString.camelToUnderscore(clave);
      if (!id || id === 'nuevo') {
        formulario.modo = 'nuevo';
        formulario.elemento = {};
        formulario.titulo = formulario.prefijo + '.NUEVO.TITULO';
      } else {
        formulario.modo = 'editar';
        formulario.elemento = formulario.servicio.get({
          codigo: id
        });
        promesas.push(formulario.elemento.$promise);
        if (soloLectura) {
          formulario.titulo = formulario.prefijo + '.CONSULTAR.TITULO';
        } else {
          formulario.titulo = formulario.prefijo + '.EDITAR.TITULO';
        }
      }
      promesas.push(CamposEditar.getCampos(clave).then(function (data) {
        formulario.campos = data;
        ServicioValidadoresPersonalizados.agregarValidadores(formulario.campos);
        //Consultar
        if (soloLectura) {
          formulario.campos.forEach(function (campo) {
            if (!campo.templateOptions) {
              campo.templateOptions = {};
            }
            campo.templateOptions.disabled = true;
          });
        }
        return formulario;
      }).then(ServicioListasRelacionadas.completarListasRelacionadas).then(
        ServicioPatrones.agregarPatrones));

      return $q.all(promesas).then(function () {
        return formulario;
      });
    }

    function guardar(formulario) {
      var prefijo = formulario.prefijo + (formulario.modo === 'nuevo' ? '.DIALOGO_GUARDAR' : '.DIALOGO_ACTUALIZAR');
      if (formulario.form.$valid) {
        if (ConfigurarConfirmaciones.alGuardar) {
          Dialogos.abrirDialogoConfirmacion({
            prefijo: prefijo,
            tieneTitulo: true,
            cancelar: true
          }).result.then(abrirDialogoCargando).then(function () {
            guardarObjeto(formulario);
          });
        } else {
          guardarObjeto(formulario);
        }

      } else {
        Dialogos.abrirDialogoInformacion({
          prefijo: prefijo,
          tieneTitulo: true,
          textoDinamico: 'Complete los campos requeridos'
        });
      }

    }

    function cancelar(formulario) {
      var prefijo = formulario.prefijo + (formulario.modo === 'nuevo' ? '.DIALOGO_CANCELAR_NUEVO' : '.DIALOGO_CANCELAR_EDICION');
      Dialogos.abrirDialogoConfirmacion({
        prefijo: prefijo,
        tieneTitulo: true,
        cancelar: true
      }).result.then(abrirDialogoCargando).then(function() {
        volver(formulario);
      });
    }



    function abrirDialogoCargando() {
      return Dialogos.abrirDialogoTransicionEstados();
    }

    function guardarObjeto(formulario) {
      abrirDialogoCargando();
      if (formulario.modo === 'editar') {
        formulario.servicio.update(formulario.elemento, function () {
          volver(formulario);
        });
      } else if (formulario.modo === 'nuevo') {
        formulario.servicio.save(formulario.elemento, function () {
          volver(formulario);
        });
      }

    }

    function volver(formulario) {
      if (formulario && formulario.modalInstance && formulario.modalInstance.modalInstance) {
        formulario.modalInstance.modalInstance.close();
      } else {
        $window.history.back();
      }

    }

    function getFormulario(clave, id, soloLectura, modalInstance) {
      var form = new Formulario(clave, id, soloLectura, modalInstance);
      return form;
    }

  }

})();
