(function() {
    'use strict';

    angular
    .module('integragenerico').service('ServicioImagen', ServicioImagen);

    function ServicioImagen() {

        var vm = this;

        vm.seperarImagen = separarImagen;

        function separarImagen(objeto) {
            if (objeto) {
                var particion = objeto.split(';');
                var resultado = {};
                resultado.imagen = particion[1].split(',')[1];
                resultado.tipo = particion[0].split(':')[1];
                return resultado;   
            }
            return {};
        }

    }

})();