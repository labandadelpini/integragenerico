'use strict';

angular.module('integragenerico')
.service('ServicioPatrones', function ($q, Patrones) {

	this.agregarPatrones = agregarPatrones;
	this.agregarPatron = agregarPatron;

	function agregarPatrones(formulario) {
		var campos = formulario.campos;
		var promesas = [];
		campos.forEach(function(campo){
			var respuesta = agregarPatron(campo);
			if(respuesta) {
				promesas.push(respuesta);
			}
		});
		return $q.all(promesas).then(function(){
			return formulario;
		});
	}


	function agregarPatron(campo) {
		if (campo.templateOptions.patron) {
			return Patrones.getPatron(campo.templateOptions.patron.tipo).then(function(respuesta) {
				campo.templateOptions.pattern = respuesta.patron;
				campo.templateOptions.patternValidationMessage = respuesta.mensaje;
			});
		}
	}
});