(function() {
    'use strict';

    angular
    .module('integragenerico.seguridad')
    .factory('Usuario', Usuario);

    function Usuario($resource, URL_SERVIDOR) {
        var Usuario = $resource(URL_SERVIDOR + 'usuarios/:codigo', {
            codigo: '@id'
        }, {
            invertirEstado: {
                url: URL_SERVIDOR + 'usuarios/:codigo/invertir-estado',
                method: 'PUT'
            },
            cambiarPassword: {
                url: URL_SERVIDOR + 'usuarios/cambiar-password',
                method: 'PUT'
            },
            cambiarClave : {
              url: URL_SERVIDOR + 'usuarios/actual/cambiar-password',
              method: 'POST'
            },
            getImagen: {
                url: URL_SERVIDOR + 'usuarios/:codigo/imagen',
                method: 'GET'
            },
            login: {
                url: URL_SERVIDOR + '/usuarios/actual',
                method: 'GET'
            }
        });

        Usuario.prototype.iconoEstado = function() {
            return (this.habilitado !== undefined && this.habilitado) ? 'fa fa-check' : 'fa fa-ban';
        };

        return Usuario;
    }
})();
