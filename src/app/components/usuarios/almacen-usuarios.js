(function() {
  'use strict';

  angular
  .module('integragenerico.seguridad')
  .service('AlmacenUsuarios', AlmacenUsuarios);
/**
 * @ngdoc service
 * @name negocioGenerico.ServicioAlmacenUsuarios
 * @description
 * # ServicioAlmacenUsuarios
 * Service in the negocioGenerico.
 */
 function AlmacenUsuarios ($base64, $localStorage, $sessionStorage) {

  var getAutenticacion = function() {
    if ($sessionStorage.autenticacion) {
      return $sessionStorage.autenticacion;
    }
    if ($localStorage.autenticacion) {
      return $localStorage.autenticacion;
    }
    return undefined;
  };

  var guardar = function(autenticacion, recordar) {
    if (recordar) {
      $localStorage.autenticacion = autenticacion;
    }else {
      $sessionStorage.autenticacion = autenticacion;
    }
  };

  this.limpiar = function () {
    delete $localStorage.autenticacion;
    delete $sessionStorage.autenticacion;
  };

  this.guardarAutenticacion = function (nombreUsuario, password, recordar) {
    if (nombreUsuario && password) {
      var autenticacion = {};
      autenticacion.nombreUsuario = nombreUsuario;
      autenticacion.password = password;
      autenticacion.encabezado = 'Basic ' + $base64.encode(nombreUsuario + ':' + password);
      guardar(autenticacion, recordar);
    }
  };

  this.guardarUsuario = function (usuario) {
    if (usuario) {
      $sessionStorage.usuario = usuario;
    }
  };

  this.getUsuario = function () {
    return $sessionStorage.usuario;
  };

  this.getAutenticacion = function () {
    return getAutenticacion().authdata;
  };

  this.getAutenticacionBasica = function() {
    if (!validarAutenticacionBasica()) {
      return undefined;
    }
    return getAutenticacion().encabezado;
  };

  var validarAutenticacionBasica = function() {
    return getAutenticacion() && getAutenticacion().encabezado;
  };

  this.usuarioActualizado = function() {
    return !!$sessionStorage.usuario;
  };

  this.usuarioLogueado = function() {
    return getAutenticacion() && getAutenticacion().encabezado;
  };

  this.tienePermiso = function(permiso) {
                var respuesta = false;
                var usuario = this.getUsuario();
                if (usuario !== undefined && usuario.roles !== undefined) {
                    usuario.roles.forEach(function(rol) {
                        rol.permisos.forEach(function(permisoRol) {
                            if (permiso === permisoRol.nombre || permiso.indexOf(permisoRol.nombre) !== -1) {
                                respuesta = true;
                            }
                        });
                    });
                }
                return respuesta;
            }
}

})();
