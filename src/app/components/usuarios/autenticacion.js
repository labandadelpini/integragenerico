(function () {
    'use strict';

    angular
        .module('integragenerico.seguridad')
        .service('Autenticacion1', Autenticacion);

    function Autenticacion($q, AlmacenUsuarios, Usuario) {

        this.login = function (nombreUsuario, password, recordar) {
            AlmacenUsuarios.guardarAutenticacion(nombreUsuario, password, recordar);
            return actualizarUsuario().$promise;
        };

        var actualizarUsuario = function () {
            return Usuario.login(function (usuario) {
                AlmacenUsuarios.guardarUsuario(usuario);
                AlmacenUsuarios.getUsuario().avatar = Usuario.getImagen(usuario.id);
                return usuario;
            }, function (error) {
                AlmacenUsuarios.limpiar();
                return $q.reject('Error de login');
            });
        };

        this.tienePermiso = function (permiso) {
            return AlmacenUsuarios.tienePermiso(permiso);
        };

        this.tienePermisos = function (permisos) {
            var autorizado = false;
            permisos.forEach(function (permiso) {
                if (AlmacenUsuarios.tienePermiso(permiso)) {
                    autorizado = true;
                }
            });
            return autorizado;
        };

        this.validarUsuario = function () {
            return actualizarUsuario().$promise;
        };
    }
})();
