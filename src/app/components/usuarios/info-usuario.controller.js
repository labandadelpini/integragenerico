(function() {
  'use strict';

  angular
    .module('integragenerico.seguridad')
    .controller('InfoUsuarioCtrl', InfoUsuarioCtrl);

  /** @ngInject */
  function InfoUsuarioCtrl(AlmacenUsuarios, $timeout, $state, $modal) {
    var vm = this;

    vm.cerrarSesion = function() {
      AlmacenUsuarios.limpiar();
      $timeout(function() {
        $state.go('login');
      });
    };

    vm.cambiarPassword = function() {
      var modalInstance = $modal.open({
        templateUrl: 'integragenerico/components/usuarios/cambiar-pass/cambiar-clave.html',
        controller: 'CambiarClaveCtrl',
        size: 'lg',
        resolve: {
          objetoModificando: function() {
            return AlmacenUsuarios.getUsuario();
          }
        }
      });
    }

    vm.menuVisible = false;

    vm.getImagen = function() {
      if (!AlmacenUsuarios.getUsuario().id || !AlmacenUsuarios.getUsuario().avatar) {
        return undefined;
      } else {
        // console.log(AlmacenUsuarios.getUsuario().avatar);
        return "data:" + AlmacenUsuarios.getUsuario().avatar.tipoImagen + ";base64," + AlmacenUsuarios.getUsuario().avatar.imagen;
      }
    };

    vm.getUsuario = function() {
      return AlmacenUsuarios.getUsuario();
    };

    vm.mostrarMenu = function() {
      vm.menuVisible = !vm.menuVisible;
    };


  }
})();
