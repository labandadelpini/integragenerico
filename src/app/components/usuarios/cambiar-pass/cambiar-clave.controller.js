'use strict';

/**
* @ngdoc function
* @name bolsaEmpleo.controller:DatosDemandanteCtrl
* @description
* # DatosDemandanteCtrl
* Controller of the bolsaEmpleo
*/
angular.module('integragenerico.seguridad')
.controller('CambiarClaveCtrl', function ($scope, $modalInstance, Usuario, Dialogos, objetoModificando) {
	var vm = $scope;

	vm.clave = {};

	vm.objetoModificando = objetoModificando;

	vm.camposCambiarClave =[
	{
		key:"anterior",
		type:"input",
		className:'col-md-6 col-md-offset-3',
		templateOptions:{
			type:"password",
			label:"Contraseña Anterior:",
			placeholder:"Ingrese su anterior contraseña",
			required:true,
			maxLength:20,
			minlength:8
		}
	},
	{
		key:"clave",
		type:"input",
		className:'col-md-6 col-md-offset-3',
		templateOptions:{
			type:"password",
			label:"Nueva Contraseña:",
			placeholder:"Ingrese su nueva contraseña",
			required:true,
			maxLength:20,
			minlength:8
		}
	},
	{
		key:'repetida',
		type:'input',
		className:'col-md-6 col-md-offset-3',
		validators:{
			passwordIguales:{
				expression:function($viewValue, $modelValue, scope){
					var valor=($viewValue || $modelValue);
					var valor2=scope.model.clave;
					if(valor){
						return compararPassword(valor,valor2);
					}else{
						return true;
					}
				},
				message: '"las contraseñas deben ser iguales"'
			},
		},

		extras:{
			validateOnModelChange:true
		},

		templateOptions:{
			type:"password",
			label:"Repita contraseña:",
			placeholder:"Ingrese nuevamente su contraseña",
			required:true,
			maxLength:20,
			minlength:8
		}
	}
	];

	vm.cambiarClaves=function(form){
		// vm.clave.id = vm.objetoModificando.id;

		Usuario.cambiarClave(vm.clave,function(){
			Dialogos.abrirDialogoInformacion({textoDinamico:'Su contraseña fue actualizada correctamente'});
			$modalInstance.close();
		},function(error){
			console.log(error);
			Dialogos.abrirDialogoInformacion({textoDinamico:error.data.message});
		});
	};

	var compararPassword=function(valor1,valor2){
		if(valor1 !== valor2){
			return false;
		}else{
			return true;
		}
	};


	vm.cancelar = function () {
		$modalInstance.dismiss('Cancelado');
	};

});
