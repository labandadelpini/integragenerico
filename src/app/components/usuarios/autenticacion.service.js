'use strict';

 angular.module('integragenerico.seguridad')
 .service('Autenticacion', function ($q, AlmacenUsuarios, Usuario) {

    var vm = this;

    vm.login = login;

    vm.tienePermiso = tienePermiso;

    vm.validarUsuario = validarUsuario;

    vm.getAutenticacionBasica = getAutenticacionBasica;

    var actualizacion;

    function login(nombreUsuario, password, recordar) {
        AlmacenUsuarios.guardarAutenticacion(nombreUsuario, password, recordar);
        return actualizarUsuario();
    }

    function actualizarUsuario() {
        if (!actualizacion || actualizacion.$$state.status) {
            actualizacion = Usuario.login().$promise.then(function (usuario) {
                AlmacenUsuarios.guardarUsuario(usuario);
                Usuario.getImagen({codigo: usuario.id}).$promise.then(function(imagen){
                  AlmacenUsuarios.getUsuario().avatar = imagen;
                });
                return usuario;
            }, function (error) {
                AlmacenUsuarios.limpiar();
                return $q.reject({mensaje:"El usuario no se pudo loguear",estado:'No logueado'});
            });
            //console.log(actualizacion);
        }
        return actualizacion;
    }

    function tienePermiso(permiso) {
        return AlmacenUsuarios.tienePermiso(permiso);
    }

    function validarUsuario() {
        return actualizarUsuario();
    }

    function getAutenticacionBasica() {
        return AlmacenUsuarios.getAutenticacionBasica();
    }
});
