(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
 	 angular.module('integragenerico')
 	 .directive('tablaElementos', tablaElementos);

 	 function tablaElementos(ListaElementosTemplate) {
 	 	return {
 	 		restrict: 'E',
 	 		replace: true,
 	 		templateUrl: ListaElementosTemplate.getTemplate(),
 	 		scope: {
 	 			tabla: '='
 	 		},
 	 		controllerAs : 'vm',
 	 		controller : function ($scope,ConfigurarListados) {
 	 			var vm = this;
 	 			vm.configurarListados = ConfigurarListados;

 	 			vm.ejecutarAccionGeneral = ejecutarAccionGeneral;

 	 			function ejecutarAccionGeneral(accion) {
 	 				accion.ejecutarAccion($scope);
 	 			}
 	 		}
 	 	};
 	 }

 	})();
