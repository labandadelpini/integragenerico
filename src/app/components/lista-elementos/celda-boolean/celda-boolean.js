'use strict';

 angular.module('integragenerico')
 .directive('celdaBoolean', function($compile) {
   return {
     restrict: 'E',
     replace: true,
     scope: true,
     templateUrl: 'integragenerico/components/lista-elementos/celda-boolean/celda-boolean.html',
     cotrollerAs: 'vm',
     controller: function($scope, $parse) {
       var vm = this;
       $scope.valor = $parse($scope.col.field)($scope.row);
     }
   };
 });
