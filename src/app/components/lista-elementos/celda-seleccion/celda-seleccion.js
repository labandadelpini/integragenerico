'use strict';

angular.module('integragenerico')
.directive('celdaSeleccion', function($compile) {
 return {
   restrict: 'E',
   replace: true,
   scope: true,
   templateUrl: 'integragenerico/components/lista-elementos/celda-seleccion/celda-seleccion.html',
   cotrollerAs: 'vm',
   controller: function($scope, $parse) {
     var vm = this;
     $scope.valor = $parse($scope.col.field)($scope.row);
     $scope.mostrar = mostrar;

     function obtenerValor() {
      return $parse($scope.col.field)($scope.row);
    }

    var parseado = $parse($scope.col.mostrar ? $scope.col.mostrar : 'true');

    function mostrar() {
      return parseado($scope);
    }

    $scope.$watch(function() {
      return obtenerValor();
    }, function(newValue, oldValue) {
      $scope.valor = newValue;
    });

    $scope.$watch(function(){
      return $scope.valor;
    }, function(newValue, oldValue) {
      $scope.row[$scope.col.field] = newValue;
    });
  }
};
});
