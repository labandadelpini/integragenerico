(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name bolsaEmpleo.controller:AboutCtrl
     * @description
     * # AboutCtrl
     * Controller of the bolsaEmpleo
     */
     angular.module('integragenerico')
     .directive('celdaImagen', celdaImagen);

     function celdaImagen() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'integragenerico/components/lista-elementos/celda-imagen.html',
            scope: true,
            controllerAs: 'vm',
            controller: function ($scope, Imagen) {
                var vm = this;
                //console.log($scope.row);
                var pedirImagen = function() {
                    Imagen.get({codigo:$scope.row.id, clase: $scope.col.field}).$promise.then(function(data){
                        vm.imagen=  'data:' + data.tipoImagen + ';base64,' + data.imagen;
                    });
                };
                pedirImagen();
            }
        };
    }
})();