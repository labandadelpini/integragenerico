'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
angular.module('integragenerico')
  .directive('celdaIcono', function ($parse) {
    return {
      restrict: 'E',
      replace: true,
      scope: true,
      templateUrl: 'integragenerico/components/lista-elementos/celda-icono.html',
      cotrollerAs: 'vm',
      controller: function ($scope, $parse) {
        // console.log($scope.col);
        // console.log($scope.row);
        var vm = this;
        var parseado = $parse($scope.col.field);
        var parseadoIcono = $parse($scope.col.campoIcono);

        $scope.$watch('row', function (newValue) {
          $scope.color = parseado(newValue);
          if ($scope.col.campoIcono) {
            $scope.icono = parseadoIcono(newValue);
          } else {
            $scope.icono = $scope.col.icono;
          }
        }, true);
      }
    };
  });
