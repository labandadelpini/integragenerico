'use strict';

 angular.module('integragenerico')
 .directive('celdaMultiplesFilas', function() {
   return {
     restrict: 'E',
     replace: true,
     scope: true,
     templateUrl: 'integragenerico/components/lista-elementos/celda-multiples-filas/celda-multiples-filas.html',
     cotrollerAs: 'vm',
     controller: function($scope, $parse) {
       var vm = this;
       $scope.valor = $parse($scope.col.field)($scope.row);
       $scope.propiedad = $scope.col.propiedad;
     }
   };
 });
