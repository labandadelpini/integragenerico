'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
 angular.module('integragenerico')
 .directive('celda', function($compile) {
 	return {
 		restrict: 'A',
 		scope: true,
 		link: function(scope, element) {
 			var campo = scope.$eval('col');
 			var tipo = campo.tipo || 'celda-estatica';
 			element.html($compile(angular.element('<'+tipo+'></'+tipo+'>'))(scope));
 		}
 	};
 });