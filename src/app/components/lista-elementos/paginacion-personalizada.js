(function() {
    'use strict';

    angular
    .module('integragenerico').directive('paginacionPersonalizada', paginacionPersonalizada);

    function paginacionPersonalizada() {
    	return {
    		templateUrl: 'integragenerico/components/lista-elementos/paginacion-personalizada.html',
    		scope: {
    			paginacion: '=paginacionPersonalizada'
    		},
    		controller: function($scope) {

    			var self = this;

    			this.paginacion = $scope.paginacion;

    			this.calcularPrimerElemento = calcularPrimerElemento;

    			this.calcularUltimoElemento = calcularUltimoElemento;

    			this.calcularTamanioPagina = calcularTamanioPagina;

    			this.calcularTotalElementos = calcularTotalElementos;

    			function calcularPrimerElemento() {
    				return (self.paginacion.page() - 1) * self.paginacion.count() + 1;
    			}

    			function calcularUltimoElemento() {
    				var minimoTeorico = self.paginacion.page() * self.paginacion.count();
    				return Math.min(minimoTeorico,calcularTotalElementos());
    			}

    			function calcularTamanioPagina() {
    				return self.paginacion.count();
    			}

    			function calcularTotalElementos() {
    				return self.paginacion.total();
    			}

    			$scope.$watch('paginacion.count()', function(newValue) {
//    				console.log('Modelo modificado:' + self.paginacion.count());
    				self.tamanio = newValue;
    			});

    			$scope.$watch('vm.tamanio', function(newValue) {
//    				console.log('Vista modificada:' + newValue);
    				self.paginacion.count(newValue);
    			});

    		},
    		controllerAs: 'vm'
    	};
    }

})();