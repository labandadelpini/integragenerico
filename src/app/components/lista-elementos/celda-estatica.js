'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
 angular.module('integragenerico')
 .directive('celdaEstatica', function($compile) {
 	return {
 		restrict: 'E',
 		replace: true,
 		scope: true,
 		link: function(scope, element) {
// 			console.log('Compilando');
 			var campo = scope.$eval('col');
 			var valor = 'row.' + campo.field;
 			if (campo.filtros && campo.filtros.length > 0) {
 				valor += '|' + campo.filtros.join('|');
 			}
 			element.html($compile(angular.element('<span>{{' + valor + '}}</span>'))(scope));
 		}
 	};
 });