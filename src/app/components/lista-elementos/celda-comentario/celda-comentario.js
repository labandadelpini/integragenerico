'use strict';

 angular.module('integragenerico')
 .directive('celdaComentario', function($compile) {
   return {
     restrict: 'E',
     replace: true,
     scope: true,
     templateUrl: 'integragenerico/components/lista-elementos/celda-comentario/celda-comentario.html',
     cotrollerAs: 'vm',
     controller: function($scope, $parse) {
       var vm = this;
       $scope.valor = $parse($scope.col.field)($scope.row);
     }
   };
 });
