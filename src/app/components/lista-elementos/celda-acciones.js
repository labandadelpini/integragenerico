'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
angular.module('integragenerico')
  .directive('celdaAcciones', function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'integragenerico/components/lista-elementos/celda-acciones.html',
      scope: true,
      controllerAs: 'vm',
      controller: function($scope, $parse, Autenticacion1) {
        var vm = this;
        vm.ejecutarAccion = ejecutarAccion;
        vm.mostrarImp = mostrarImp;
        vm.tienePermisosImp = tienePermisosImp;

        // 			console.log($scope.tabla);
        function compilarAcciones() {
          $scope.col.acciones.forEach(function(accion) {

            accion.ejecutarAccion = $parse(accion.accion);

            var parseado = $parse(accion.mostrar);

            accion.mostrarImp = function($scope) {
              return accion.mostrar ? parseado($scope) : true;
            };
          });
        }
        compilarAcciones();

        function ejecutarAccion(accion) {
          accion.ejecutarAccion($scope);
        }

        function mostrarImp(accion) {
          return accion.mostrarImp($scope);
        }

        function tienePermisosImp(accion) {
          return accion.permisos ? Autenticacion1.tienePermisos(accion.permisos) : true;
        }

      }
    };
  });
