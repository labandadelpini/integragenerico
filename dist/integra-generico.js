(function() {
	'use strict';

	angular
	.module('integragenerico', [
    'ngAnimate', //Animaciones angular
    'ngCookies', //guardar datos en cookies con angular
    'ngTouch', //eventos tactiles con angular
    'ngSanitize', //limpiar los datos ingresados por el usuario con angular (xss)
    'ngMessages', //mostrar mensajes basados en las propiedades de los objetos con angular (se usa para mensajes de error de los form)
    'ngAria', //mostrar los atributos de los objetos en propiedades visibles x herramientas visibles de propiedades de accesibilidad
    'ngResource', //Accede recursos REST
    'ui.router', //maneja estados de la app
    'ui.bootstrap', //framework de componente de UI
    'toastr', //notificaciones toast con js
    'formly', //formularios a partir de arrays de campos
    'formlyBootstrap', //plantillas de bootstrap para formly
    'pascalprecht.translate', // permite la internacionalizacion de textos en la UI
    'ui.bootstrap.showErrors', //permite mostrar errores en formularios
    'ngTagsInput', //componente de etiquetas en formularios
//  'aj.crop', //componente para el tratamiento de imagenes (recortar)
    'ui.calendar', //componente para manejo de calendarios en UI
//    'highcharts-ng', //componente de graficos UI
    'ngStorage', //soporte para guardar datos en el local o sesion storage
    'base64', //soporte para codificar en base64
    'ngTable', //soporte para tablas dinámicas con angular.
    'xeditable', //soporte para editar en el lugar en angular.
    'angular-momentjs', // Módulo de momentjs
    'color.picker' //soporte para seleccionar un color
    ]);

		angular
		.module('devoops', []);

		angular
		.module('integragenerico.seguridad', [
			'ngCookies', //guardar datos en cookies con angular
			'ngResource', //Accede recursos REST
			'ngStorage' //soporte para guardar datos en el local o sesion storage
		]);

})();

(function () {
  'use strict';

  angular
    .module('integragenerico')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, $localStorageProvider, $sessionStorageProvider, $resourceProvider, $translateProvider, formlyConfigProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    function _defineProperty(obj, key, value) {
      if (key in obj) {
        Object.defineProperty(obj, key, {
          value: value,
          enumerable: true,
          configurable: true,
          writable: true
        });
      } else {
        obj[key] = value;
      }
      return obj;
    }

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;

    $localStorageProvider.setKeyPrefix('integra.');
    $sessionStorageProvider.setKeyPrefix('integra.');
    angular.extend($resourceProvider.defaults.actions, {
      update: {
        method: 'PUT'
      }
    });
    $translateProvider.useStaticFilesLoader({
      prefix: 'assets/translate/locale-',
      suffix: '.json'
    });
    $translateProvider.preferredLanguage('es');

    formlyConfigProvider.setType({
      name: 'typeahead',
      templateUrl: 'integragenerico/components/formly/typeahead.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'static',
      templateUrl: 'integragenerico/components/formly/texto-estatico/texto-estatico.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'tags',
      templateUrl: 'integragenerico/components/formly/etiquetas.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'tagsTexto',
      templateUrl: 'integragenerico/components/formly/etiquetas-texto.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'colorpicker',
      templateUrl: 'integragenerico/components/formly/color-picker.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'addon',
      templateUrl: 'integragenerico/components/formly/addon/addon.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
    });

    var controladorDatePicker = function ($scope, $parse) {
      $scope.crearMomento = function () {
        return new moment;
      };

      $scope.minimo = $parse($scope.to.minimo)($scope);
      $scope.maximo = $parse($scope.to.maximo)($scope);

      $scope.datepicker = {};
      $scope.datepicker.opened = false;
      $scope.datepicker.open = function ($event) {
        $scope.datepicker.opened = !$scope.datepicker.opened;
      };
    };

    controladorDatePicker.$inject = ['$scope', '$parse'];

    formlyConfigProvider.setType({
      name: 'datepicker',
      templateUrl: 'integragenerico/components/formly/datepicker.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
      controller: controladorDatePicker
    });

    formlyConfigProvider.setType({
      name: 'horizontalDatepicker',
      extends: 'datepicker',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalStatic',
      extends: 'static',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalAddon',
      extends: 'addon',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalColorPicker',
      extends: 'colorpicker',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setWrapper({
      name: 'horizontalBootstrapLabel',
      template: [
        '<label for="{{::id}}" class="{{to.anchoLabel ? to.anchoLabel : \'col-sm-2\'}} control-label">',
        '{{to.label}} {{to.required ? "*" : ""}}',
        '</label>',
        '<div class="{{to.ancho ? to.ancho : \'col-sm-10\'}}"> ',
        '<formly-transclude></formly-transclude>',
        '</div>'
      ].join(' ')
    });

    formlyConfigProvider.setType({
      name: 'file',
      templateUrl: 'integragenerico/components/formly/file-uploader.html',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
    });

    formlyConfigProvider.setType({
      name: 'horizontalInput',
      extends: 'input',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalRadio',
      extends: 'radio',
      templateUrl: 'integragenerico/components/formly/radio-button.html',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalTextarea',
      extends: 'textarea',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalSelect',
      extends: 'select',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError'],
      defaultOptions: function defaultOptions(options) {
        var ngOptions = options.templateOptions.ngOptions || 'option[to.valueProp || \'value\'] as option[to.labelProp || \'name\'] group by option[to.groupProp || \'group\'] for option in to.options';
        return {
          ngModelAttrs: _defineProperty({}, ngOptions, {
            value: options.templateOptions.optionsAttr || 'ng-options'
          })
        };
      }
    });

    formlyConfigProvider.setType({
      name: 'horizontalTypeahead',
      extends: 'typeahead',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalTags',
      extends: 'tags',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalTagsTexto',
      extends: 'tagsTexto',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'momentPicker',
      extends: 'input',
      defaultOptions: {
        templateOptions: {
          momentPicker: true
        },
        ngModelAttrs: {
          momentPicker: {
            bound: 'moment-picker',
            attribute: 'moment-picker'
          }
        }
      }
    });

    formlyConfigProvider.setType({
      name: 'horizontalMomentPicker',
      extends: 'momentPicker',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType([{
      name: 'telefono',
      template: '<input-telefono ng-model="model[options.key]" options="options" ng-class="{\'has-error\': showError}"></input-telefono>',
      wrapper: ['bootstrapLabel', 'bootstrapHasError']
    }, {
      name: 'horizontalTelefono',
      extends: 'telefono',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    }, {
      name: 'conjuntoTelefonos',
      template: '<conjunto-telefonos ng-model="model[options.key]" options="options"></conjunto-telefonos>',
      wrapper: ['bootstrapLabel', 'bootstrapHasError']
    }, {
      name: 'horizontalConjuntoTelefonos',
      extends: 'conjuntoTelefonos',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    }]);

    formlyConfigProvider.setType({
      name: 'timepicker',
      templateUrl: 'integragenerico/components/formly/timepicker/timepicker.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'horizontalTimepicker',
      extends: 'timepicker',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    formlyConfigProvider.setType({
      name: 'direccion',
      template: '<input-direccion ng-model="model[options.key]" options="options" ng-class="{\'has-error\': showError}"></input-direccion>',
      wrapper: ['bootstrapHasError']
    });
    formlyConfigProvider.setType({
      name: 'direccionNueva',
      template: '<input-direccion-nueva ng-model="model[options.key]" options="options" ng-class="{\'has-error\': showError}"></input-direccion-nueva>',
      wrapper: ['bootstrapHasError']
    });
    formlyConfigProvider.setType({
      name: 'lugarGeografico',
      template: '<input-lugar-geografico ng-model="model[options.key]" options="options" ng-class="{\'has-error\': showError}"></input-lugar-geografico>',
      wrapper: ['bootstrapHasError']
    });

    formlyConfigProvider.setWrapper([{
      templateUrl: 'integragenerico/components/formly/mensajes-error.html'
    }]);

    var controladorObjetoSeleccion = function ($scope, $parse, ServicioTablaConsulta) {
      $scope.abrirDialogoSeleccion = function () {
        ServicioTablaConsulta.abrirDialogoSeleccionar($scope.to.claseObjeto, function (objeto) {
          $parse($scope.options.key).assign($scope.model, objeto);
        }, function (cancelado) {

        }, $scope.to.funcionFiltro);
      };

      $scope.eliminarSeleccion = function () {
        $parse($scope.options.key).assign($scope.model, undefined);
      }

      $scope.getValor = function () {
        return $parse($scope.to.propiedadObjeto)($parse($scope.options.key)($scope.model));
      };
    };

    controladorObjetoSeleccion.$inject = ['$scope', '$parse', 'ServicioTablaConsulta'];

    formlyConfigProvider.setType({
      name: 'objetoSeleccion',
      templateUrl: 'integragenerico/components/formly/objeto-seleccion/objeto-seleccion.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
      controller: controladorObjetoSeleccion
    });

    formlyConfigProvider.setType({
      name: 'horizontalObjetoSeleccion',
      extends: 'objetoSeleccion',
      wrapper: ['horizontalBootstrapLabel', 'bootstrapHasError']
    });

    var controladorShowPassword = function ($scope, $parse) {
      $scope.tipoInput = 'password';
      $scope.cambiarTipo = function() {
        $scope.muestro = !$scope.muestro;
      };
      $scope.muestro = false;
    };

    controladorShowPassword.$inject = ['$scope', '$parse'];

    formlyConfigProvider.setType({
      name: 'showPasswordInput',
      templateUrl: 'integragenerico/components/formly/show-password/show-password.html',
      wrapper: ['bootstrapLabel', 'bootstrapHasError'],
      controller: controladorShowPassword
    });

  }


})();

/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('integragenerico')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('$',$);
})();

angular.module("integragenerico").run(["$templateCache", function($templateCache) {$templateCache.put("integragenerico/components/dialogos/dialogo-cargando.html","<div class=\"modal-header\">\r\n    <h3 class=\"modal-title\" translate>{{vm.titulo}}</h3>\r\n</div>\r\n<div class=\"modal-body\">{{vm.contenido | translate}}<br/>\r\n    <uib-progressbar value=\"100\" type=\"danger\" class=\"progress-striped active\"/><br/>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/dialogos/dialogo-confirmacion.html","<div ng-if=\"vm.tieneTitulo\" class=\"modal-header\">\r\n    <h4 class=\"modal-title\" translate>{{vm.titulo}}</h4>\r\n</div>\r\n<div class=\"modal-body\">\r\n    <p class=\"text-center\" translate>{{vm.contenido}}</p>\r\n</div>\r\n<div class=\"modal-footer\">\r\n    <button class=\"btn btn-success\" ng-click=\"vm.si()\" translate>{{vm.textoSi}}</button>\r\n    <button ng-if=\"vm.cancelar\" class=\"btn btn-danger\" ng-click=\"vm.no()\" translate>{{vm.textoNo}}</button>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/dialogos/dialogo-informacion.html","<div ng-if=\"vm.tieneTitulo\" class=\"modal-header\">\r\n    <h4 class=\"modal-title\" translate>{{vm.titulo}}</h4>\r\n</div>\r\n<div class=\"modal-body\">\r\n            <td style=\"vertical-align: middle; color: blue;\"><i class=\"fa fa-info-circle fa-2x vcenter\"></i></td>\r\n            <td style=\"vertical-align: middle;padding-left: 10px;\"><span class=\"text-center\" translate> {{vm.contenido}}</span></td>\r\n</div>\r\n<div class=\"modal-footer\">\r\n    <button class=\"btn btn-success\" ng-click=\"vm.cerrar()\" translate>{{vm.textoCerrar}}</button>\r\n</div>");
$templateCache.put("integragenerico/components/dialogos/dialogo-mostrar-imagen.html","<div class=\"modal-header\">\r\n    <h5 class=\"modal-title\" translate>{{vm.titulo}}</h5>\r\n</div>\r\n<div class=\"modal-body\">\r\n    <img ng-src=\"{{vm.img}}\" alt=\"{{vm.info}}\" />\r\n</div>\r\n<div class=\"modal-footer\">\r\n    <button class=\"btn btn-success\" ng-click=\"vm.cerrar()\">Cerrar</button>\r\n</div>");
$templateCache.put("integragenerico/components/editar-elemento/formulario-editar-elemento.html","<div>\r\n	<div class=\"row\">\r\n		<div class=\"col-xs-12 titulo\">\r\n			<h4>{{formulario.titulo | translate}}</h4>\r\n		</div>\r\n	</div>\r\n	<section class=\"main\">\r\n		<div class=\"row\">\r\n			<div class=\"col-xs-12 col-sm-12\">\r\n				<div class=\"box\">\r\n					<div class=\"box-header\">\r\n						<div class=\"box-name\">\r\n							<i class=\"fa fa-table\"></i>\r\n							<span>{{formulario.titulo | translate}}</span>\r\n						</div>\r\n						<div class=\"box-icons\">\r\n							<a class=\"collapse-link\">\r\n								<i class=\"fa fa-chevron-up\"></i>\r\n							</a>\r\n							<a class=\"expand-link\">\r\n								<i class=\"fa fa-expand\"></i>\r\n							</a>\r\n						</div>\r\n						<div class=\"no-move\"></div>\r\n					</div>\r\n					<div class=\"box-content\">\r\n						<div class=\"panel-body\">\r\n							<form autocomplete=\"off\">\r\n								<formly-form model=\"formulario.elemento\" fields=\"formulario.campos\" form=\"formulario.form\" options=\"formulario.opciones\">\r\n									<div class=\"row col-xs-12 pull-right\">\r\n										<div class=\"pull-right\">\r\n											<button type=\"submit\" class=\"btn btn-success\" ng-click=\"formulario.guardar(formulario)\">Guardar</button>\r\n											<button type=\"button\" class=\"btn btn-danger\" ng-click=\"formulario.cancelar(formulario)\">Cancelar</button>\r\n										</div>\r\n									</div>\r\n								</formly-form>\r\n							</form>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</section>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/editar-elemento/vista-editar-elemento-modal.html","<div class=\"modal-body\">\r\n    <formulario-editar-elemento formulario=\"vm.formulario\"></formulario-editar-elemento>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/editar-elemento/vista-editar-elemento.html","<formulario-editar-elemento formulario=\"vm.formulario\"></formulario-editar-elemento>");
$templateCache.put("integragenerico/components/lista-elementos/celda-acciones.html","<div class=\"btn-group text-center\">\r\n	<button\r\n	ng-repeat=\"accion in col.acciones\"\r\n	class=\"btn\"\r\n	ng-class=\"accion.claseBoton\"\r\n	ng-click=\"vm.ejecutarAccion(accion)\"\r\n	ng-if=\"vm.mostrarImp(accion) && vm.tienePermisosImp(accion) && accion.popover\"\r\n	uib-popover=\"{{accion.popover}}\" popover-trigger=\"\'mouseenter\'\"\r\n	popover-append-to-body=\"true\">\r\n	<span ng-class=\"accion.icono\"></span>\r\n	</button>\r\n\r\n	<button\r\n	ng-repeat=\"accion in col.acciones\"\r\n	class=\"btn\"\r\n	ng-class=\"accion.claseBoton\"\r\n	ng-click=\"vm.ejecutarAccion(accion)\"\r\n	ng-if=\"vm.mostrarImp(accion) && vm.tienePermisosImp(accion) && !accion.popover\">\r\n	<span ng-class=\"accion.icono\"></span>\r\n	</button>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/lista-elementos/celda-icono.html","<div>\r\n	<span ng-class=\"icono\" ng-style=\"{color: color}\"></span>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/lista-elementos/celda-imagen.html","<span>\r\n    <img ng-if=\"vm.imagen\" ng-src=\"{{vm.imagen}}\" class=\"img-rounded\" style=\"width: 50px;\">\r\n    <span ng-if=\"!vm.imagen\">\r\n    	<span  class=\"fa fa-spinner fa-pulse\"></span>\r\n    </span>\r\n</span>\r\n\r\n");
$templateCache.put("integragenerico/components/lista-elementos/encabezado-seleccion.html","<div >\r\n	<input type=\"checkbox\" ng-model=\"tabla.checkboxes.checked\" class=\"select-all\" value=\"\" />\r\n</div>");
$templateCache.put("integragenerico/components/lista-elementos/lista-elementos.html","<div class=\"row\">\r\n    <div class=\"col-xs-12 titulo\">\r\n        <h4>{{titulo}}</h4>\r\n    </div>\r\n</div>\r\n\r\n<section class=\"main\">\r\n    <div class=\"row\">\r\n        <div class=\"col-xs-12 col-sm-12\">\r\n            <div class=\"box\">\r\n                <div class=\"box-header\">\r\n                    <div class=\"box-name\">\r\n                        <i class=\"fa fa-table\"></i>\r\n                        <span>Listado {{titulo}}</span>\r\n                    </div>\r\n                    <div class=\"box-icons\">\r\n                        <a class=\"collapse-link\">\r\n                            <i class=\"fa fa-chevron-up\"></i>\r\n                        </a>\r\n                        <a class=\"expand-link\">\r\n                            <i class=\"fa fa-expand\"></i>\r\n                        </a>\r\n                    </div>\r\n                    <div class=\"no-move\"></div>\r\n                </div>\r\n                <div class=\"box-content\">\r\n                    <div class=\"form-horizontal\">\r\n                        <p filtro-tabla ng-if=\"filtro\" filtro=\"filtro\"/>\r\n                        <p filtro-seleccion ng-if=\"filtroSeleccion\" filtro=\"filtroSeleccion\"/>\r\n                    </div>                    \r\n                  <table class=\"table\" tabla-ordenable columnas=\"columnas\" elementos=\"elementos\" acciones=\"acciones\" footer=\"footer\" mostrar-dialogo-orden=\"mostrarDialogoOrden\"></table>\r\n                    Separacion\r\n                    <paginacion-con-cantidad-items ng-if=\"paginacion\" paginacion=\"paginacion\"/>\r\n                    <div class=\"btn-group btn-group-justified\" ng-if=\"accionesGenerales\">\r\n                        <a ng-repeat=\"accion in accionesGenerales\" ng-class=\"accion.claseBoton\" ng-click=\"accion.accion()\">{{accion.nombre}}</a>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>       \r\n\r\n");
$templateCache.put("integragenerico/components/lista-elementos/paginacion-personalizada.html","<span ng-if=\"vm.calcularTotalElementos()\"> Mostrando {{vm.calcularPrimerElemento()}} - {{vm.calcularUltimoElemento()}} de {{vm.calcularTotalElementos()}}. </span>\r\n<span ng-if=\"!vm.calcularTotalElementos()\"> No hay elementos para mostrar. </span>\r\n<span> Mostrando <a editable-number=\"vm.tamanio\" e-min=\"1\" href=\"\"->{{vm.calcularTamanioPagina()}}</a> elementos por página. </span>\r\n");
$templateCache.put("integragenerico/components/lista-elementos/tabla-elementos.html","<div>\r\n	<div class=\"row\">\r\n		<div class=\"col-xs-12 titulo\">\r\n			<h4>{{tabla.titulo}}</h4>\r\n		</div>\r\n	</div>\r\n	<section class=\"main\">\r\n		<div class=\"row\">\r\n			<div class=\"col-xs-12 col-sm-12\">\r\n				<div class=\"box\">\r\n					<div class=\"box-header\">\r\n						<div class=\"box-name\">\r\n							<i class=\"fa fa-table\"></i>\r\n							<span>{{tabla.titulo}}</span>\r\n						</div>\r\n						<div class=\"box-icons\">\r\n							<a class=\"collapse-link\">\r\n								<i class=\"fa fa-chevron-up\"></i>\r\n							</a>\r\n							<a class=\"expand-link\">\r\n								<i class=\"fa fa-expand\"></i>\r\n							</a>\r\n						</div>\r\n						<div class=\"no-move\"></div>\r\n					</div>\r\n					<div class=\"box-content\">\r\n						<div class=\"panel-body\">\r\n							<div ng-if=\"vm.configurarListados.filtra\" class=\"col-xs-10\">\r\n								<formly-form model=\"tabla.filtro\" fields=\"tabla.filtro.campos\" form=\"form\"></formly-form>\r\n							</div>\r\n							<div ng-if=\"vm.configurarListados.filtra\" class=\"col-xs-2\">\r\n								<button class=\"btn btn-default pull-right\" ng-if=\"tabla.cargando\">\r\n									<span class=\"pull-right\">\r\n										<span class=\"fa fa-refresh fa-spin\"></span>\r\n									</span>\r\n								</button>\r\n								<button class=\"btn btn-default pull-right\" ng-if=\"!tabla.cargando\" ng-click=\"tabla.cambiarOrden()\"><span class=\"fa fa-gear\"></span></button>\r\n								<button class=\"btn btn-default pull-right\" ng-click=\"tabla.mostrarFiltros()\"><span class=\"fa fa-filter\"></span></button>\r\n							</div>\r\n							<table ng-table-dynamic=\"tabla.tableParams with tabla.cols\" show-filter=\"tabla.filtrosActivos\" class=\"table table-striped\">\r\n								<tr ng-if=\"!tabla.cargando\" ng-repeat=\"row in $data track by row.id\">\r\n									<td ng-repeat=\"col in $columns\" ng-class=\"col.className\" celda></td>\r\n								</tr>\r\n								<tr ng-if=\"tabla.cargando\">\r\n									<td colspan=\"{{$columns.length}}\" class=\"text-center\">\r\n										<span class=\"fa fa-spinner fa-pulse icono-cargando\"></span>\r\n									</td>\r\n								</tr>\r\n							</table>\r\n\r\n							<div ng-if=\"vm.configurarListados.pagina\" paginacion-personalizada=\"tabla.tableParams\" class=\"pull-right\"></div>\r\n							<br/>\r\n							<br/>\r\n							<div class=\"btn-group btn-group-justified\" ng-if=\"tabla.accionesGenerales.length > 0\">\r\n								<a class=\"btn\" ng-class=\"accion.claseBoton\" ng-repeat=\"accion in tabla.accionesGenerales\" ng-click=\"vm.ejecutarAccionGeneral(accion)\">\r\n									{{accion.texto}}\r\n								</a>\r\n							</div>\r\n\r\n						</div>\r\n\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</section>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/lista-elementos/vista-tabla-elementos-modal.html","<div class=\"modal-body\">\r\n  <tabla-elementos tabla=\"vm.tabla\"></tabla-elementos>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/lista-elementos/vista-tabla-elementos.html","<tabla-elementos tabla=\"vm.tabla\"></tabla-elementos>");
$templateCache.put("integragenerico/components/formly/color-picker.html","<div>\r\n	<color-picker \r\n	ng-model=\"model[options.key]\"\r\n	color-picker-format=\"to.formatocolor\"\r\n	color-picker-alpha=\"to.alpha\"\r\n	></color-picker>\r\n</div>");
$templateCache.put("integragenerico/components/formly/datepicker.html","<p class=\"input-group\">\r\n    <input  type=\"text\"\r\n    ng-model=\"model[options.key]\"\r\n    class=\"form-control\"\r\n    ng-click=\"datepicker.open($event)\"\r\n    is-open=\"datepicker.opened\"\r\n    min-date=\"{{minimo}}\"\r\n    max-date=\"{{maximo}}\"\r\n    clear-text=\"{{to.clearText}}\"\r\n    close-text=\"{{to.closeText}}\"\r\n    current-text=\"{{to.currentText}}\"\r\n    uib-datepicker-popup=\"{{to.formatoFecha}}\"\r\n    />\r\n    <span class=\"input-group-btn\">\r\n        <button type=\"button\" class=\"btn btn-default\" ng-click=\"datepicker.open($event)\" ng-disabled=\"to.disabled\"><i class=\"glyphicon glyphicon-calendar\"></i></button>\r\n    </span>\r\n</p>\r\n");
$templateCache.put("integragenerico/components/formly/etiquetas-texto.html","<div>\r\n    <tags-input\r\n    ng-model=\"model[options.key]\"\r\n    id=\"{{options.key}}\"\r\n    name=\"{{options.key}}\"\r\n    placeholder=\"{{to.placeholder}}\"\r\n    min-tags=\"{{ to.cantidadMinima ? to.cantidadMinima : 0 }}\"\r\n    min-length=\"1\"\r\n    >\r\n    </tags-input>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/formly/etiquetas.html","<div>\r\n    <tags-input\r\n\r\n    ng-model=\"model[options.key]\"\r\n    id=\"{{options.key}}\"\r\n    name=\"{{options.key}}\"\r\n    placeholder=\"{{to.placeholder}}\"\r\n\r\n    min-tags=\"{{ to.cantidadMinima ? to.cantidadMinima : 0 }}\"\r\n    min-length=\"1\"\r\n    display-property=\"{{to.propiedadFiltro}}\"\r\n    add-from-autocomplete-only=\"true\"\r\n    replace-spaces-with-dashes=\"{{to.completaConEspacios ? to.completaConEspacios : false}}\"\r\n    >\r\n        <auto-complete\r\n        min-length=\"1\"\r\n        source=\"to.funcion($query)\"\r\n        load-on-down-arrow=\"true\"\r\n        ></auto-complete>\r\n    </tags-input>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/formly/file-uploader.html","<div class=\"input-group\">\r\n<input type=\"file\"  fileread=\"model[options.key]\"/>\r\n\r\n<div class=\"col-sm-12\">\r\n	<img ng-src=\"{{model[options.key]}}\" style=\"width:100%;\" />\r\n</div>\r\n </div>");
$templateCache.put("integragenerico/components/formly/mensajes-error-popover.html","<div class=\"alert-danger\" ng-messages=\"options.formControl.$error\">\r\n  <span class=\"glyphicon glyphicon-exclamation-sign\"></span>\r\n  <span ng-message=\"{{::name}}\" ng-repeat=\"(name, message) in ::options.validation.messages\">\r\n   {{message(options.formControl.$viewValue, options.formControl.$modelValue, this)}}\r\n  </span>\r\n </div>\r\n");
$templateCache.put("integragenerico/components/formly/mensajes-error.html"," <div popover-template=\"options.validation.errorExistsAndShouldBeVisible ? \'integragenerico/components/formly/mensajes-error-popover.html\':null\" popover-trigger=\"mouseenter\">\r\n 	<formly-transclude></formly-transclude>\r\n </div>");
$templateCache.put("integragenerico/components/formly/radio-button.html","<div class=\"radio-group\">\r\n  <label ng-repeat=\"(key, option) in to.options\" class=\"radio-inline\">\r\n    <input type=\"radio\" id=\"{{id + \'_\'+ $index}}\" tabindex=\"0\" ng-value=\"option[to.valueProp || \'value\']\" ng-model=\"model[options.key]\"> {{option[to.labelProp || \'name\']}}\r\n  </label>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/formly/typeahead.html","<div class=\"input-group\">\r\n<input type=\"text\" ng-model=\"model[options.key]\"\r\n uib-typeahead=\"item as item[to.propiedad] for item in to.funcion($viewValue) | limitTo:to.limite ? to.limite : 8\"\r\n typeahead-editable=\"to.permitirNuevo\"\r\n typeahead-wait-ms=\"to.espera\"\r\n typeahead-loading=\"to.cargando\"\r\n typeahead-min-length=\"to.cantidadMinima ? to.cantidadMinima : 3\"\r\n class=\"form-control\">\r\n <span class=\"input-group-addon\">\r\n 	<span ng-if=\"to.cargando\"><span class=\"fa fa-refresh fa-spin\"></span></span>\r\n 	<span ng-if=\"!to.cargando\" class=\"fa fa-search fa-fw\"></span>\r\n </span>\r\n </div>\r\n");
$templateCache.put("integragenerico/components/usuarios/info-usuario.html","<li class=\"dropdown\" ng-controller=\"InfoUsuarioCtrl as vm\">\r\n    <a class=\"dropdown-toggle account\" data-toggle=\"dropdown\">\r\n        <div class=\"avatar\">\r\n            <img ng-if=\"vm.getImagen()\" ng-src=\"{{vm.getImagen()}}\" class=\"img-rounded\" alt=\"avatar\" />\r\n        </div>\r\n        <i class=\"fa fa-angle-down pull-right\" ng-click=\"vm.mostrarMenu()\"></i>\r\n        <div class=\"user-mini pull-right\">\r\n            <span class=\"welcome\">Bienvenido,</span>\r\n            <span>{{vm.getUsuario().vendedor.nombre !== undefined ? vm.getUsuario().vendedor.nombre: vm.getUsuario().nombre }}</span>\r\n        </div>\r\n    </a>\r\n    <ul ng-if=\"vm.menuVisible\" class=\"dropdown-menu\" style=\"display: block;\">\r\n        <li>\r\n            <a href=\"#\"> <i class=\"fa fa-user\"></i>\r\n                <span>Perfil</span>\r\n            </a>\r\n        </li>\r\n        <li>\r\n            <a href=\"\" class=\"ajax-link\" ng-click=\"vm.cambiarPassword()\">\r\n                <i class=\"fa fa-envelope\"></i>\r\n                <span>Cambio contrase&ntilde;a</span>\r\n            </a>\r\n        </li>\r\n        <li>\r\n            <a href=\"\" ng-click=\"vm.cerrarSesion()\">\r\n                <i class=\"fa fa-power-off\"></i>\r\n                <span>Cerrar Sesión</span>\r\n            </a>\r\n        </li>\r\n    </ul>\r\n</li>\r\n");
$templateCache.put("integragenerico/components/util/elemento-seleccionable.html","<div class=\"checkbox\">\r\n    <label><input type=\"checkbox\" ng-model=\"valor\"\r\n                  ng-click=\"columna.funcionValidarSeleccionarTodos()\"/><i class=\"fa fa-square-o small\"/></label>\r\n</div>");
$templateCache.put("integragenerico/components/util/encabezado_ordenable.html","<div ng-if=\"campo.ordenable === undefined\" ng-click=\"ejecutarAccion(campo)\" class=\"encabezado-ordenable\">\r\n    {{campo.ordenable}}\r\n    <div ng-if=\"campo.tipo === \'Seleccionable\'\">\r\n        <div encabezado-seleccionable campo=\"campo\"/>\r\n    </div>\r\n    <div ng-if=\"campo.tipo !== \'Seleccionable\'\">\r\n        {{campo.textoEncabezado}}\r\n        <span class=\"glyphicon\" \r\n              ng-class=\"{\'glyphicon-none\':!esOrdenImp(campo.nombreCampo),\'glyphicon-chevron-down\': esOrdenImp(campo.nombreCampo,false),\'glyphicon-chevron-up\': esOrdenImp(campo.nombreCampo,true)}\">\r\n        </span>\r\n    </div>\r\n\r\n</div>\r\n<div ng-if=\"(campo.ordenable !== undefined && campo.ordenable === false)\">\r\n    {{campo.textoEncabezado}}\r\n</div>");
$templateCache.put("integragenerico/components/util/encabezado_seleccionable.html","<div class=\"checkbox\">\r\n    <label>\r\n        <input type=\"checkbox\" \r\n               ng-model=\"campo.funcionSeleccionarTodos\" \r\n               ng-model-options=\"{getterSetter: true}\"/>\r\n        <i class=\"fa fa-square-o small\"/></label>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/util/fila_encabezado_ordenable.html","<tr>\r\n    <th ng-repeat=\"columna in columnas\" style=\"vertical-align: middle;\" encabezado-ordenable campo=\"columna\" es-orden=\"esOrdenImp\" cambiar-orden=\"cambiarOrdenImp\"></th>\r\n    <th ng-if=\"acciones\" style=\"vertical-align: middle;\" class=\"text-center\">Acciones\r\n<div class=\"btn-group\">\r\n    <button ng-click=\"mostrarDialogoOrden(columnas)\" \r\n        \r\n        class=\"btn btn-primary\">\r\n        <span class=\"fa fa-sort\"></span>\r\n    </button>\r\n</div>\r\n</th>\r\n</tr>");
$templateCache.put("integragenerico/components/util/filtro-seleccion.html","<div class=\"form-group col-sm-6 has-error has-feedback\">\r\n    <div class=\"selectContainer\" ng-if=\"filtro.elementos !== undefined\">\r\n        <div class=\"col-sm-3\">\r\n            <label class=\"control-label\">{{filtro.label}}</label>\r\n        </div>\r\n        <div class=\"col-sm-9\">\r\n            <select \r\n                class=\"form-control\" \r\n                ng-options=\"elemento as elemento.toString() for elemento in filtro.elementos\" \r\n                ng-model=\"filtro.elementoSeleccionado\"\r\n                ng-change=\"filtro.actualizarSeleccion()\"></select>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("integragenerico/components/util/filtro_tabla.html","<div class=\" form-group has-error has-feedback col-sm-6\">\r\n    <div class=\"col-sm-2\">\r\n        <label class=\"control-label\">Filtro:</label>\r\n    </div>\r\n\r\n    <div class=\"col-sm-6\">\r\n        <div class=\"input-group\">\r\n            <input type=\"text\" class=\"form-control\" placeholder=\"Buscar\" ng-model=\"filtro.filtroTextoClientes\" on-enter=\"filtro.actualizarFiltro()\">\r\n            <span class=\"input-group-addon\">\r\n                <span class=\"fa fa-times\" style=\"cursor: pointer;\" ng-click=\"filtro.limpiarFiltro()\"></span> \r\n            </span>\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <div class=\"col-sm-4\">\r\n        <button ng-click=\"filtro.actualizarFiltro()\" class=\"btn btn-danger\">Buscar</button>\r\n    </div>\r\n</div>");
$templateCache.put("integragenerico/components/util/paginacion_con_cantidad_items.html","\r\n\r\n<table width=\"99%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n  <tr>\r\n    <th width=\"49%\"scope=\"col\">\r\n        <pagination \r\n            total-items=\"paginacion.totalItems\" \r\n            items-per-page=\"paginacion.itemsPorPagina\" \r\n            ng-model=\"paginacion.paginaActual\" \r\n            max-size=\"paginacion.tamanioMaximo\" \r\n            class=\"pagination-sm\" \r\n            boundary-links=\"true\" \r\n            num-pages=\"paginacion.numeroPaginas\"\r\n            previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"\r\n            rotate=\"false\">\r\n        </pagination>\r\n    </th>\r\n    <th width=\"51%\" scope=\"col\"><table width=\"60%\" border=\"0\" align=\"right\" cellpadding=\"0\" cellspacing=\"0\">\r\n      <tr>\r\n        <th width=\"20%\" scope=\"col\">Mostrando</th>\r\n        <th width=\"15%\" scope=\"col\"><input class=\"form-control\" ng-model=\"paginacion.itemsPorPaginaTransitorio\" on-enter=\"paginacion.actualizarItemPorPagina()\"/> \r\n</th>\r\n        <th width=\"65%\" scope=\"col\"> elementos de {{paginacion.totalItems}}</th>\r\n      </tr>\r\n    </table></th>\r\n  </tr>\r\n</table>");
$templateCache.put("integragenerico/components/util/tabla_ordenable.html","<table class=\"table table-striped\" id=\"tablaOrdenable\">\r\n    <tr fila-encabezado-ordenable  columnas=\"columnas\" es-orden=\"esOrdenImp\" cambiar-orden=\"cambiarOrdenImp\" acciones=\"acciones\" id-tabla=\"\'tablaOrdenable\'\" mostrar-dialogo-orden=\"mostrarDialogoOrden\"></tr>    </thead>\r\n<tr ng-if=\"elementos.length === 0 && elementos.$resolved\"><td colspan=\"{{columnas.length + 1}}\" class=\"text-center\">No se han encontrado elementos para el filtro aplicado</td></tr>\r\n<tr ng-if=\"elementos.length === 0 && !elementos.$resolved\"><td colspan=\"{{columnas.length + 1}}\" class=\"text-center\"><span class=\"fa fa-spinner fa-pulse icono-cargando\"></span></td></tr>\r\n<tr ng-repeat=\"elemento in elementos\" ng-class=\"{info : elemento.estaSeleccionado}\">\r\n    <td ng-repeat=\"columna in columnas\" style=\"vertical-align: middle;\">\r\n        <div ng-if=\"!columna.tipo\">{{devolverValor(elemento, columna)}}</div>\r\n        <div ng-if=\"columna.tipo === \'Fecha\'\">{{devolverValor(elemento, columna) | date : \'dd/MM/yyyy\'}}</div>\r\n        <div ng-if=\"columna.tipo === \'FechaHora\'\">{{devolverValor(elemento, columna) | date : \'dd/MM/yyyy HH:mm\'}}</div>\r\n        <div ng-if=\"columna.tipo === \'Moneda\'\" class=\"text-right\">{{devolverValor(elemento, columna) | currency}}</div>\r\n        <div ng-if=\"columna.tipo === \'Numero\'\" class=\"text-right\">{{devolverValor(elemento, columna) | number:0}}</div>\r\n        <div ng-if=\"columna.tipo === \'NumeroCentrado\'\" class=\"text-center\">{{devolverValor(elemento, columna) | number:0}}</div>\r\n        <div ng-if=\"columna.tipo === \'MonedaSinIVA\'\" class=\"text-right\">{{devolverValor(elemento, columna) | currency}} + IVA</div>\r\n\r\n        <div ng-if=\"columna.tipo === \'Imagen\'\" class=\"text-center\" >\r\n            <imagen-en-fila elemento=\"elemento\" columna=\"columna\" valor=\"devolverValor\">\r\n        </div>    \r\n\r\n        <div ng-if=\"columna.tipo === \'Barra\'\" class=\"text-center\" >\r\n            <div barra-dinamica elemento=\"elemento\" columna=\"columna\" valor=\"devolverValor\">\r\n\r\n            </div>\r\n        </div>\r\n        <div ng-if=\"columna.tipo === \'Booleano\'\" class=\"text-center\">\r\n            <span class=\"fa\" ng-class=\"{\'fa-check columna-habilitado\': devolverValor(elemento, columna),\'fa-close columna-no-habilitado\': !devolverValor(elemento, columna)}\"></span>\r\n        </div>\r\n        <div ng-if=\"columna.tipo === \'MultipleFilas\'\">\r\n            <div ng-repeat=\"valor in devolverValor(elemento, columna)\">\r\n                {{valor}}\r\n            </div>\r\n        </div>\r\n        <div ng-if=\"columna.tipo === \'Seleccionable\'\">\r\n            <span elemento-seleccionable elemento=\"elemento\" columna=\"columna\"/>  \r\n        </div>\r\n        <div ng-if=\"columna.tipo === \'Comentario\' && devolverValor(elemento, columna)\">\r\n            <span class=\"fa fa-comment\"  popover-placement=\"top\" popover-trigger=\"mouseenter\" popover=\"{{devolverValor(elemento, columna)}}\"></span>\r\n        </div>\r\n        <div ng-if=\"columna.tipo === \'Icono\' && devolverValor(elemento, columna) != \'\'\" class=\"text-center\">\r\n            <span class=\"fa {{devolverValor(elemento, columna, \'icono\')}} text-center\" popover-placement=\"top\" popover-trigger=\"mouseenter\" popover=\"{{devolverValor(elemento, columna)}}\"></span>\r\n        </div>\r\n    </td>\r\n    <td ng-if=\"acciones\" style=\"vertical-align: middle; text-align: center\">\r\n        <div class=\"btn-group\">\r\n            <button \r\n                ng-repeat=\"accion in acciones\" \r\n                ng-click=\"accion.funcion(elemento)\" \r\n                popover=\"{{accion.nombre}}\" \r\n                popover-append-to-body=\"true\"\r\n                popover-trigger=\"mouseenter\" \r\n                ng-class=\"accion.claseBoton\">\r\n                <span ng-class=\"accion.clase\"></span>\r\n            </button>\r\n        </div>\r\n    </td>\r\n</tr>\r\n<tfoot ng-if=\"footer && !(elementos.length === 0 && elementos.$resolved)\" ng-include=\"footer.template\">  \r\n</tfoot>\r\n</table>");
$templateCache.put("integragenerico/components/lista-elementos/celda-boolean/celda-boolean.html","<div>\r\n	<span class=\"fa fa-lg\" ng-class=\"{true: \'fa-check text-success\', false: \'fa-close text-danger\'}[row[col.field]]\"></span>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/lista-elementos/celda-comentario/celda-comentario.html","<div>\r\n	<span ng-if=\"valor\" class=\"fa fa-lg fa-comment\" uib-popover=\"{{valor}}\" popover-trigger=\"\'mouseenter\'\"\r\n	popover-append-to-body=\"true\"></span>\r\n</div>");
$templateCache.put("integragenerico/components/lista-elementos/celda-multiples-filas/celda-multiples-filas.html","<div>\r\n	<div ng-repeat=\"val in valor\">\r\n	{{val[propiedad]}}\r\n	</div>\r\n</div>");
$templateCache.put("integragenerico/components/lista-elementos/celda-seleccion/celda-seleccion.html","<div ng-show=\"mostrar(row)\">\r\n	<input ng-disabled=\"row.seleccionable !== undefined && !row.seleccionable\" type=\"checkbox\" value=\"\" ng-model=\"valor\">\r\n</div>\r\n");
$templateCache.put("integragenerico/components/formly/addon/addon.html","<p class=\"input-group\">\r\n  <span ng-if=\"to.textoIzquierda\" class=\"input-group-addon\">\r\n    <span ng-if=\"!to.textoIzquierda.tipo || to.textoIzquierda.tipo === \'texto\'\">{{to.textoIzquierda.texto}}</span>\r\n    <span ng-if=\"to.textoIzquierda.tipo && to.textoIzquierda.tipo === \'icono\'\" class=\"fa fa-fw\" ng-class=\"to.textoIzquierda.icono\"></span>\r\n  </span>\r\n    <input  type=\"{{to.type}}\"\r\n    ng-model=\"model[options.key]\"\r\n    class=\"form-control\"\r\n    />\r\n     <span ng-if=\"to.textoDerecha\" class=\"input-group-addon\">\r\n       <span ng-if=\"!to.textoDerecha.tipo || to.textoDerecha.tipo === \'texto\'\">{{to.textoDerecha.texto}}</span>\r\n       <span ng-if=\"to.textoDerecha.tipo && to.textoDerecha.tipo === \'icono\'\" class=\"fa fa-fw\" ng-class=\"to.textoDerecha.icono\"></span>\r\n     </span>\r\n</p>\r\n");
$templateCache.put("integragenerico/components/formly/conjunto-telefonos/conjunto-telefonos.html","<div class=\"row\">\r\n	<div class=\"col-xs-12\" ng-repeat=\"telefono in vm.telefonos\">\r\n		<input-telefono ng-model=\"telefono\" options=\"vm.options\" class=\"col-xs-11\"></input-telefono>\r\n		<button ng-if=\"!vm.options.templateOptions.disabled\" ng-click=\"vm.quitarTelefono($index)\" class=\"col-xs-1 btn btn-danger\">\r\n			<i class=\"fa fa-trash\"></i>\r\n		</button>\r\n	</div>\r\n	<div class=\"col-xs-12\">\r\n		<button ng-if=\"!vm.options.templateOptions.disabled\" ng-click=\"vm.agregarTelefono()\" class=\"col-xs-12 btn btn-default\">Agregar...</button>\r\n	</div>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/formly/direccion/direccion.html","<div class=\"panel panel-default\">\r\n  <div class=\"panel-heading\">\r\n    {{vm.options.templateOptions.titulo}}\r\n  </div>\r\n  <div class=\"panel-body\">\r\n    <formly-form model=\"vm.model\" fields=\"vm.campos\" form=\"vm.form\"></formly-form>\r\n  </div>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/formly/direccion-nueva/direccion-nueva.html","<div class=\"panel panel-default\">\r\n    <div class=\"panel-heading\">\r\n      {{vm.options.templateOptions.titulo}}\r\n    </div>\r\n    <div class=\"panel-body\">\r\n      <formly-form model=\"vm.model\" fields=\"vm.campos\" form=\"vm.form\"></formly-form>\r\n    </div>\r\n  </div>\r\n  ");
$templateCache.put("integragenerico/components/formly/objeto-seleccion/objeto-seleccion.html","<p ng-class=\"{\'input-group\' : !to.disabled}\">\r\n    <input  type=\"text\"\r\n    ng-model=\"getValor\"\r\n    ng-model-options=\"{getterSetter : true}\"\r\n    class=\"form-control\"\r\n    ng-disabled=\"true\"\r\n    />\r\n    <span class=\"input-group-btn\" ng-if=\"!to.disabled\">\r\n        <button type=\"button\" class=\"btn btn-default\" ng-click=\"abrirDialogoSeleccion()\"><i class=\"fa fa-search\"></i></button>\r\n        <button type=\"button\" class=\"btn btn-danger\" ng-click=\"eliminarSeleccion()\"><i class=\"fa fa-trash\"></i></button>\r\n    </span>\r\n</p>\r\n");
$templateCache.put("integragenerico/components/formly/show-password/show-password.html","<p class=\"input-group\">\r\n    <input type=\"{{muestro ? \'text\' : \'password\'}}\"\r\n    ng-model=\"model[options.key]\"\r\n    class=\"form-control\"\r\n    />\r\n    <span class=\"input-group-btn\">\r\n        <span class=\"btn btn-default\" ng-click=\"cambiarTipo()\"><i class=\"fa fa-fw\" ng-class=\"{\'fa-eye\' : !muestro, \'fa-eye-slash\': muestro}\"></i></span>\r\n    </span>\r\n</p>\r\n");
$templateCache.put("integragenerico/components/formly/telefono/telefono-con-prefijo.html","	<div class=\"col-xs-12\"  style=\"padding-bottom: 5px;\">\r\n		<div class=\"col-xs-4 columna-pegada\">\r\n			<div class=\"input-group\" ng-class=\"{\'has-error\': vm.form.prefijo.$invalid}\">\r\n				<div class=\"input-group-btn\">\r\n					<button type=\"button\" ng-disabled=\"vm.options.templateOptions.disabled\" ng-click=\"vm.mostrarMenu()\" id=\"tipoTelefono\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">{{vm.telefono.tipo.nombre}} <span class=\"caret\"></span></button>\r\n					<ul ng-if=\"vm.menuVisible\" class=\"dropdown-menu\" style=\"display: block;\">\r\n						<li ng-repeat=\"tipo in vm.tipos\">\r\n							<a ng-click=\"vm.seleccionarTipo(tipo)\">{{tipo.nombre}}</a>\r\n						</li>\r\n					</ul>\r\n				</div>\r\n				<span ng-if=\"vm.telefono.tipo.prefijoHumano\" class=\"input-group-addon\">{{vm.telefono.tipo.prefijoHumano}}</span>\r\n				<input name=\"prefijo\" type=\"number\" ng-disabled=\"vm.options.templateOptions.disabled\" ng-minlength=\"vm.telefono.tipo.minimoPrefijo\" ng-maxlength=\"vm.telefono.tipo.maximoPrefijo\" ng-model=\"vm.telefono.prefijo\" placeholder=\"Prefijo\" class=\"form-control\">\r\n			</div>\r\n		</div>\r\n		<div class=\"col-xs-4 columna-pegada\">\r\n			<div class=\"input-group\" ng-class=\"{\'has-error\': vm.form.sufijo.$invalid}\">\r\n				<span class=\"input-group-addon\">{{vm.telefono.tipo.divisorHumano ? vm.telefono.tipo.divisorHumano : \'-\'}}</span>\r\n        <input name=\"sufijo\" type=\"number\" ng-disabled=\"vm.options.templateOptions.disabled\"  ng-minlength=\"vm.telefono.tipo.minimoSufijo\" ng-maxlength=\"vm.telefono.tipo.maximoSufijo\" ng-model=\"vm.telefono.sufijo\" placeholder=\"Sufijo\" class=\"form-control\">\r\n			</div>\r\n    </div>\r\n    <div class=\"col-xs-4 columna-pegada\">\r\n      <input name=\"comentario\" type=\"text\" ng-model=\"vm.telefono.comentario\" placeholder=\"Comentario\" class=\"form-control\">\r\n    </div>\r\n	</div>\r\n");
$templateCache.put("integragenerico/components/formly/telefono/telefono-sin-prefijo.html","<div class=\"col-xs-12\"  style=\"padding-bottom: 5px;\">\r\n  <div class=\"col-xs-8 columna-pegada\">\r\n    <div class=\"input-group\" ng-class=\"{\'has-error\': vm.form.sufijo.$invalid}\">\r\n      <div class=\"input-group-btn\">\r\n        <button type=\"button\" ng-disabled=\"vm.options.templateOptions.disabled\" ng-click=\"vm.mostrarMenu()\" id=\"tipoTelefono\" class=\"btn btn-default dropdown-toggle\"\r\n          data-toggle=\"dropdown\">{{vm.telefono.tipo.nombre}}\r\n          <span class=\"caret\"></span>\r\n        </button>\r\n        <ul ng-if=\"vm.menuVisible\" class=\"dropdown-menu\" style=\"display: block;\">\r\n          <li ng-repeat=\"tipo in vm.tipos\">\r\n            <a ng-click=\"vm.seleccionarTipo(tipo)\">{{tipo.nombre}}</a>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n      <span ng-if=\"vm.telefono.tipo.prefijoHumano\" class=\"input-group-addon\">{{vm.telefono.tipo.prefijoHumano}}</span>\r\n      <input name=\"sufijo\" type=\"number\" ng-disabled=\"vm.options.templateOptions.disabled\" ng-minlength=\"vm.telefono.tipo.minimoSufijo\"\r\n        ng-maxlength=\"vm.telefono.tipo.maximoSufijo\" ng-model=\"vm.telefono.sufijo\" placeholder=\"Sufijo\" class=\"form-control\">\r\n\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-4 columna-pegada\">\r\n      <input name=\"comentario\" type=\"text\" ng-model=\"vm.telefono.comentario\" placeholder=\"Comentario\" class=\"form-control\">\r\n  </div>\r\n</div>\r\n");
$templateCache.put("integragenerico/components/formly/telefono/telefono.html","<form novalidate name=\"vm.form\" longitud-telefono=\"vm.telefono.tipo.longitud\">\r\n	<div class=\"row\" ng-if=\"vm.telefono.tipo.maximoPrefijo\" ng-include=\"\'integragenerico/components/formly/telefono/telefono-con-prefijo.html\'\"></div>\r\n	<div class=\"row\" ng-if=\"!vm.telefono.tipo.maximoPrefijo\" ng-include=\"\'integragenerico/components/formly/telefono/telefono-sin-prefijo.html\'\"></div>\r\n</form>\r\n");
$templateCache.put("integragenerico/components/formly/texto-estatico/texto-estatico.html","<span class=\"form-control-static\" ng-model=\"model[options.key]\" ng-bind=\"model[options.key]\" texto-estatico></span>\r\n");
$templateCache.put("integragenerico/components/formly/timepicker/timepicker.html","<div uib-timepicker ng-disabled=\"to.disabled\" ng-model=\"model[options.key]\" minute-step=\"to.minuteStep\" show-spinners=\"false\" show-meridian=\"to.showMeridian\">\r\n</div>\r\n");
$templateCache.put("integragenerico/components/usuarios/cambiar-pass/cambiar-clave.html","<div class=\"modal-header\">\r\n  <h3 class=\"modal-title\">Cambiar clave</h3>\r\n</div>\r\n<div class=\"modal-body\">\r\n<div class=\"row\">\r\n  <formly-form model=\"clave\" fields=\"camposCambiarClave\" form=\"formularioCambiarClave\">\r\n  <div class=\"container-fluid col-lg-12\">\r\n    <div class=\"col-lg-2 col-lg-offset-4 col-md-3 col-md-offset-3 col-xs-6\">\r\n      <button type=\"button\" class=\"btn btn-success btn-block\" data-dismiss=\"modal\" ng-click=\"cambiarClaves()\" ng-disabled=\"formularioCambiarClave.$invalid\">\r\n        <span class=\"glyphicon glyphicon-ok\"></span> Aceptar\r\n      </button>\r\n    </div>\r\n    <div class=\"col-lg-2 col-md-3 col-xs-6\">\r\n      <button  type=\"button\" class=\"btn btn-danger btn-block\" ng-click=\"cancelar()\">\r\n        <span class=\"glyphicon glyphicon-remove\"></span> Cancelar\r\n      </button>\r\n    </div>\r\n  </div>\r\n</formly-form>\r\n</div>\r\n</div>\r\n\r\n\r\n\r\n");}]);
(function () {
  'use strict';

  angular
    .module('integragenerico').service('ServicioTablaConsulta', ServicioTablaConsulta);

  function ServicioTablaConsulta($injector, $state, $uibModal, NgTableParams, CamposListado, AccionesGenerales, Filtro, Paginacion, Orden, Dialogos, TransformacionString, ConfigurarListados, ConfigurarConfirmaciones) {

    var vm = this;

    vm.getTabla = getTabla;

    vm.abrirDialogoSeleccionar = abrirDialogoSeleccionar;

    function Tabla(clave) {
      var lista = {
        listar: listar,
        resultados: [],
        cargando: false,
        acciones: {},
        prefijo: TransformacionString.camelToUnderscore(clave)
      };
      lista.acciones.editar = function (row) {
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_SELECCIONAR',
          tieneTitulo: true,
          cancelar: true
        };
        Dialogos.abrirDialogoConfirmacion(config)
          .result.then(abrirDialogoTransicion)
          .then(function () {
            confirmarSeleccion(row);
          });
      };
      lista.acciones.nuevoModal = function () {
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_NUEVO',
          tieneTitulo: true,
          cancelar: true
        };
        if (ConfigurarConfirmaciones.alCrear) {
          Dialogos.abrirDialogoConfirmacion(config)
            .result.then(abrirDialogoTransicion)
            .then(function () {
              // confirmarEdicion('nuevo');
              abrirDialogoNuevo(clave, function () {
                lista.tableParams.reload();
              });
            });
        } else {
          abrirDialogoNuevo(clave, function () {
            lista.tableParams.reload();
          });
        }
      };
      return CamposListado.getCampos(clave).then(function (data) {
        data[data.length - 1].acciones = [];
        // vm.tabla.acciones.seleccionar = seleccionar(fila);

        data[data.length - 1].acciones.push({
          accion: "tabla.acciones.seleccionar(row)",
          claseBoton: "btn-primary",
          icono: "fa fa-thumb-tack"
        });

        lista.cols = data;

        return Orden.getOrdenPorDefecto(clave).then(function (orden) {
          lista.filtro = Filtro.getFiltro(lista);
          lista.filtrosActivos = false;
          lista.mostrarFiltros = mostrarFiltros;
          lista.ordenPorDefecto = orden;
          lista.tableParams = crearTabla(clave, lista);
          return AccionesGenerales.getAcciones(clave, true).then(function (acciones) {
            lista.accionesGenerales = acciones;
            return lista;
          });
        });
      });
    }

    function crearTabla(clave, lista) {
      var parametros = Paginacion.getParametros(clave);
      // console.log(lista);
      parametros.sorting = lista.ordenPorDefecto;
      // console.log(parametros);
      return new NgTableParams(parametros, {
        filterDelay: Filtro.getDelay(),
        paginationMinBlocks: Paginacion.getBloquesMinimos,
        getData: function (params) {
          lista.cargando = true;
          lista.resultados = lista.listar(clave, params, lista.filtro.filtro).then(function (resultados) {
            lista.cargando = false;
            return resultados;
          });
          return lista.resultados;
        }
      });
    }

    function listar(clave, params, filtro) {
      this.cargando = true;
      var parametros = params.parameters();
      var query = {};
      if (ConfigurarListados.filtra) {
        Filtro.generarFiltros(query, parametros.filter, filtro);
      }
      if (ConfigurarListados.ordena) {
        Orden.generarOrden(query, parametros.sorting);
      }
      if (ConfigurarListados.pagina) {
        Paginacion.generarPaginacion(query, parametros.page, parametros.count);
      }
      if (ConfigurarListados.pagina) {
        return $injector.get(clave).get(query).$promise.then(function (data) {
          params.total(data.totalElements);
          return data.content;
        });
      } else {
        return $injector.get(clave).query(query).$promise.then(function (data) {
          return data;
        });
      }
    }

    function getTabla(clave) {
      return new Tabla(clave);
    }

    function abrirDialogoTransicion() {
      return Dialogos.abrirDialogoTransicionEstados();
    }

    function abrirDialogoCargando() {
      return Dialogos.abrirDialogoCargando();
    }

    function mostrarFiltros() {
      this.filtrosActivos = !this.filtrosActivos;
    }

    function abrirDialogoSeleccionar(clave, callbackSeleccionado, callbackSalida, funcionFiltros) {
      var modalInstance = $uibModal.open({
        templateUrl: 'integragenerico/components/lista-elementos/vista-tabla-elementos-modal.html',
        size: 'lg',
        backdrop: 'static',
        controller: function ($uibModalInstance, tabla, funcionFiltros) {
          var vm = this;
          vm.tabla = tabla;
          if (funcionFiltros) {
            funcionFiltros(vm.tabla);
          }
          vm.tabla.acciones.seleccionar = function (fila) {
            $uibModalInstance.close(fila);
          }
        },
        controllerAs: 'vm',
        resolve: {
          tabla: function (ServicioTablaConsulta) {
            return ServicioTablaConsulta.getTabla(clave).then(function (tabla) {
              return tabla;
            });
          },
          funcionFiltros: function () {
            return funcionFiltros;
          }
        }
      });
      modalInstance.result.then(callbackSeleccionado, callbackSalida);
    }

    function abrirDialogoNuevo(clave, callbackCerrado) {
      var propiedades = {};
      Dialogos.abrirDialogoTransicionEstados();
      var modalInstance = $uibModal.open({
        templateUrl: 'integragenerico/components/editar-elemento/vista-editar-elemento-modal.html',
        backdrop: 'static',
        windowClass: 'app-modal-window',
        controller: function ($uibModalInstance, formulario) {
          var vm = this;
          vm.formulario = formulario;
          Dialogos.cerrarDialogoTransicionEstados();

        },
        controllerAs: 'vm',
        resolve: {
          formulario: function (ServicioEdicion) {
            return ServicioEdicion.getFormulario(clave, null, null, propiedades);
          }
        }
      });
      propiedades.modalInstance = modalInstance;
      modalInstance.result.then(function () {
        Dialogos.cerrarDialogoTransicionEstados();
        callbackCerrado();
      });
    }

  }

})();

(function () {
	'use strict';
	angular
	.module('integragenerico')
	.controller('DialogoConfirmacionCtrl', DialogoConfirmacionCtrl);
	function DialogoConfirmacionCtrl($scope, $uibModalInstance, prefijo, tieneTitulo, cancelar, textoDinamico ) {

		var vm = this;

		vm.tieneTitulo = tieneTitulo;
		vm.cancelar = cancelar;
		vm.textoDinamico = textoDinamico;
		vm.titulo = prefijo+'.TITULO';
		vm.contenido = vm.textoDinamico ? vm.textoDinamico : prefijo+'.CONTENIDO';
		vm.textoSi = prefijo+'.TEXTO_SI';
		vm.textoNo = prefijo+'.TEXTO_NO';
		vm.si = function() {
			$uibModalInstance.close(true);
		};

		vm.no = function() {
			$uibModalInstance.dismiss(false);
		};
	}
})();

(function () {
	'use strict';
	angular
	.module('integragenerico')
	.controller('DialogoInformacionCtrl', DialogoInformacionCtrl);
	function DialogoInformacionCtrl($scope, $uibModalInstance, prefijo, tieneTitulo, textoDinamico ) {

		var vm = this;

		vm.tieneTitulo = tieneTitulo;
		vm.textoDinamico = textoDinamico;
		vm.titulo = 'Información';
		vm.contenido = vm.textoDinamico ? vm.textoDinamico : prefijo+'.CONTENIDO';
		vm.textoCerrar = 'Cerrar';
		vm.cerrar = function() {
			$uibModalInstance.close(true);
		};
	}
})();

(function () {
	'use strict';
	angular
	.module('integragenerico')
	.controller('DialogoMostrarImagenCtrl', DialogoMostrarImagenCtrl);
	function DialogoMostrarImagenCtrl($scope, $modalInstance, titulo, img, info ) {

		var vm = this;
		vm.titulo = titulo;
		vm.img = img;
		vm.info = info;

		vm.cerrar = function() {
			$modalInstance.close(true);
		};
	}
})();
(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name clienteWebBondisvmApp.ServicioDialogos
   * @description
   * # ServicioDialogos
   * Provee servicios para hacer mas simple la instanciacion de dialogos.
   */
  angular.module('integragenerico')
    .service('Dialogos', Dialogos);


  function Dialogos($uibModal, ConfigurarDialogos) {

    var vm = this;

    var dialogoTransicion = null;

    this.abrirDialogoTransicionEstados = function (config) {
      if (!dialogoTransicion) {
        dialogoTransicion = vm.abrirDialogoCargando(config);
      }
    };

    this.cerrarDialogoTransicionEstados = function () {
      if (dialogoTransicion) {
        dialogoTransicion.opened.then(function () {
          dialogoTransicion.close();
          dialogoTransicion = null;
        });

      }
    };

    /**
     * Crea un dialogo de cargando con un determinado titulo y lo
     * devuelve.
     * @param String prefijo el prefijo de las propiedades usadas para
     * traducir el texto del dialogo.
     * @param Function onOpen una funcion que se llamara cuando el
     * dialogo se abra.
     * @returns Object un manejador del dialogo que puede usarse para
     * interactuar con el.
     */
    this.abrirDialogoCargando = function (config) {
      if (!config) {
        config = {};
      }
      if (!config.prefijo) {
        config.prefijo = 'GLOBAL.CARGANDO';
      }
      var respuesta = $uibModal.open({
        templateUrl: 'integragenerico/components/dialogos/dialogo-cargando.html',
        controllerAs: 'vm',
        controller: function (prefijo) {
          var vm = this;
          vm.titulo = prefijo + '.TITULO';
          vm.contenido = prefijo + '.CONTENIDO';
        },
        resolve: {
          prefijo: function () {
            return config.prefijo;
          }
        },
        backdrop: ConfigurarDialogos.backdrop
      });
      if (config.onOpen) {
        respuesta.opened.then(config.onOpen);
      }
      return respuesta;
    };

    /**
     * Crea un dialogo de confirmacion con las propiedades pasadas y lo
     * devuelve.
     * @param String prefijo el prefijo de las propiedades desde donde
     * se obtendran los textos para el titulo, contenido, boton si y
     * boton no.
     * @param Boolean tieneTitulo indica si el cuadro de dialogo tiene titulo.
     * @param Boolean cancelar indica si el cuadro de dialogo tendra boton
     * cancelar.
     * @param Function onResult una funcion a ejecutar cuando se cierra
     * el dialogo
     * @returns Object un manejador del dialogo que puede usarse para
     * interactuar con el.
     */
    this.abrirDialogoConfirmacion = function (config) {
      var respuesta = $uibModal.open({
        templateUrl: 'integragenerico/components/dialogos/dialogo-confirmacion.html',
        controllerAs: 'vm',
        controller: 'DialogoConfirmacionCtrl',
        size: 'sm',
        backdrop: 'static',
        resolve: {
          tieneTitulo: function () {
            return config.tieneTitulo;
          },
          prefijo: function () {
            return config.prefijo;
          },
          cancelar: function () {
            return config.cancelar;
          },
          textoDinamico: function () {
            return config.textoDinamico;
          }
        }
      });
      respuesta.result.then(config.onResult, function (info) {
        if (config.callbackCancelar) {
          return config.callbackCancelar(info);
        }
      });
      return respuesta;
    };

    this.abrirDialogoMostrarImagen = function (titulo, img, info, onOpen) {
      var respuesta = $uibModal.open({
        templateUrl: 'integragenerico/components/dialogos/dialogo-mostrar-imagen.html',
        controllerAs: 'vm',
        controller: 'DialogoMostrarImagenCtrl',
        windowClass: 'box expanded expanded-padding',
        keyboard: false,
        backdrop: 'static',
        resolve: {
          titulo: function () {
            return titulo;
          },
          img: function () {
            return img;
          },
          info: function () {
            return info;
          }
        }
      });
      if (onOpen) {
        respuesta.opened.then(onOpen);
      }
      return respuesta;
    };

    this.abrirDialogoInformacion = function (config) {
      var respuesta = $uibModal.open({
        templateUrl: 'integragenerico/components/dialogos/dialogo-informacion.html',
        controllerAs: 'vm',
        controller: 'DialogoInformacionCtrl',
        size: 'sm',
        resolve: {
          tieneTitulo: function () {
            return config.tieneTitulo;
          },
          prefijo: function () {
            return config.prefijo;
          },
          textoDinamico: function () {
            return config.textoDinamico;
          }
        }
      });
      if (config.onOpen) {
        respuesta.opened.then(config.onOpen);
      }
      return respuesta;
    };
  }

})();

(function() {
  'use strict';

  angular
  .module('devoops')
  .service('devoops', devoops);

  /** @ngInject */
  function devoops($,$window, $timeout) {
    var window = $window;
    var setTimeout = $timeout;
    this.inicializar = function() {

//
//    Main script of DevOOPS v1.0 Bootstrap Theme
//

/*-------------------------------------------
 Dynamically load plugin scripts
 ---------------------------------------------*/
//
// Dynamically load Fullcalendar Plugin Script
// homepage: http://arshaw.com/fullcalendar
// require moment.js
//
function LoadCalendarScript(callback) {
    function LoadFullCalendarScript() {
        if (!$.fn.fullCalendar) {
            $.getScript('plugins/fullcalendar/fullcalendar.js', callback);
        }
        else {
            if (callback && typeof (callback) === "function") {
                callback();
            }
        }
    }
    if (!$.fn.moment) {
        $.getScript('plugins/moment/moment.min.js', LoadFullCalendarScript);
    }
    else {
        LoadFullCalendarScript();
    }
}
//
// Dynamically load  OpenStreetMap Plugin
// homepage: http://openlayers.org
//
function LoadOpenLayersScript(callback) {
    if (!$.fn.OpenLayers) {
        $.getScript('http://www.openlayers.org/api/OpenLayers.js', callback);
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load  jQuery Timepicker plugin
//  homepage: http://trentrichardson.com/examples/timepicker/
//
function LoadTimePickerScript(callback) {
    if (!$.fn.timepicker) {
        $.getScript('plugins/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js', callback);
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load Bootstrap Validator Plugin
//  homepage: https://github.com/nghuuphuoc/bootstrapvalidator
//
function LoadBootstrapValidatorScript(callback) {
    if (!$.fn.bootstrapValidator) {
        $.getScript('plugins/bootstrapvalidator/bootstrapValidator.min.js', callback);
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load jQuery Select2 plugin
//  homepage: https://github.com/ivaynberg/select2  v3.4.5  license - GPL2
//
function LoadSelect2Script(callback) {
    if (!$.fn.select2) {
        $.getScript('plugins/select2/select2.min.js', callback);
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load DataTables plugin
//  homepage: http://datatables.net v1.9.4 license - GPL or BSD
//
function LoadDataTablesScripts(callback) {
    function LoadDatatables() {
        $.getScript('plugins/datatables/jquery.dataTables.js', function() {
            $.getScript('plugins/datatables/ZeroClipboard.js', function() {
                $.getScript('plugins/datatables/TableTools.js', function() {
                    $.getScript('plugins/datatables/dataTables.bootstrap.js', callback);
                });
            });
        });
    }
    if (!$.fn.dataTables) {
        LoadDatatables();
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load Widen FineUploader
//  homepage: https://github.com/Widen/fine-uploader  v5.0.1 license - GPL3
//
function LoadFineUploader(callback) {
    if (!$.fn.fineuploader) {
        $.getScript('plugins/fineuploader/jquery.fineuploader-5.0.1.min.js', callback);
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load xCharts plugin
//  homepage: http://tenxer.github.io/xcharts/ v0.3.0 license - MIT
//  Required D3 plugin http://d3js.org/ v3.4.1 license - MIT
//
function LoadXChartScript(callback) {
    function LoadXChart() {
        $.getScript('plugins/xcharts/xcharts.min.js', callback);
    }
    function LoadD3Script() {
        if (!$.fn.d3) {
            $.getScript('plugins/d3/d3.v3.min.js', LoadXChart)
        }
        else {
            LoadXChart();
        }
    }
    if (!$.fn.xcharts) {
        LoadD3Script();
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load Flot plugin
//  homepage: http://www.flotcharts.org  v0.8.2 license- MIT
//
function LoadFlotScripts(callback) {
    function LoadFlotScript() {
        $.getScript('plugins/flot/jquery.flot.js', LoadFlotResizeScript);
    }
    function LoadFlotResizeScript() {
        $.getScript('plugins/flot/jquery.flot.resize.js', LoadFlotTimeScript);
    }
    function LoadFlotTimeScript() {
        $.getScript('plugins/flot/jquery.flot.time.js', callback);
    }
    if (!$.fn.flot) {
        LoadFlotScript();
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load Morris Charts plugin
//  homepage: http://www.oesmith.co.uk/morris.js/ v0.4.3 License - MIT
//  require Raphael http://raphael.js
//
function LoadMorrisScripts(callback) {
    function LoadMorrisScript() {
        if (!$.fn.Morris) {
            $.getScript('plugins/morris/morris.min.js', callback);
        }
        else {
            if (callback && typeof (callback) === "function") {
                callback();
            }
        }
    }
    if (!$.fn.raphael) {
        $.getScript('plugins/raphael/raphael-min.js', LoadMorrisScript);
    }
    else {
        LoadMorrisScript();
    }
}
//
//  Dynamically load Fancybox 2 plugin
//  homepage: http://fancyapps.com/fancybox/ v2.1.5 License - MIT
//
function LoadFancyboxScript(callback) {
    if (!$.fn.fancybox) {
        $.getScript('plugins/fancybox/jquery.fancybox.js', callback);
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load jQuery-Knob plugin
//  homepage: http://anthonyterrien.com/knob/  v1.2.5 License- MIT or GPL
//
function LoadKnobScripts(callback) {
    if (!$.fn.knob) {
        $.getScript('plugins/jQuery-Knob/jquery.knob.js', callback);
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
//
//  Dynamically load Sparkline plugin
//  homepage: http://omnipotent.net/jquery.sparkline v2.1.2  License - BSD
//
function LoadSparkLineScript(callback) {
    if (!$.fn.sparkline) {
        $.getScript('plugins/sparkline/jquery.sparkline.min.js', callback);
    }
    else {
        if (callback && typeof (callback) === "function") {
            callback();
        }
    }
}
/*-------------------------------------------
 Main scripts used by theme
 ---------------------------------------------*/
//
//  Function for load content from url and put in $('.ajax-content') block
//
function LoadAjaxContent(url) {
    $('.preloader').show();

    /*	$http({
     method:'JSONP',
     url: url,
     success: function(data) {
     $('#ajax-content').html(data);
     $('.preloader').hide();
     },
     error: function (jqXHR, textStatus, errorThrown) {
     alert(errorThrown);
     },
     dataType: "html",
     async: false
 });*/


$.ajax({
        mimeType: 'text/html; charset=utf-8', // ! Need set mimeType only when run from local file
        url: url,
        type: 'GET',
        success: function(data) {
            $('#ajax-content').html(data);
            $('.preloader').hide();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        },
        dataType: "html",
        async: false
    });
}
//
//  Function maked all .box selector is draggable, to disable for concrete element add class .no-drop
//
function WinMove() {
    $("div.box").not('.no-drop')
    .draggable({
        revert: true,
        zIndex: 2000,
        cursor: "crosshair",
        handle: '.box-name',
        opacity: 0.8
    })
    .droppable({
        tolerance: 'pointer',
        drop: function(event, ui) {
            var draggable = ui.draggable;
            var droppable = $(this);
            var dragPos = draggable.position();
            var dropPos = droppable.position();
            draggable.swap(droppable);
            setTimeout(function() {
                var dropmap = droppable.find('[id^=map-]');
                var dragmap = draggable.find('[id^=map-]');
                if (dragmap.length > 0 || dropmap.length > 0) {
                    dragmap.resize();
                    dropmap.resize();
                }
                else {
                    draggable.resize();
                    droppable.resize();
                }
            }, 50);
            setTimeout(function() {
                draggable.find('[id^=map-]').resize();
                droppable.find('[id^=map-]').resize();
            }, 250);
        }
    });
}
//
// Swap 2 elements on page. Used by WinMove function
//
jQuery.fn.swap = function(b) {
    b = jQuery(b)[0];
    var a = this[0];
    var t = a.parentNode.insertBefore(document.createTextNode(''), a);
    b.parentNode.insertBefore(a, b);
    t.parentNode.insertBefore(b, t);
    t.parentNode.removeChild(t);
    return this;
};
//
//  Screensaver function
//  used on locked screen, and write content to element with id - canvas
//
function ScreenSaver() {
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    // Size of canvas set to fullscreen of browser
    var W = window.innerWidth;
    var H = window.innerHeight;
    canvas.width = W;
    canvas.height = H;
    // Create array of particles for screensaver
    var particles = [];
    for (var i = 0; i < 25; i++) {
        particles.push(new Particle());
    }
    function Particle() {
        // location on the canvas
        this.location = {x: Math.random() * W, y: Math.random() * H};
        // radius - lets make this 0
        this.radius = 0;
        // speed
        this.speed = 3;
        // random angle in degrees range = 0 to 360
        this.angle = Math.random() * 360;
        // colors
        var r = Math.round(Math.random() * 255);
        var g = Math.round(Math.random() * 255);
        var b = Math.round(Math.random() * 255);
        var a = Math.random();
        this.rgba = "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
    }
    // Draw the particles
    function draw() {
        // re-paint the BG
        // Lets fill the canvas black
        // reduce opacity of bg fill.
        // blending time
        ctx.globalCompositeOperation = "source-over";
        ctx.fillStyle = "rgba(0, 0, 0, 0.02)";
        ctx.fillRect(0, 0, W, H);
        ctx.globalCompositeOperation = "lighter";
        for (var i = 0; i < particles.length; i++) {
            var p = particles[i];
            ctx.fillStyle = "white";
            ctx.fillRect(p.location.x, p.location.y, p.radius, p.radius);
            // Lets move the particles
            // So we basically created a set of particles moving in random direction
            // at the same speed
            // Time to add ribbon effect
            for (var n = 0; n < particles.length; n++) {
                var p2 = particles[n];
                // calculating distance of particle with all other particles
                var yd = p2.location.y - p.location.y;
                var xd = p2.location.x - p.location.x;
                var distance = Math.sqrt(xd * xd + yd * yd);
                // draw a line between both particles if they are in 200px range
                if (distance < 200) {
                    ctx.beginPath();
                    ctx.lineWidth = 1;
                    ctx.moveTo(p.location.x, p.location.y);
                    ctx.lineTo(p2.location.x, p2.location.y);
                    ctx.strokeStyle = p.rgba;
                    ctx.stroke();
                    //The ribbons appear now.
                }
            }
            // We are using simple vectors here
            // New x = old x + speed * cos(angle)
            p.location.x = p.location.x + p.speed * Math.cos(p.angle * Math.PI / 180);
            // New y = old y + speed * sin(angle)
            p.location.y = p.location.y + p.speed * Math.sin(p.angle * Math.PI / 180);
            // You can read about vectors here:
            // http://physics.about.com/od/mathematics/a/VectorMath.htm
            if (p.location.x < 0)
                p.location.x = W;
            if (p.location.x > W)
                p.location.x = 0;
            if (p.location.y < 0)
                p.location.y = H;
            if (p.location.y > H)
                p.location.y = 0;
        }
    }
    setInterval(draw, 30);
}
//
// Helper for draw Google Chart
//
function drawGoogleChart(chart_data, chart_options, element, chart_type) {
    // Function for visualize Google Chart
    var data = google.visualization.arrayToDataTable(chart_data);
    var chart = new chart_type(document.getElementById(element));
    chart.draw(data, chart_options);
}
//
//  Function for Draw Knob Charts
//
function DrawKnob(elem) {
    elem.knob({
        change: function(value) {
            //console.log("change : " + value);
        },
        release: function(value) {
            //console.log(this.$.attr('value'));
            console.log("release : " + value);
        },
        cancel: function() {
            console.log("cancel : ", this);
        },
        draw: function() {
            // "tron" case
            if (this.$.data('skin') == 'tron') {
                var a = this.angle(this.cv);  // Angle
                var sa = this.startAngle;          // Previous start angle
                var sat = this.startAngle;         // Start angle
                var ea;                            // Previous end angle
                var eat = sat + a;                 // End angle
                var r = 1;
                this.g.lineWidth = this.lineWidth;
                this.o.cursor
                && (sat = eat - 0.3)
                && (eat = eat + 0.3);
                if (this.o.displayPrevious) {
                    ea = this.startAngle + this.angle(this.v);
                    this.o.cursor
                    && (sa = ea - 0.3)
                    && (ea = ea + 0.3);
                    this.g.beginPath();
                    this.g.strokeStyle = this.pColor;
                    this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                    this.g.stroke();
                }
                this.g.beginPath();
                this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                this.g.stroke();
                this.g.lineWidth = 2;
                this.g.beginPath();
                this.g.strokeStyle = this.o.fgColor;
                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                this.g.stroke();
                return false;
            }
        }
    });
    // Example of infinite knob, iPod click wheel
    var v;
    var up = 0;
    var down = 0;
    var i = 0;
    var $idir = $("div.idir");
    var $ival = $("div.ival");
    var incr = function() {
        i++;
        $idir.show().html("+").fadeOut();
        $ival.html(i);
    }
    var decr = function() {
        i--;
        $idir.show().html("-").fadeOut();
        $ival.html(i);
    };
    $("input.infinite").knob(
    {
        min: 0,
        max: 20,
        stopper: false,
        change: function() {
            if (v > this.cv) {
                if (up) {
                    decr();
                    up = 0;
                } else {
                    up = 1;
                    down = 0;
                }
            } else {
                if (v < this.cv) {
                    if (down) {
                        incr();
                        down = 0;
                    } else {
                        down = 1;
                        up = 0;
                    }
                }
            }
            v = this.cv;
        }
    });
}
//
// Create OpenLayers map with required options and return map as object
//
function drawMap(lon, lat, elem, layers) {
    var LayersArray = [];
    // Map initialization
    var map = new OpenLayers.Map(elem);
    // Add layers on map
    map.addLayers(layers);
    // WGS 1984 projection
    var epsg4326 = new OpenLayers.Projection("EPSG:4326");
    //The map projection (Spherical Mercator)
    var projectTo = map.getProjectionObject();
    // Max zoom = 17
    var zoom = 10;
    map.zoomToMaxExtent();
    // Set longitude/latitude
    var lonlat = new OpenLayers.LonLat(lon, lat);
    map.setCenter(lonlat.transform(epsg4326, projectTo), zoom);
    var layerGuest = new OpenLayers.Layer.Vector("You are here");
    // Define markers as "features" of the vector layer:
    var guestMarker = new OpenLayers.Feature.Vector(
        new OpenLayers.Geometry.Point(lon, lat).transform(epsg4326, projectTo)
        );
    layerGuest.addFeatures(guestMarker);
    LayersArray.push(layerGuest);
    map.addLayers(LayersArray);
    // If map layers > 1 then show checker
    if (layers.length > 1) {
        map.addControl(new OpenLayers.Control.LayerSwitcher({'ascending': true}));
    }
    // Link to current position
    map.addControl(new OpenLayers.Control.Permalink());
    // Show current mouse coords
    map.addControl(new OpenLayers.Control.MousePosition({displayProjection: epsg4326}));
    return map
}
//
//  Function for create 2 dates in human-readable format (with leading zero)
//
function PrettyDates() {
    var currDate = new Date();
    var year = currDate.getFullYear();
    var month = currDate.getMonth() + 1;
    var startmonth = 1;
    if (month > 3) {
        startmonth = month - 2;
    }
    if (startmonth <= 9) {
        startmonth = '0' + startmonth;
    }
    if (month <= 9) {
        month = '0' + month;
    }
    var day = currDate.getDate();
    if (day <= 9) {
        day = '0' + day;
    }
    var startdate = year + '-' + startmonth + '-01';
    var enddate = year + '-' + month + '-' + day;
    return [startdate, enddate];
}
//
//  Function set min-height of window (required for this theme)
//
function SetMinBlockHeight(elem) {
    elem.css('min-height', window.innerHeight - 49)
}
//
//  Helper for correct size of Messages page
//
function MessagesMenuWidth() {
    var W = window.innerWidth;
    var W_menu = $('#sidebar-left').outerWidth();
    var w_messages = (W - W_menu) * 16.666666666666664 / 100;
    $('#messages-menu').width(w_messages);
}
//
// Function for change panels of Dashboard
//
function DashboardTabChecker() {
    $('#content').on('click', 'a.tab-link', function(e) {
        e.preventDefault();
        $('div#dashboard_tabs').find('div[id^=dashboard]').each(function() {
            $(this).css('visibility', 'hidden').css('position', 'absolute');
        });
        var attr = $(this).attr('id');
        $('#' + 'dashboard-' + attr).css('visibility', 'visible').css('position', 'relative');
        $(this).closest('.nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');
    });
}
//
// Helper for run TinyMCE editor with textarea's
//
function TinyMCEStart(elem, mode) {
    var plugins = [];
    if (mode == 'extreme') {
        plugins = ["advlist anchor autolink autoresize autosave bbcode charmap code contextmenu directionality ",
        "emoticons fullpage fullscreen hr image insertdatetime layer legacyoutput",
        "link lists media nonbreaking noneditable pagebreak paste preview print save searchreplace",
        "tabfocus table template textcolor visualblocks visualchars wordcount"]
    }
    tinymce.init({selector: elem,
        theme: "modern",
        plugins: plugins,
        //content_css: "css/style.css",
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
        style_formats: [
        {title: 'Header 2', block: 'h2', classes: 'page-header'},
        {title: 'Header 3', block: 'h3', classes: 'page-header'},
        {title: 'Header 4', block: 'h4', classes: 'page-header'},
        {title: 'Header 5', block: 'h5', classes: 'page-header'},
        {title: 'Header 6', block: 'h6', classes: 'page-header'},
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });
}
//
// Helper for draw Sparkline plots on Dashboard page
//
function SparkLineDrawBarGraph(elem, arr, color) {
    if (color) {
        var stacked_color = color;
    }
    else {
        stacked_color = '#6AA6D6'
    }
    elem.sparkline(arr, {type: 'bar', barWidth: 7, highlightColor: '#000', barSpacing: 2, height: 40, stackedBarColor: stacked_color});
}
//
//  Helper for open ModalBox with requested header, content and bottom
//
//
function OpenModalBox(header, inner, bottom) {
    var modalbox = $('#modalbox');
    modalbox.find('.modal-header-name span').html(header);
    modalbox.find('.devoops-modal-inner').html(inner);
    modalbox.find('.devoops-modal-bottom').html(bottom);
    modalbox.fadeIn('fast');
    $('body').addClass("body-expanded");
}
//
//  Close modalbox
//
//
function CloseModalBox() {
    var modalbox = $('#modalbox');
    modalbox.fadeOut('fast', function() {
        modalbox.find('.modal-header-name span').children().remove();
        modalbox.find('.devoops-modal-inner').children().remove();
        modalbox.find('.devoops-modal-bottom').children().remove();
        $('body').removeClass("body-expanded");
    });
}
//
//  Beauty tables plugin (navigation in tables with inputs in cell)
//  Created by DevOOPS.
//
(function($) {
    $.fn.beautyTables = function() {
        var table = this;
        var string_fill = false;
        this.on('keydown', function(event) {
            var target = event.target;
            var tr = $(target).closest("tr");
            var col = $(target).closest("td");
            if (target.tagName.toUpperCase() == 'INPUT') {
                if (event.shiftKey === true) {
                    switch (event.keyCode) {
                        case 37: // left arrow
                        col.prev().children("input[type=text]").focus();
                        break;
                        case 39: // right arrow
                        col.next().children("input[type=text]").focus();
                        break;
                        case 40: // down arrow
                        if (string_fill == false) {
                            tr.next().find('td:eq(' + col.index() + ') input[type=text]').focus();
                        }
                        break;
                        case 38: // up arrow
                        if (string_fill == false) {
                            tr.prev().find('td:eq(' + col.index() + ') input[type=text]').focus();
                        }
                        break;
                    }
                }
                if (event.ctrlKey === true) {
                    switch (event.keyCode) {
                        case 37: // left arrow
                        tr.find('td:eq(1)').find("input[type=text]").focus();
                        break;
                        case 39: // right arrow
                        tr.find('td:last-child').find("input[type=text]").focus();
                        break;
                        case 40: // down arrow
                        if (string_fill == false) {
                            table.find('tr:last-child td:eq(' + col.index() + ') input[type=text]').focus();
                        }
                        break;
                        case 38: // up arrow
                        if (string_fill == false) {
                            table.find('tr:eq(1) td:eq(' + col.index() + ') input[type=text]').focus();
                        }
                        break;
                    }
                }
                if (event.keyCode == 13 || event.keyCode == 9) {
                    event.preventDefault();
                    col.next().find("input[type=text]").focus();
                }
                if (string_fill == false) {
                    if (event.keyCode == 34) {
                        event.preventDefault();
                        table.find('tr:last-child td:last-child').find("input[type=text]").focus();
                    }
                    if (event.keyCode == 33) {
                        event.preventDefault();
                        table.find('tr:eq(1) td:eq(1)').find("input[type=text]").focus();
                    }
                }
            }
        });
table.find("input[type=text]").each(function() {
    $(this).on('blur', function(event) {
        var target = event.target;
        var col = $(target).parents("td");
        if (table.find("input[name=string-fill]").prop("checked") == true) {
            col.nextAll().find("input[type=text]").each(function() {
                $(this).val($(target).val());
            });
        }
    });
})
};
})(jQuery);
//
// Beauty Hover Plugin (backlight row and col when cell in mouseover)
//
//
(function($) {
    $.fn.beautyHover = function() {
        var table = this;
        table.on('mouseover', 'td', function() {
            var idx = $(this).index();
            var rows = $(this).closest('table').find('tr');
            rows.each(function() {
                $(this).find('td:eq(' + idx + ')').addClass('beauty-hover');
            });
        })
        .on('mouseleave', 'td', function(e) {
            var idx = $(this).index();
            var rows = $(this).closest('table').find('tr');
            rows.each(function() {
                $(this).find('td:eq(' + idx + ')').removeClass('beauty-hover');
            });
        });
    };
})(jQuery);
//
//  Function convert values of inputs in table to JSON data
//
//
function Table2Json(table) {
    var result = {};
    table.find("tr").each(function() {
        var oneRow = [];
        var varname = $(this).index();
        $("td", this).each(function(index) {
            if (index != 0) {
                oneRow.push($("input", this).val());
            }
        });
        result[varname] = oneRow;
    });
    var result_json = JSON.stringify(result);
    OpenModalBox('Table to JSON values', result_json);
}
/*-------------------------------------------
 Demo graphs for Flot Chart page (charts_flot.html)
 ---------------------------------------------*/
//
// Graph1 created in element with id = box-one-content
//
function FlotGraph1() {
    // We use an inline data source in the example, usually data would
    // be fetched from a server
    var data = [],
    totalPoints = 300;
    function getRandomData() {
        if (data.length > 0)
            data = data.slice(1);
        // Do a random walk
        while (data.length < totalPoints) {
            var prev = data.length > 0 ? data[data.length - 1] : 50,
            y = prev + Math.random() * 10 - 5;
            if (y < 0) {
                y = 0;
            } else if (y > 100) {
                y = 100;
            }
            data.push(y);
        }
        // Zip the generated y values with the x values
        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]])
        }
        return res;
    }
    var updateInterval = 30;
    var plot = $.plot("#box-one-content", [getRandomData()], {
        series: {
            shadowSize: 0	// Drawing is faster without shadows
        },
        yaxis: {min: 0, max: 100},
        xaxis: {show: false}
    });
    function update() {
        plot.setData([getRandomData()]);
        // Since the axes don't change, we don't need to call plot.setupGrid()
        plot.draw();
        setTimeout(update, updateInterval);
    }
    update();
}
//
// Graph2 created in element with id = box-two-content
//
function FlotGraph2() {
    var sin = [];
    var cos = [];
    var tan = [];
    for (var i = 0; i < 14; i += 0.1) {
        sin.push([i, Math.sin(i)]);
        cos.push([i, Math.cos(i)]);
        tan.push([i, Math.tan(i) / 4]);
    }
    var plot = $.plot("#box-two-content", [
        {data: sin, label: "sin(x) = -0.00"},
        {data: cos, label: "cos(x) = -0.00"},
        {data: tan, label: "tan(x)/4 = -0.00"}
        ], {
            series: {
                lines: {
                    show: true
                }
            },
            crosshair: {
                mode: "x"
            },
            grid: {
                hoverable: true,
                autoHighlight: false
            },
            yaxis: {
                min: -5.2,
                max: 5.2
            }
        });
    var legends = $("#box-two-content .legendLabel");
    legends.each(function() {
        // fix the widths so they don't jump around
        $(this).css('width', $(this).width());
    });
    var updateLegendTimeout = null;
    var latestPosition = null;
    function updateLegend() {
        updateLegendTimeout = null;
        var pos = latestPosition;
        var axes = plot.getAxes();
        if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max ||
            pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) {
            return;
    }
    var i, j, dataset = plot.getData();
    for (i = 0; i < dataset.length; ++i) {
        var series = dataset[i];
            // Find the nearest points, x-wise
            for (j = 0; j < series.data.length; ++j) {
                if (series.data[j][0] > pos.x) {
                    break;
                }
            }
            // Now Interpolate
            var y, p1 = series.data[j - 1], p2 = series.data[j];
            if (p1 == null) {
                y = p2[1];
            } else if (p2 == null) {
                y = p1[1];
            } else {
                y = p1[1] + (p2[1] - p1[1]) * (pos.x - p1[0]) / (p2[0] - p1[0]);
            }
            legends.eq(i).text(series.label.replace(/=.*/, "= " + y.toFixed(2)));
        }
    }
    $("#box-two-content").bind("plothover", function(event, pos, item) {
        latestPosition = pos;
        if (!updateLegendTimeout) {
            updateLegendTimeout = setTimeout(updateLegend, 50);
        }
    });
}
//
// Graph3 created in element with id = box-three-content
//
function FlotGraph3() {
    var d1 = [];
    for (var i = 0; i <= 60; i += 1) {
        d1.push([i, parseInt(Math.random() * 30 - 10)]);
    }
    function plotWithOptions(t) {
        $.plot("#box-three-content", [{
            data: d1,
            color: "rgb(30, 180, 20)",
            threshold: {
                below: t,
                color: "rgb(200, 20, 30)"
            },
            lines: {
                steps: true
            }
        }]);
    }
    plotWithOptions(0);
}
//
// Graph4 created in element with id = box-four-content
//
function FlotGraph4() {
    var d1 = [];
    for (var i = 0; i < 14; i += 0.5) {
        d1.push([i, Math.sin(i)]);
    }
    var d2 = [[0, 3], [4, 8], [8, 5], [9, 13]];
    var d3 = [];
    for (var i = 0; i < 14; i += 0.5) {
        d3.push([i, Math.cos(i)]);
    }
    var d4 = [];
    for (var i = 0; i < 14; i += 0.1) {
        d4.push([i, Math.sqrt(i * 10)]);
    }
    var d5 = [];
    for (var i = 0; i < 14; i += 0.5) {
        d5.push([i, Math.sqrt(i)]);
    }
    var d6 = [];
    for (var i = 0; i < 14; i += 0.5 + Math.random()) {
        d6.push([i, Math.sqrt(2 * i + Math.sin(i) + 5)]);
    }
    $.plot("#box-four-content", [{
        data: d1,
        lines: {show: true, fill: true}
    }, {
        data: d2,
        bars: {show: true}
    }, {
        data: d3,
        points: {show: true}
    }, {
        data: d4,
        lines: {show: true}
    }, {
        data: d5,
        lines: {show: true},
        points: {show: true}
    }, {
        data: d6,
        lines: {show: true, steps: true}
    }]);
}
/*-------------------------------------------
 Demo graphs for Morris Chart page (charts_morris.html)
 ---------------------------------------------*/
//
// Graph1 created in element with id = morris-chart-1
//
function MorrisChart1() {
    var day_data = [
    {"period": "2013-10-01", "licensed": 3407, "sorned": 660},
    {"period": "2013-09-30", "licensed": 3351, "sorned": 629},
    {"period": "2013-09-29", "licensed": 3269, "sorned": 618},
    {"period": "2013-09-20", "licensed": 3246, "sorned": 661},
    {"period": "2013-09-19", "licensed": 3257, "sorned": 667},
    {"period": "2013-09-18", "licensed": 3248, "sorned": 627},
    {"period": "2013-09-17", "licensed": 3171, "sorned": 660},
    {"period": "2013-09-16", "licensed": 3171, "sorned": 676},
    {"period": "2013-09-15", "licensed": 3201, "sorned": 656},
    {"period": "2013-09-10", "licensed": 3215, "sorned": 622}
    ];
    Morris.Bar({
        element: 'morris-chart-1',
        data: day_data,
        xkey: 'period',
        ykeys: ['licensed', 'sorned'],
        labels: ['Licensed', 'SORN'],
        xLabelAngle: 60
    });
}
//
// Graph2 created in element with id = morris-chart-2
//
function MorrisChart2() {
    // Use Morris.Area instead of Morris.Line
    Morris.Area({
        element: 'morris-chart-2',
        data: [
        {x: '2011 Q1', y: 3, z: 3, m: 1},
        {x: '2011 Q2', y: 2, z: 0, m: 7},
        {x: '2011 Q3', y: 2, z: 5, m: 2},
        {x: '2011 Q4', y: 4, z: 4, m: 5},
        {x: '2012 Q1', y: 6, z: 1, m: 11},
        {x: '2012 Q2', y: 4, z: 4, m: 3},
        {x: '2012 Q3', y: 4, z: 4, m: 7},
        {x: '2012 Q4', y: 4, z: 4, m: 9}
        ],
        xkey: 'x',
        ykeys: ['y', 'z', 'm'],
        labels: ['Y', 'Z', 'M']
    })
    .on('click', function(i, row) {
        console.log(i, row);
    });
}
//
// Graph3 created in element with id = morris-chart-3
//
function MorrisChart3() {
    var decimal_data = [];
    for (var x = 0; x <= 360; x += 10) {
        decimal_data.push({x: x, y: Math.sin(Math.PI * x / 180).toFixed(4), z: Math.cos(Math.PI * x / 180).toFixed(4)});
    }
    Morris.Line({
        element: 'morris-chart-3',
        data: decimal_data,
        xkey: 'x',
        ykeys: ['y', 'z'],
        labels: ['sin(x)', 'cos(x)'],
        parseTime: false,
        goals: [-1, 0, 1]
    });
}
//
// Graph4 created in element with id = morris-chart-4
//
function MorrisChart4() {
    // Use Morris.Bar
    Morris.Bar({
        element: 'morris-chart-4',
        data: [
        {x: '2011 Q1', y: 0},
        {x: '2011 Q2', y: 1},
        {x: '2011 Q3', y: 2},
        {x: '2011 Q4', y: 3},
        {x: '2012 Q1', y: 4},
        {x: '2012 Q2', y: 5},
        {x: '2012 Q3', y: 6},
        {x: '2012 Q4', y: 7},
        {x: '2013 Q1', y: 8},
        {x: '2013 Q2', y: 7},
        {x: '2013 Q3', y: 6},
        {x: '2013 Q4', y: 5},
        {x: '2014 Q1', y: 9}
        ],
        xkey: 'x',
        ykeys: ['y'],
        labels: ['Y'],
        barColors: function(row, series, type) {
            if (type === 'bar') {
                var red = Math.ceil(255 * row.y / this.ymax);
                return 'rgb(' + red + ',0,0)';
            }
            else {
                return '#000';
            }
        }
    });
}
//
// Graph5 created in element with id = morris-chart-5
//
function MorrisChart5() {
    Morris.Area({
        element: 'morris-chart-5',
        data: [
        {period: '2010 Q1', iphone: 2666, ipad: null, itouch: 2647},
        {period: '2010 Q2', iphone: 2778, ipad: 2294, itouch: 2441},
        {period: '2010 Q3', iphone: 4912, ipad: 1969, itouch: 2501},
        {period: '2010 Q4', iphone: 3767, ipad: 3597, itouch: 5689},
        {period: '2011 Q1', iphone: 6810, ipad: 1914, itouch: 2293},
        {period: '2011 Q2', iphone: 5670, ipad: 4293, itouch: 1881},
        {period: '2011 Q3', iphone: 4820, ipad: 3795, itouch: 1588},
        {period: '2011 Q4', iphone: 15073, ipad: 5967, itouch: 5175},
        {period: '2012 Q1', iphone: 10687, ipad: 4460, itouch: 2028},
        {period: '2012 Q2', iphone: 8432, ipad: 5713, itouch: 1791}
        ],
        xkey: 'period',
        ykeys: ['iphone', 'ipad', 'itouch'],
        labels: ['iPhone', 'iPad', 'iPod Touch'],
        pointSize: 2,
        hideHover: 'auto'
    });
}
/*-------------------------------------------
 Demo graphs for Google Chart page (charts_google.html)
 ---------------------------------------------*/
//
// One function for create all graphs on Google Chart page
//
function DrawAllCharts() {
    //  Chart 1
    var chart1_data = [
    ['Smartphones', 'PC', 'Notebooks', 'Monitors', 'Routers', 'Switches'],
    ['01.01.2014', 1234, 2342, 344, 232, 131],
    ['02.01.2014', 1254, 232, 314, 232, 331],
    ['03.01.2014', 2234, 342, 298, 232, 665],
    ['04.01.2014', 2234, 42, 559, 232, 321],
    ['05.01.2014', 1999, 82, 116, 232, 334],
    ['06.01.2014', 1634, 834, 884, 232, 191],
    ['07.01.2014', 321, 342, 383, 232, 556],
    ['08.01.2014', 845, 112, 499, 232, 731]
    ];
    var chart1_options = {
        title: 'Sales of company',
        hAxis: {title: 'Date', titleTextStyle: {color: 'red'}},
        backgroundColor: '#fcfcfc',
        vAxis: {title: 'Quantity', titleTextStyle: {color: 'blue'}}
    };
    var chart1_element = 'google-chart-1';
    var chart1_type = google.visualization.ColumnChart;
    drawGoogleChart(chart1_data, chart1_options, chart1_element, chart1_type);
    //  Chart 2
    var chart2_data = [
    ['Height', 'Width'],
    ['Samsung', 74.5],
    ['Apple', 31.24],
    ['LG', 12.10],
    ['Huawei', 11.14],
    ['Sony', 8.3],
    ['Nokia', 7.4],
    ['Blackberry', 6.8],
    ['HTC', 6.63],
    ['Motorola', 3.5],
    ['Other', 43.15]
    ];
    var chart2_options = {
        title: 'Smartphone marketshare 2Q 2013',
        backgroundColor: '#fcfcfc'
    };
    var chart2_element = 'google-chart-2';
    var chart2_type = google.visualization.PieChart;
    drawGoogleChart(chart2_data, chart2_options, chart2_element, chart2_type);
    //  Chart 3
    var chart3_data = [
    ['Age', 'Weight'],
    [8, 12],
    [4, 5.5],
    [11, 14],
    [4, 5],
    [3, 3.5],
    [6.5, 7]
    ];
    var chart3_options = {
        title: 'Age vs. Weight comparison',
        hAxis: {title: 'Age', minValue: 0, maxValue: 15},
        vAxis: {title: 'Weight', minValue: 0, maxValue: 15},
        legend: 'none',
        backgroundColor: '#fcfcfc'
    };
    var chart3_element = 'google-chart-3';
    var chart3_type = google.visualization.ScatterChart;
    drawGoogleChart(chart3_data, chart3_options, chart3_element, chart3_type);
    //  Chart 4
    var chart4_data = [
    ['ID', 'Life Expectancy', 'Fertility Rate', 'Region', 'Population'],
    ['CAN', 80.66, 1.67, 'North America', 33739900],
    ['DEU', 79.84, 1.36, 'Europe', 81902307],
    ['DNK', 78.6, 1.84, 'Europe', 5523095],
    ['EGY', 72.73, 2.78, 'Middle East', 79716203],
    ['GBR', 80.05, 2, 'Europe', 61801570],
    ['IRN', 72.49, 1.7, 'Middle East', 73137148],
    ['IRQ', 68.09, 4.77, 'Middle East', 31090763],
    ['ISR', 81.55, 2.96, 'Middle East', 7485600],
    ['RUS', 68.6, 1.54, 'Europe', 141850000],
    ['USA', 78.09, 2.05, 'North America', 307007000]
    ];
    var chart4_options = {
        title: 'Correlation between life expectancy, fertility rate and population of some world countries (2010)',
        hAxis: {title: 'Life Expectancy'},
        vAxis: {title: 'Fertility Rate'},
        backgroundColor: '#fcfcfc',
        bubble: {textStyle: {fontSize: 11}}
    };
    var chart4_element = 'google-chart-4';
    var chart4_type = google.visualization.BubbleChart;
    drawGoogleChart(chart4_data, chart4_options, chart4_element, chart4_type);
    //  Chart 5
    var chart5_data = [
    ['Country', 'Popularity'],
    ['Germany', 200],
    ['United States', 300],
    ['Brazil', 400],
    ['Canada', 500],
    ['France', 600],
    ['RU', 700]
    ];
    var chart5_options = {
        backgroundColor: '#fcfcfc',
        enableRegionInteractivity: true
    };
    var chart5_element = 'google-chart-5';
    var chart5_type = google.visualization.GeoChart;
    drawGoogleChart(chart5_data, chart5_options, chart5_element, chart5_type);
    //  Chart 6
    var chart6_data = [
    ['Year', 'Sales', 'Expenses'],
    ['2004', 1000, 400],
    ['2005', 1170, 460],
    ['2006', 660, 1120],
    ['2007', 1030, 540],
    ['2008', 2080, 740],
    ['2009', 1949, 690],
    ['2010', 2334, 820]
    ];
    var chart6_options = {
        backgroundColor: '#fcfcfc',
        title: 'Company Performance'
    };
    var chart6_element = 'google-chart-6';
    var chart6_type = google.visualization.LineChart;
    drawGoogleChart(chart6_data, chart6_options, chart6_element, chart6_type);
    //  Chart 7
    var chart7_data = [
    ['Task', 'Hours per Day'],
    ['Work', 11],
    ['Eat', 2],
    ['Commute', 2],
    ['Watch TV', 2],
    ['Sleep', 7]
    ];
    var chart7_options = {
        backgroundColor: '#fcfcfc',
        title: 'My Daily Activities',
        pieHole: 0.4
    };
    var chart7_element = 'google-chart-7';
    var chart7_type = google.visualization.PieChart;
    drawGoogleChart(chart7_data, chart7_options, chart7_element, chart7_type);
    //  Chart 8
    var chart8_data = [
    ['Generation', 'Descendants'],
    [0, 1], [1, 33], [2, 269], [3, 2013]
    ];
    var chart8_options = {
        backgroundColor: '#fcfcfc',
        title: 'Descendants by Generation',
        hAxis: {title: 'Generation', minValue: 0, maxValue: 3},
        vAxis: {title: 'Descendants', minValue: 0, maxValue: 2100},
        trendlines: {
            0: {
                type: 'exponential',
                visibleInLegend: true
            }
        }
    };
    var chart8_element = 'google-chart-8';
    var chart8_type = google.visualization.ScatterChart;
    drawGoogleChart(chart8_data, chart8_options, chart8_element, chart8_type);
}
/*-------------------------------------------
 Demo graphs for xCharts page (charts_xcharts.html)
 ---------------------------------------------*/
//
// Graph1 created in element with id = xchart-1
//
function xGraph1() {
    var tt = document.createElement('div'),
    leftOffset = -(~~$('html').css('padding-left').replace('px', '') + ~~$('body').css('margin-left').replace('px', '')),
    topOffset = -32;
    tt.className = 'ex-tooltip';
    document.body.appendChild(tt);
    var data = {
        "xScale": "time",
        "yScale": "linear",
        "main": [
        {
            "className": ".xchart-class-1",
            "data": [
            {
                "x": "2012-11-05",
                "y": 6
            },
            {
                "x": "2012-11-06",
                "y": 6
            },
            {
                "x": "2012-11-07",
                "y": 8
            },
            {
                "x": "2012-11-08",
                "y": 3
            },
            {
                "x": "2012-11-09",
                "y": 4
            },
            {
                "x": "2012-11-10",
                "y": 9
            },
            {
                "x": "2012-11-11",
                "y": 6
            },
            {
                "x": "2012-11-12",
                "y": 16
            },
            {
                "x": "2012-11-13",
                "y": 4
            },
            {
                "x": "2012-11-14",
                "y": 9
            },
            {
                "x": "2012-11-15",
                "y": 2
            }
            ]
        }
        ]
    };
    var opts = {
        "dataFormatX": function(x) {
            return d3.time.format('%Y-%m-%d').parse(x);
        },
        "tickFormatX": function(x) {
            return d3.time.format('%A')(x);
        },
        "mouseover": function(d, i) {
            var pos = $(this).offset();
            $(tt).text(d3.time.format('%A')(d.x) + ': ' + d.y)
            .css({top: topOffset + pos.top, left: pos.left + leftOffset})
            .show();
        },
        "mouseout": function(x) {
            $(tt).hide();
        }
    };
    var myChart = new xChart('line-dotted', data, '#xchart-1', opts);
}
//
// Graph2 created in element with id = xchart-2
//
function xGraph2() {
    var data = {
        "xScale": "ordinal",
        "yScale": "linear",
        "main": [
        {
            "className": ".xchart-class-2",
            "data": [
            {
                "x": "Apple",
                "y": 575
            },
            {
                "x": "Facebook",
                "y": 163
            },
            {
                "x": "Microsoft",
                "y": 303
            },
            {
                "x": "Cisco",
                "y": 121
            },
            {
                "x": "Google",
                "y": 393
            }
            ]
        }
        ]
    };
    var myChart = new xChart('bar', data, '#xchart-2');
}
//
// Graph3 created in element with id = xchart-3
//
function xGraph3() {
    var data = {
        "xScale": "time",
        "yScale": "linear",
        "type": "line",
        "main": [
        {
            "className": ".xchart-class-3",
            "data": [
            {
                "x": "2012-11-05",
                "y": 1
            },
            {
                "x": "2012-11-06",
                "y": 6
            },
            {
                "x": "2012-11-07",
                "y": 13
            },
            {
                "x": "2012-11-08",
                "y": -3
            },
            {
                "x": "2012-11-09",
                "y": -4
            },
            {
                "x": "2012-11-10",
                "y": 9
            },
            {
                "x": "2012-11-11",
                "y": 6
            },
            {
                "x": "2012-11-12",
                "y": 7
            },
            {
                "x": "2012-11-13",
                "y": -2
            },
            {
                "x": "2012-11-14",
                "y": -7
            }
            ]
        }
        ]
    };
    var opts = {
        "dataFormatX": function(x) {
            return d3.time.format('%Y-%m-%d').parse(x);
        },
        "tickFormatX": function(x) {
            return d3.time.format('%A')(x);
        }
    };
    var myChart = new xChart('line', data, '#xchart-3', opts);
}
/*-------------------------------------------
 Demo graphs for CoinDesk page (charts_coindesk.html)
 ---------------------------------------------*/
//
// Main function for CoinDesk API Page
// (we get JSON data and make 4 graph from this)
//
function CoinDeskGraph() {
    var dates = PrettyDates();
    var startdate = dates[0];
    var enddate = dates[1];
    // Load JSON data from CoinDesk API
    var jsonURL = 'http://api.coindesk.com/v1/bpi/historical/close.json?start=' + startdate + '&end=' + enddate;
    $.getJSON(jsonURL, function(result) {
        // Create array of data for xChart
        $.each(result.bpi, function(key, val) {
            xchart_data.push({'x': key, 'y': val});
        });
        // Set handler for resize and create xChart plot
        var graphXChartResize;
        $('#coindesk-xchart').resize(function() {
            clearTimeout(graphXChartResize);
            graphXChartResize = setTimeout(DrawCoinDeskXCharts, 500);
        });
        DrawCoinDeskXCharts();
        // Create array of data for Google Chart
        $.each(result.bpi, function(key, val) {
            google_data.push([key, val]);
        });
        // Set handler for resize and create Google Chart plot
        var graphGChartResize;
        $('#coindesk-google-chart').resize(function() {
            clearTimeout(graphGChartResize);
            graphGChartResize = setTimeout(DrawCoinDeskGoogleCharts, 500);
        });
        DrawCoinDeskGoogleCharts();
        // Create array of data for Flot and Sparkline
        $.each(result.bpi, function(key, val) {
            var parseDate = key;
            parseDate = parseDate.split("-");
            var newDate = parseDate[1] + "/" + parseDate[2] + "/" + parseDate[0];
            var new_date = new Date(newDate).getTime();
            exchange_rate.push([new_date, val]);
        });
        // Create Flot plot (not need bind to resize, cause Flot use plugin 'resize')
        DrawCoinDeskFlot();
        // Set handler for resize and create Sparkline plot
        var graphSparklineResize;
        $('#coindesk-sparklines').resize(function() {
            clearTimeout(graphSparklineResize);
            graphSparklineResize = setTimeout(DrawCoinDeskSparkLine, 500);
        });
        DrawCoinDeskSparkLine();
    });
}
//
// Draw Sparkline Graph on Coindesk page
//
function DrawCoinDeskSparkLine() {
    $('#coindesk-sparklines').sparkline(exchange_rate, {height: '100%', width: '100%'});
}
//
// Draw xChart Graph on Coindesk page
//
function DrawCoinDeskXCharts() {
    var data = {
        "xScale": "ordinal",
        "yScale": "linear",
        "main": [
        {
            "className": ".pizza",
            "data": xchart_data
        }
        ]
    };
    var myChart = new xChart('line-dotted', data, '#coindesk-xchart');
}
//
// Draw Flot Graph on Coindesk page
//
function DrawCoinDeskFlot() {
    var data1 = [
    {data: exchange_rate, label: "Bitcoin exchange rate ($)"}
    ];
    var options = {
        canvas: true,
        xaxes: [
        {mode: "time"}
        ],
        yaxes: [
        {min: 0},
        {
            position: "right",
            alignTicksWithAxis: 1,
            tickFormatter: function(value, axis) {
                return value.toFixed(axis.tickDecimals) + "â‚¬";
            }
        }
        ],
        legend: {position: "sw"}
    };
    $.plot("#coindesk-flot", data1, options);
}
//
// Draw Google Chart Graph on Coindesk page
//
function DrawCoinDeskGoogleCharts() {
    var google_options = {
        backgroundColor: '#fcfcfc',
        title: 'Coindesk Exchange Rate'
    };
    var google_element = 'coindesk-google-chart';
    var google_type = google.visualization.LineChart;
    drawGoogleChart(google_data, google_options, google_element, google_type);
}
/*-------------------------------------------
 Scripts for DataTables page (tables_datatables.html)
 ---------------------------------------------*/
//
// Function for table, located in element with id = datatable-1
//
function TestTable1() {
    $('#datatable-1').dataTable({
        "aaSorting": [[0, "asc"]],
        "sDom": "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sSearch": "",
            "sLengthMenu": '_MENU_'
        }
    });
}
//
// Function for table, located in element with id = datatable-2
//
function TestTable2() {
    var asInitVals = [];
    var oTable = $('#datatable-2').dataTable({
        "aaSorting": [[0, "asc"]],
        "sDom": "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sSearch": "",
            "sLengthMenu": '_MENU_'
        },
        bAutoWidth: false
    });
    var header_inputs = $("#datatable-2 thead input");
    header_inputs.on('keyup', function() {
        /* Filter on the column (the index) of this element */
        oTable.fnFilter(this.value, header_inputs.index(this));
    })
    .on('focus', function() {
        if (this.className == "search_init") {
            this.className = "";
            this.value = "";
        }
    })
    .on('blur', function(i) {
        if (this.value == "") {
            this.className = "search_init";
            this.value = asInitVals[header_inputs.index(this)];
        }
    });
    header_inputs.each(function(i) {
        asInitVals[i] = this.value;
    });
}
//
// Function for table, located in element with id = datatable-3
//
function TestTable3() {
    $('#datatable-3').dataTable({
        "aaSorting": [[0, "asc"]],
        "sDom": "T<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sSearch": "",
            "sLengthMenu": '_MENU_'
        },
        "oTableTools": {
            "sSwfPath": "plugins/datatables/copy_csv_xls_pdf.swf",
            "aButtons": [
            "copy",
            "print",
            {
                "sExtends": "collection",
                "sButtonText": 'Save <span class="caret" />',
                "aButtons": ["csv", "xls", "pdf"]
            }
            ]
        }
    });
}
/*-------------------------------------------
 Functions for Dashboard page (dashboard.html)
 ---------------------------------------------*/
//
// Helper for random change data (only test data for Sparkline plots)
//
function SmallChangeVal(val) {
    var new_val = Math.floor(100 * Math.random());
    var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
    var result = val[0] + new_val * plusOrMinus;
    if (parseInt(result) > 1000) {
        return [val[0] - new_val]
    }
    if (parseInt(result) < 0) {
        return [val[0] + new_val]
    }
    return [result];
}
//
// Make array of random data
//
function SparklineTestData() {
    var arr = [];
    for (var i = 1; i < 9; i++) {
        arr.push([Math.floor(1000 * Math.random())])
    }
    return arr;
}
//
// Redraw Knob charts on Dashboard (panel- servers)
//
function RedrawKnob(elem) {
    elem.animate({
        value: Math.floor(100 * Math.random())
    }, {
        duration: 3000,
        easing: 'swing',
        progress: function()
        {
            $(this).val(parseInt(Math.ceil(elem.val()))).trigger('change');
        }
    });
}
//
// Draw 3 Sparkline plot in Dashboard header
//
function SparklineLoop() {
    SparkLineDrawBarGraph($('#sparkline-1'), sparkline_arr_1.map(SmallChangeVal));
    SparkLineDrawBarGraph($('#sparkline-2'), sparkline_arr_2.map(SmallChangeVal), '#7BC5D3');
    SparkLineDrawBarGraph($('#sparkline-3'), sparkline_arr_3.map(SmallChangeVal), '#B25050');
}
//
// Draw Morris charts on Dashboard (panel- Statistics + 3 donut)
//
function MorrisDashboard() {
    Morris.Line({
        element: 'stat-graph',
        data: [
        {"period": "2014-01", "Win8": 13.4, "Win7": 55.3, 'Vista': 1.5, 'NT': 0.3, 'XP': 11, 'Linux': 4.9, 'Mac': 9.6, 'Mobile': 4},
        {"period": "2013-12", "Win8": 10, "Win7": 55.9, 'Vista': 1.5, 'NT': 3.1, 'XP': 11.6, 'Linux': 4.8, 'Mac': 9.2, 'Mobile': 3.8},
        {"period": "2013-11", "Win8": 8.6, "Win7": 56.4, 'Vista': 1.6, 'NT': 3.7, 'XP': 11.7, 'Linux': 4.8, 'Mac': 9.6, 'Mobile': 3.7},
        {"period": "2013-10", "Win8": 9.9, "Win7": 56.7, 'Vista': 1.6, 'NT': 1.4, 'XP': 12.4, 'Linux': 4.9, 'Mac': 9.6, 'Mobile': 3.3},
        {"period": "2013-09", "Win8": 10.2, "Win7": 56.8, 'Vista': 1.6, 'NT': 0.4, 'XP': 13.5, 'Linux': 4.8, 'Mac': 9.3, 'Mobile': 3.3},
        {"period": "2013-08", "Win8": 9.6, "Win7": 55.9, 'Vista': 1.7, 'NT': 0.4, 'XP': 14.7, 'Linux': 5, 'Mac': 9.2, 'Mobile': 3.4},
        {"period": "2013-07", "Win8": 9, "Win7": 56.2, 'Vista': 1.8, 'NT': 0.4, 'XP': 15.8, 'Linux': 4.9, 'Mac': 8.7, 'Mobile': 3.2},
        {"period": "2013-06", "Win8": 8.6, "Win7": 56.3, 'Vista': 2, 'NT': 0.4, 'XP': 15.4, 'Linux': 4.9, 'Mac': 9.1, 'Mobile': 3.2},
        {"period": "2013-05", "Win8": 7.9, "Win7": 56.4, 'Vista': 2.1, 'NT': 0.4, 'XP': 15.7, 'Linux': 4.9, 'Mac': 9.7, 'Mobile': 2.6},
        {"period": "2013-04", "Win8": 7.3, "Win7": 56.4, 'Vista': 2.2, 'NT': 0.4, 'XP': 16.4, 'Linux': 4.8, 'Mac': 9.7, 'Mobile': 2.2},
        {"period": "2013-03", "Win8": 6.7, "Win7": 55.9, 'Vista': 2.4, 'NT': 0.4, 'XP': 17.6, 'Linux': 4.7, 'Mac': 9.5, 'Mobile': 2.3},
        {"period": "2013-02", "Win8": 5.7, "Win7": 55.3, 'Vista': 2.4, 'NT': 0.4, 'XP': 19.1, 'Linux': 4.8, 'Mac': 9.6, 'Mobile': 2.2},
        {"period": "2013-01", "Win8": 4.8, "Win7": 55.3, 'Vista': 2.6, 'NT': 0.5, 'XP': 19.9, 'Linux': 4.8, 'Mac': 9.3, 'Mobile': 2.2}
        ],
        xkey: 'period',
        ykeys: ['Win8', 'Win7', 'Vista', 'NT', 'XP', 'Linux', 'Mac', 'Mobile'],
        labels: ['Win8', 'Win7', 'Vista', 'NT', 'XP', 'Linux', 'Mac', 'Mobile']
    });
Morris.Donut({
    element: 'morris_donut_1',
    data: [
    {value: 70, label: 'pay', formatted: 'at least 70%'},
    {value: 15, label: 'client', formatted: 'approx. 15%'},
    {value: 10, label: 'buy', formatted: 'approx. 10%'},
    {value: 5, label: 'hosted', formatted: 'at most 5%'}
    ],
    formatter: function(x, data) {
        return data.formatted;
    }
});
Morris.Donut({
    element: 'morris_donut_2',
    data: [
    {value: 20, label: 'office', formatted: 'current'},
    {value: 35, label: 'store', formatted: 'approx. 35%'},
    {value: 20, label: 'shop', formatted: 'approx. 20%'},
    {value: 25, label: 'cars', formatted: 'at most 25%'}
    ],
    formatter: function(x, data) {
        return data.formatted;
    }
});
Morris.Donut({
    element: 'morris_donut_3',
    data: [
    {value: 17, label: 'current', formatted: 'current'},
    {value: 22, label: 'week', formatted: 'last week'},
    {value: 10, label: 'month', formatted: 'last month'},
    {value: 25, label: 'period', formatted: 'period'},
    {value: 25, label: 'year', formatted: 'this year'}
    ],
    formatter: function(x, data) {
        return data.formatted;
    }
});
}
//
// Draw SparkLine example Charts for Dashboard (table- Tickers)
//
function DrawSparklineDashboard() {
    SparklineLoop();
    setInterval(SparklineLoop, 1000);
    var sparkline_clients = [[309], [223], [343], [652], [455], [18], [912], [15]];
    $('.bar').each(function() {
        $(this).sparkline(sparkline_clients.map(SmallChangeVal), {type: 'bar', barWidth: 5, highlightColor: '#000', barSpacing: 2, height: 30, stackedBarColor: '#6AA6D6'});
    });
    var sparkline_table = [[1, 341], [2, 464], [4, 564], [5, 235], [6, 335], [7, 535], [8, 642], [9, 342], [10, 765]];
    $('.td-graph').each(function() {
        var arr = $.map(sparkline_table, function(val, index) {
            return [[val[0], SmallChangeVal([val[1]])]];
        });
        $(this).sparkline(arr,
            {defaultPixelsPerValue: 10, minSpotColor: null, maxSpotColor: null, spotColor: null,
                fillColor: false, lineWidth: 2, lineColor: '#5A8DB6'});
    });
}
//
// Draw Knob Charts for Dashboard (for servers)
//
function DrawKnobDashboard() {
    var srv_monitoring_selectors = [
    $("#knob-srv-1"), $("#knob-srv-2"), $("#knob-srv-3"),
    $("#knob-srv-4"), $("#knob-srv-5"), $("#knob-srv-6")
    ];
    srv_monitoring_selectors.forEach(DrawKnob);
    setInterval(function() {
        srv_monitoring_selectors.forEach(RedrawKnob);
    }, 3000);
}
/*-------------------------------------------
 Function for File upload page (form_file_uploader.html)
 ---------------------------------------------*/
 function FileUpload() {
    $('#bootstrapped-fine-uploader').fineUploader({
        template: 'qq-template-bootstrap',
        classes: {
            success: 'alert alert-success',
            fail: 'alert alert-error'
        },
        thumbnails: {
            placeholders: {
                waitingPath: "assets/waiting-generic.png",
                notAvailablePath: "assets/not_available-generic.png"
            }
        },
        request: {
            endpoint: 'server/handleUploads'
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
        }
    });
}
/*-------------------------------------------
 Function for OpenStreetMap page (maps.html)
 ---------------------------------------------*/
//
// Load GeoIP JSON data and draw 3 maps
//
function LoadTestMap() {
    $.getJSON("http://www.telize.com/geoip?callback=?",
        function(json) {
                var osmap = new OpenLayers.Layer.OSM("OpenStreetMap");//ÑÐ¾Ð·Ð´Ð°Ð½Ð¸Ðµ ÑÐ»Ð¾Ñ ÐºÐ°Ñ€Ñ‚Ñ‹
                var googlestreets = new OpenLayers.Layer.Google("Google Streets", {numZoomLevels: 22, visibility: false});
                var googlesattelite = new OpenLayers.Layer.Google("Google Sattelite", {type: google.maps.MapTypeId.SATELLITE, numZoomLevels: 22});
                var map1_layers = [googlestreets, osmap, googlesattelite];
                // Create map in element with ID - map-1
                var map1 = drawMap(json.longitude, json.latitude, "map-1", map1_layers);
                $("#map-1").resize(function() {
                    setTimeout(map1.updateSize(), 500);
                });
                // Create map in element with ID - map-2
                var osmap1 = new OpenLayers.Layer.OSM("OpenStreetMap");//ÑÐ¾Ð·Ð´Ð°Ð½Ð¸Ðµ ÑÐ»Ð¾Ñ ÐºÐ°Ñ€Ñ‚Ñ‹
                var map2_layers = [osmap1];
                var map2 = drawMap(json.longitude, json.latitude, "map-2", map2_layers);
                $("#map-2").resize(function() {
                    setTimeout(map2.updateSize(), 500);
                });
                // Create map in element with ID - map-3
                var sattelite = new OpenLayers.Layer.Google("Google Sattelite", {type: google.maps.MapTypeId.SATELLITE, numZoomLevels: 22});
                var map3_layers = [sattelite];
                var map3 = drawMap(json.longitude, json.latitude, "map-3", map3_layers);
                $("#map-3").resize(function() {
                    setTimeout(map3.updateSize(), 500);
                });
            }
            );
}
/*-------------------------------------------
 Function for Fullscreen Map page (map_fullscreen.html)
 ---------------------------------------------*/
//
// Create Fullscreen Map
//
function FullScreenMap() {
    $.getJSON("http://www.telize.com/geoip?callback=?",
        function(json) {
                var osmap = new OpenLayers.Layer.OSM("OpenStreetMap");//ÑÐ¾Ð·Ð´Ð°Ð½Ð¸Ðµ ÑÐ»Ð¾Ñ ÐºÐ°Ñ€Ñ‚Ñ‹
                var googlestreets = new OpenLayers.Layer.Google("Google Streets", {numZoomLevels: 22, visibility: false});
                var googlesattelite = new OpenLayers.Layer.Google("Google Sattelite", {type: google.maps.MapTypeId.SATELLITE, numZoomLevels: 22});
                var map1_layers = [googlestreets, osmap, googlesattelite];
                var map_fs = drawMap(json.longitude, json.latitude, "full-map", map1_layers);
            }
            );
}
/*-------------------------------------------
 Function for Flickr Gallery page (gallery_flickr.html)
 ---------------------------------------------*/
//
// Load data from Flicks, parse and create gallery
//
function displayFlickrImages(data) {
    var res;
    $.each(data.items, function(i, item) {
        if (i > 11) {
            return false;
        }
        res = "<a href=" + item.link + " title=" + item.title + " target=\"_blank\"><img alt=" + item.title + " src=" + item.media.m + " /></a>";
        $('#box-one-content').append(res);
    });
    setTimeout(function() {
        $("#box-one-content").justifiedGallery({
            'usedSuffix': 'lt240',
            'justifyLastRow': true,
            'rowHeight': 150,
            'fixedHeight': false,
            'captions': true,
            'margins': 1
        });
        $('#box-one-content').fadeIn('slow');
    }, 100);
}
/*-------------------------------------------
 Function for Form Layout page (form layouts.html)
 ---------------------------------------------*/
//
// Example form validator function
//
function DemoFormValidator() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The username must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'The username can only consist of alphabetical, number, dot and underscore'
                    }
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: 'The country is required and can\'t be empty'
                    }
                }
            },
            acceptTerms: {
                validators: {
                    notEmpty: {
                        message: 'You have to accept the terms and policies'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            website: {
                validators: {
                    uri: {
                        message: 'The input is not a valid URL'
                    }
                }
            },
            phoneNumber: {
                validators: {
                    digits: {
                        message: 'The value can contain only digits'
                    }
                }
            },
            color: {
                validators: {
                    hexColor: {
                        message: 'The input is not a valid hex color'
                    }
                }
            },
            zipCode: {
                validators: {
                    usZipCode: {
                        message: 'The input is not a valid US zip code'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'confirmPassword',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            confirmPassword: {
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            ages: {
                validators: {
                    lessThan: {
                        value: 100,
                        inclusive: true,
                        message: 'The ages has to be less than 100'
                    },
                    greaterThan: {
                        value: 10,
                        inclusive: false,
                        message: 'The ages has to be greater than or equals to 10'
                    }
                }
            }
        }
    });
}
//
// Function for Dynamically Change input size on Form Layout page
//
function FormLayoutExampleInputLength(selector) {
    var steps = [
    "col-sm-1",
    "col-sm-2",
    "col-sm-3",
    "col-sm-4",
    "col-sm-5",
    "col-sm-6",
    "col-sm-7",
    "col-sm-8",
    "col-sm-9",
    "col-sm-10",
    "col-sm-11",
    "col-sm-12"
    ];
    selector.slider({
        range: 'min',
        value: 1,
        min: 0,
        max: 11,
        step: 1,
        slide: function(event, ui) {
            if (ui.value < 1) {
                return false;
            }
            var input = $("#form-styles");
            var f = input.parent();
            f.removeClass();
            f.addClass(steps[ui.value]);
            input.attr("placeholder", '.' + steps[ui.value]);
        }
    });
}
/*-------------------------------------------
 Functions for Progressbar page (ui_progressbars.html)
 ---------------------------------------------*/
//
// Function for Knob clock
//
function RunClock() {
    var second = $(".second");
    var minute = $(".minute");
    var hour = $(".hour");
    var d = new Date();
    var s = d.getSeconds();
    var m = d.getMinutes();
    var h = d.getHours();
    if (h > 11) {
        h = h - 12;
    }
    $('#knob-clock-value').html(h + ':' + m + ':' + s);
    second.val(s).trigger("change");
    minute.val(m).trigger("change");
    hour.val(h).trigger("change");
}
//
// Function for create test sliders on Progressbar page
//
function CreateAllSliders() {
    $(".slider-default").slider();
    var slider_range_min_amount = $(".slider-range-min-amount");
    var slider_range_min = $(".slider-range-min");
    var slider_range_max = $(".slider-range-max");
    var slider_range_max_amount = $(".slider-range-max-amount");
    var slider_range = $(".slider-range");
    var slider_range_amount = $(".slider-range-amount");
    slider_range_min.slider({
        range: "min",
        value: 37,
        min: 1,
        max: 700,
        slide: function(event, ui) {
            slider_range_min_amount.val("$" + ui.value);
        }
    });
    slider_range_min_amount.val("$" + slider_range_min.slider("value"));
    slider_range_max.slider({
        range: "max",
        min: 1,
        max: 100,
        value: 2,
        slide: function(event, ui) {
            slider_range_max_amount.val(ui.value);
        }
    });
    slider_range_max_amount.val(slider_range_max.slider("value"));
    slider_range.slider({
        range: true,
        min: 0,
        max: 500,
        values: [75, 300],
        slide: function(event, ui) {
            slider_range_amount.val("$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ]);
        }
    });
    slider_range_amount.val("$" + slider_range.slider("values", 0) +
        " - $" + slider_range.slider("values", 1));
    $("#equalizer > div.progress > div").each(function() {
        // read initial values from markup and remove that
        var value = parseInt($(this).text(), 10);
        $(this).empty().slider({
            value: value,
            range: "min",
            animate: true,
            orientation: "vertical"
        });
    });
}
/*-------------------------------------------
 Function for jQuery-UI page (ui_jquery-ui.html)
 ---------------------------------------------*/
//
// Function for make all Date-Time pickers on page
//
function AllTimePickers() {
    $('#datetime_example').datetimepicker({});
    $('#time_example').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: 'hh:mm tt'
    });
    $('#date3_example').datepicker({numberOfMonths: 3, showButtonPanel: true});
    $('#date3-1_example').datepicker({numberOfMonths: 3, showButtonPanel: true});
    $('#date_example').datepicker({});
}
/*-------------------------------------------
 Function for Calendar page (calendar.html)
 ---------------------------------------------*/
//
// Example form validator function
//
function DrawCalendar() {
    /* initialize the external events
    -----------------------------------------------------------------*/
    $('#external-events div.external-event').each(function() {
        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };
        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);
        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });
    });
    /* initialize the calendar
    -----------------------------------------------------------------*/
    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
            var form = $('<form id="event_form">' +
                '<div class="form-group has-success has-feedback">' +
                '<label">Event name</label>' +
                '<div>' +
                '<input type="text" id="newevent_name" class="form-control" placeholder="Name of event">' +
                '</div>' +
                '<label>Description</label>' +
                '<div>' +
                '<textarea rows="3" id="newevent_desc" class="form-control" placeholder="Description"></textarea>' +
                '</div>' +
                '</div>' +
                '</form>');
            var buttons = $('<button id="event_cancel" type="cancel" class="btn btn-default btn-label-left">' +
                '<span><i class="fa fa-clock-o txt-danger"></i></span>' +
                'Cancel' +
                '</button>' +
                '<button type="submit" id="event_submit" class="btn btn-primary btn-label-left pull-right">' +
                '<span><i class="fa fa-clock-o"></i></span>' +
                'Add' +
                '</button>');
            OpenModalBox('Add event', form, buttons);
            $('#event_cancel').on('click', function() {
                CloseModalBox();
            });
            $('#event_submit').on('click', function() {
                var new_event_name = $('#newevent_name').val();
                if (new_event_name != '') {
                    calendar.fullCalendar('renderEvent',
                    {
                        title: new_event_name,
                        description: $('#newevent_desc').val(),
                        start: start,
                        end: end,
                        allDay: allDay
                    },
                    true // make the event "stick"
                    );
                }
                CloseModalBox();
            });
            calendar.fullCalendar('unselect');
        },
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function(date, allDay) { // this function is called when something is dropped
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');
            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        eventRender: function(event, element, icon) {
            if (event.description != "") {
                element.attr('title', event.description);
            }
        },
        eventClick: function(calEvent, jsEvent, view) {
            var form = $('<form id="event_form">' +
                '<div class="form-group has-success has-feedback">' +
                '<label">Event name</label>' +
                '<div>' +
                '<input type="text" id="newevent_name" value="' + calEvent.title + '" class="form-control" placeholder="Name of event">' +
                '</div>' +
                '<label>Description</label>' +
                '<div>' +
                '<textarea rows="3" id="newevent_desc" class="form-control" placeholder="Description">' + calEvent.description + '</textarea>' +
                '</div>' +
                '</div>' +
                '</form>');
            var buttons = $('<button id="event_cancel" type="cancel" class="btn btn-default btn-label-left">' +
                '<span><i class="fa fa-clock-o txt-danger"></i></span>' +
                'Cancel' +
                '</button>' +
                '<button id="event_delete" type="cancel" class="btn btn-danger btn-label-left">' +
                '<span><i class="fa fa-clock-o txt-danger"></i></span>' +
                'Delete' +
                '</button>' +
                '<button type="submit" id="event_change" class="btn btn-primary btn-label-left pull-right">' +
                '<span><i class="fa fa-clock-o"></i></span>' +
                'Save changes' +
                '</button>');
            OpenModalBox('Change event', form, buttons);
            $('#event_cancel').on('click', function() {
                CloseModalBox();
            });
            $('#event_delete').on('click', function() {
                calendar.fullCalendar('removeEvents', function(ev) {
                    return (ev._id == calEvent._id);
                });
                CloseModalBox();
            });
            $('#event_change').on('click', function() {
                calEvent.title = $('#newevent_name').val();
                calEvent.description = $('#newevent_desc').val();
                calendar.fullCalendar('updateEvent', calEvent);
                CloseModalBox()
            });
        }
    });
$('#new-event-add').on('click', function(event) {
    event.preventDefault();
    var event_name = $('#new-event-title').val();
    var event_description = $('#new-event-desc').val();
    if (event_name != '') {
        var event_template = $('<div class="external-event" data-description="' + event_description + '">' + event_name + '</div>');
        $('#events-templates-header').after(event_template);
        var eventObject = {
            title: event_name,
            description: event_description
        };
            // store the Event Object in the DOM element so we can get to it later
            event_template.data('eventObject', eventObject);
            event_template.draggable({
                zIndex: 999,
                revert: true,
                revertDuration: 0
            });
        }
    });
}
//
// Load scripts and draw Calendar
//
function DrawFullCalendar() {
    LoadCalendarScript(DrawCalendar);
}
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//
//      MAIN DOCUMENT READY SCRIPT OF DEVOOPS THEME
//
//      In this script main logic of theme
//
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
$(document).ready(function() {
    $('.show-sidebar').on('click', function(e) {
        e.preventDefault();
        $('div#main').toggleClass('sidebar-show');
        setTimeout(MessagesMenuWidth, 250);
    });
    var ajax_url = location.href;
//    var ajax_url = location.hash.replace(/^#/, '');
//    if (ajax_url.length < 1) {
//        ajax_url = 'ajax/dashboard.html';
//    }
LoadAjaxContent(ajax_url);
$('.main-menu').on('click', 'a', function(e) {
    var parents = $(this).parents('li');
    var li = $(this).closest('li.dropdown');
    var another_items = $('.main-menu li').not(parents);
    another_items.find('a').removeClass('active');
    another_items.find('a').removeClass('active-parent');
    if ($(this).hasClass('dropdown-toggle') || $(this).closest('li').find('ul').length == 0) {
        $(this).addClass('active-parent');
        var current = $(this).next();
        if (current.is(':visible')) {
            li.find("ul.dropdown-menu").slideUp('fast');
            li.find("ul.dropdown-menu a").removeClass('active')
        }
        else {
            another_items.find("ul.dropdown-menu").slideUp('fast');
            current.slideDown('fast');
        }
    }
    else {
        if (li.find('a.dropdown-toggle').hasClass('active-parent')) {
            var pre = $(this).closest('ul.dropdown-menu');
            pre.find("li.dropdown").not($(this).closest('li')).find('ul.dropdown-menu').slideUp('fast');
        }
    }
    if ($(this).hasClass('active') == false) {
        $(this).parents("ul.dropdown-menu").find('a').removeClass('active');
        $(this).addClass('active')
    }
    if ($(this).hasClass('ajax-link')) {
        e.preventDefault();
        if ($(this).hasClass('add-full')) {
            $('#content').addClass('full-content');
        }
        else {
            $('#content').removeClass('full-content');
        }
        var url = $(this).attr('href');
        window.location.hash = url;
        LoadAjaxContent(url);
    }
    if ($(this).attr('href') == '#') {
        e.preventDefault();
    }
});
var height = window.innerHeight - 49;
$('#main').css('min-height', height)
.on('click', '.expand-link', function(e) {
    var body = $('body');
    e.preventDefault();
    var box = $(this).closest('div.box');
    var button = $(this).find('i');
    button.toggleClass('fa-expand').toggleClass('fa-compress');
    box.toggleClass('expanded');
    body.toggleClass('body-expanded');
    var timeout = 0;
    if (body.hasClass('body-expanded')) {
        timeout = 100;
    }
    setTimeout(function() {
        box.toggleClass('expanded-padding');
    }, timeout);
    setTimeout(function() {
        box.resize();
        box.find('[id^=map-]').resize();
    }, timeout + 50);
})
.on('click', '.collapse-link', function(e) {
    e.preventDefault();
    var box = $(this).closest('div.box');
    var button = $(this).find('i');
    var content = box.find('div.box-content');
    content.slideToggle('fast');
    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    setTimeout(function() {
        box.resize();
        box.find('[id^=map-]').resize();
    }, 50);
})
.on('click', '.close-link', function(e) {
    e.preventDefault();
    var content = $(this).closest('div.box');
    content.remove();
});
$('#locked-screen').on('click', function(e) {
    e.preventDefault();
    $('body').addClass('body-screensaver');
    $('#screensaver').addClass("show");
    ScreenSaver();
});
$('body').on('click', 'a.close-link', function(e) {
    e.preventDefault();
    CloseModalBox();
});
$('#top-panel').on('click', 'a', function(e) {
    if ($(this).hasClass('ajax-link')) {
        e.preventDefault();
        if ($(this).hasClass('add-full')) {
            $('#content').addClass('full-content');
        }
        else {
            $('#content').removeClass('full-content');
        }
        var url = $(this).attr('href');
        window.location.hash = url;
        LoadAjaxContent(url);
    }
});
$('#search').on('keydown', function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        $('#content').removeClass('full-content');
        ajax_url = 'ajax/page_search.html';
        window.location.hash = ajax_url;
        LoadAjaxContent(ajax_url);
    }
});
$('#screen_unlock').on('mouseover', function() {
    var header = 'Enter current username and password';
    var form = $('<div class="form-group"><label class="control-label">Username</label><input type="text" class="form-control" name="username" /></div>' +
        '<div class="form-group"><label class="control-label">Password</label><input type="password" class="form-control" name="password" /></div>');
    var button = $('<div class="text-center"><a href="index.html" class="btn btn-primary">Unlock</a></div>');
    OpenModalBox(header, form, button);
});
});
}
}})();

(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
	angular.module('integragenerico')
	.directive('formularioEditarElemento', formularioEditarElemento);

	function formularioEditarElemento(EditarElementosTemplate) {
		return {
			restrict: 'E',
			templateUrl: EditarElementosTemplate.getTemplate(),
			scope: {
				formulario: '='
			}
		};
	}

})();

(function () {
  'use strict';

  angular
    .module('integragenerico').service('ServicioEdicion', ServicioEdicion);

  function ServicioEdicion($window, $q, $injector, CamposEditar, ServicioListasRelacionadas, Dialogos, TransformacionString, ServicioValidadoresPersonalizados, ServicioPatrones, ConfigurarConfirmaciones) {

    var vm = this;

    vm.getFormulario = getFormulario;

    function Formulario(clave, id, soloLectura, modalInstance) {
      var promesas = [];
      var formulario = {
        opciones: {},
        soloLectura: soloLectura
      };
      if (modalInstance) {
        formulario.modalInstance = modalInstance;
      }

      formulario.servicio = $injector.get(clave);
      formulario.guardar = guardar;
      formulario.cancelar = cancelar;
      formulario.volver = volver;
      formulario.prefijo = TransformacionString.camelToUnderscore(clave);
      if (!id || id === 'nuevo') {
        formulario.modo = 'nuevo';
        formulario.elemento = {};
        formulario.titulo = formulario.prefijo + '.NUEVO.TITULO';
      } else {
        formulario.modo = 'editar';
        formulario.elemento = formulario.servicio.get({
          codigo: id
        });
        promesas.push(formulario.elemento.$promise);
        if (soloLectura) {
          formulario.titulo = formulario.prefijo + '.CONSULTAR.TITULO';
        } else {
          formulario.titulo = formulario.prefijo + '.EDITAR.TITULO';
        }
      }
      promesas.push(CamposEditar.getCampos(clave).then(function (data) {
        formulario.campos = data;
        ServicioValidadoresPersonalizados.agregarValidadores(formulario.campos);
        //Consultar
        if (soloLectura) {
          formulario.campos.forEach(function (campo) {
            if (!campo.templateOptions) {
              campo.templateOptions = {};
            }
            campo.templateOptions.disabled = true;
          });
        }
        return formulario;
      }).then(ServicioListasRelacionadas.completarListasRelacionadas).then(
        ServicioPatrones.agregarPatrones));

      return $q.all(promesas).then(function () {
        return formulario;
      });
    }

    function guardar(formulario) {
      var prefijo = formulario.prefijo + (formulario.modo === 'nuevo' ? '.DIALOGO_GUARDAR' : '.DIALOGO_ACTUALIZAR');
      if (formulario.form.$valid) {
        if (ConfigurarConfirmaciones.alGuardar) {
          Dialogos.abrirDialogoConfirmacion({
            prefijo: prefijo,
            tieneTitulo: true,
            cancelar: true
          }).result.then(abrirDialogoCargando).then(function () {
            guardarObjeto(formulario);
          });
        } else {
          guardarObjeto(formulario);
        }

      } else {
        Dialogos.abrirDialogoInformacion({
          prefijo: prefijo,
          tieneTitulo: true,
          textoDinamico: 'Complete los campos requeridos'
        });
      }

    }

    function cancelar(formulario) {
      var prefijo = formulario.prefijo + (formulario.modo === 'nuevo' ? '.DIALOGO_CANCELAR_NUEVO' : '.DIALOGO_CANCELAR_EDICION');
      Dialogos.abrirDialogoConfirmacion({
        prefijo: prefijo,
        tieneTitulo: true,
        cancelar: true
      }).result.then(abrirDialogoCargando).then(function() {
        volver(formulario);
      });
    }



    function abrirDialogoCargando() {
      return Dialogos.abrirDialogoTransicionEstados();
    }

    function guardarObjeto(formulario) {
      abrirDialogoCargando();
      if (formulario.modo === 'editar') {
        formulario.servicio.update(formulario.elemento, function () {
          volver(formulario);
        });
      } else if (formulario.modo === 'nuevo') {
        formulario.servicio.save(formulario.elemento, function () {
          volver(formulario);
        });
      }

    }

    function volver(formulario) {
      if (formulario && formulario.modalInstance && formulario.modalInstance.modalInstance) {
        formulario.modalInstance.modalInstance.close();
      } else {
        $window.history.back();
      }

    }

    function getFormulario(clave, id, soloLectura, modalInstance) {
      var form = new Formulario(clave, id, soloLectura, modalInstance);
      return form;
    }

  }

})();

(function() {
    'use strict';

    angular
    .module('integragenerico').service('ServicioImagen', ServicioImagen);

    function ServicioImagen() {

        var vm = this;

        vm.seperarImagen = separarImagen;

        function separarImagen(objeto) {
            if (objeto) {
                var particion = objeto.split(';');
                var resultado = {};
                resultado.imagen = particion[1].split(',')[1];
                resultado.tipo = particion[0].split(':')[1];
                return resultado;   
            }
            return {};
        }

    }

})();
(function () {
    'use strict';

    angular
        .module('integragenerico').service('ServicioListasRelacionadas', ServicioListasRelacionadas);

    function ServicioListasRelacionadas($q, $timeout, $injector, filterFilter) {

        var vm = this;

        vm.completarListasRelacionadas = completarListasRelacionadas;

        function completarListasRelacionadas(formulario) {
            var campos = formulario.campos;
            var promesas = [];
            campos.forEach(function (campo) {
                if (campo.data && campo.data.lista === 'eager') {
                    var query = {};
                    // console.log(campo.data);
                    if (campo.data.sort) {
                        query.sort = campo.data.sort;
                    }
                    if (campo.data.size) {
                        query.page = 0;
                        query.pagination = true;
                        query.size = campo.data.size;
                    }
                    var servicio = $injector.get(campo.data.servicio);
                    var resultados = [];
                    if (query.sort) {
                        resultados = servicio.get(query, function (rta) {
                            var data = rta.content;
                            //Si tiene elementos, seteo el primero x defecto
                            if (formulario.modo === 'nuevo' && !campo.data.isArray && data.length > 0) {
                                formulario.elemento[campo.key] = data[0];
                            }
                            campo.templateOptions.options = data;
                            return data;
                        });
                    } else {
                        resultados = servicio.query(function (data) {
                            //Si tiene elementos, seteo el primero x defecto
                            if (formulario.modo === 'nuevo' && !campo.data.isArray && data.length > 0) {
                                formulario.elemento[campo.key] = data[0];
                            }
                            campo.templateOptions.options = data;
                            return data;
                        });
                    }

                    campo.templateOptions.funcion = function (query) {
                        var filtro = {};
                        filtro[campo.templateOptions.propiedadFiltro] = query;
                        return filterFilter(resultados, query);
                    }
                    promesas.push(resultados.$promise);
                } else if (campo.data && campo.data.lista === 'lazy') {
                    var servicio = $injector.get(campo.data.servicio);
                    campo.templateOptions.funcion = function (filtro) {
                        var query = {};
                        if (Array.isArray(campo.templateOptions.propiedadFiltro)) {
                            campo.templateOptions.propiedadFiltro.forEach(function (filtroIndividual) {
                                query[filtroIndividual] = filtro;
                            });
                        } else {
                            query[campo.templateOptions.propiedadFiltro] = filtro;
                        }
                        return servicio.query(query).$promise;
                    };
                }
            });
            promesas.push($timeout(function () {
                return true;
            }));
            return $q.all(promesas).then(function () {
                //console.log(formulario);
                return formulario;
            });
        }

    }

})();

'use strict';

angular.module('integragenerico')
.service('ServicioPatrones', function ($q, Patrones) {

	this.agregarPatrones = agregarPatrones;
	this.agregarPatron = agregarPatron;

	function agregarPatrones(formulario) {
		var campos = formulario.campos;
		var promesas = [];
		campos.forEach(function(campo){
			var respuesta = agregarPatron(campo);
			if(respuesta) {
				promesas.push(respuesta);
			}
		});
		return $q.all(promesas).then(function(){
			return formulario;
		});
	}


	function agregarPatron(campo) {
		if (campo.templateOptions.patron) {
			return Patrones.getPatron(campo.templateOptions.patron.tipo).then(function(respuesta) {
				campo.templateOptions.pattern = respuesta.patron;
				campo.templateOptions.patternValidationMessage = respuesta.mensaje;
			});
		}
	}
});
(function() {
	'use strict';

	angular
	.module('integragenerico').service('ServicioValidadoresPersonalizados', ServicioValidadoresPersonalizados);

	function ServicioValidadoresPersonalizados($injector, $q) {

		var vm = this;

		vm.agregarValidadores = agregarValidadores;

		function agregarValidadores(campos) {
			campos.forEach(function(campo){
				agregarValidador(campo);
			});
		}

		function agregarValidador(campo) {
			//Si no existe creo
			if (!campo.asyncValidators) {
				campo.asyncValidators = {};
			}
			//pregunto si tiene validadores
			if (campo.templateOptions) {
				agregarValidadorUnico(campo);
			}
		}

		function agregarValidadorUnico(campo) {
			if (!campo.templateOptions.unico){
				return;	
			} 

			campo.asyncValidators.unico = {
				expression : function($viewValue, $modelValue){
					var valor = $modelValue || $viewValue;
					var servicio = $injector.get(campo.templateOptions.unico.servicio);
					var promesa =  servicio.unico({'codigo':valor}).$promise.then( 
						function(data){
							if (!data.estado) {
								return $q.reject('Repetido');
							}else {
								return data.estado;
							}
						});
					return promesa;
				},
				message: campo.templateOptions.unico.mensaje
			};
		}
	} 

})();
(function() {
    'use strict';

    angular
    .module('integragenerico')
    .directive('fileread', fileread);

    function fileread() {
        return {
            scope: {
                fileread: '='
            },
            link: function (scope, element, attributes) {
                element.bind('change', function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileread = loadEvent.target.result;
                        });
                    };
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
        };

    }
})();
(function () {
  'use strict';

  angular
    .module('integragenerico').service('Filtro', Filtro);

  function Filtro() {

    var vm = this;

    vm.getFiltro = getFiltro;

    vm.generarFiltros = generarFiltros;

    vm.getDelay = getDelay;

    function filtrar(lista) {
      lista.filtro.filtro = {};
      lista.cols.forEach(function (col) {
        if (col.filter && (!col.tipo || col.tipo === 'celda-estatica')) {
          if (col.filterField) {
            lista.filtro.filtro[col.filterField] = lista.filtro.texto;
          } else {
            lista.filtro.filtro[col.field] = lista.filtro.texto;
          }
        }
      });
      lista.tableParams.reload();
    }

    function generarFiltros(query, filter, filtroGeneral) {
      for (var key in filtroGeneral) {
        if (filtroGeneral.hasOwnProperty(key)) {
          query[key] = filtroGeneral[key];
        }
      }
      var banderaFiltroParticular = false;
      for (key in filter) {
        if (filter.hasOwnProperty(key) && filter[key]) {
          query[key] = filter[key];
          banderaFiltroParticular = true;
        }
      }
      if (banderaFiltroParticular) {
        query.strategy = 'AND';
      }
    }

    function getDelay() {
      return 1000;
    }

    function getFiltro(lista) {
      return {
        texto: '',
        claseBoton: 'btn btn-primary',
        filtro: {},
        filtrando: false,
        campos: [{
          key: 'texto',
          type: 'horizontalInput',
          className: '',
          id: 'buscador-generico',
          templateOptions: {
            label: 'Filtro ',
            placeholder: 'Ingrese su búsqueda...',
            type: 'text',
            ancho: 'col-sm-11',
            anchoLabel: 'col-sm-1',
            maxlength: 250,
            addonRight: {
              class: 'fa fa-close',
              onClick: function () {
                lista.filtro.texto = '';
              }
            }
          },
          watcher: {
            listener: function (field, newValue, oldValue) {
              if (newValue !== oldValue) {
                filtrar(lista);
              }
            }
          },
          modelOptions: {
            debounce: getDelay()
          }
        }]
      };
    }
  }

})();

(function () {
  'use strict';

  angular
    .module('integragenerico').provider('Paginacion', PaginacionProvider);

  function PaginacionProvider() {

    var configuracion = {
      page: {
        page: 1,
        count: 25
      },
      bloquesMinimos: 2,
      bloquesMaximos: 3
    };

    this.configuracion = configuracion;

    var provider = {};
    provider.generarPaginacion = generarPaginacion;
    provider.getParametros = getParametros;
    provider.getBloquesMinimos = getBloquesMinimos;
    provider.getBloquesMaximos = getBloquesMaximos;

    function generarPaginacion(query, page, count) {
      query.page = page - 1;
      query.size = count;
      query.pagination = true;
    }

    function getParametros(clave) {
      return configuracion.page;
    }

    function getBloquesMinimos(clave) {
      return configuracion.bloquesMinimos;
    }

    function getBloquesMaximos(clave) {
        return configuracion.bloquesMaximos;
      }

    this.$get = function () {
      return provider;
    };
  }

})();

(function () {
  'use strict';

  angular
    .module('integragenerico').service('ServicioTabla', ServicioTabla);

  function ServicioTabla($injector, $state, NgTableParams, CamposListado, AccionesGenerales, Filtro, Paginacion, Orden, Dialogos, TransformacionString, ConfigurarListados, ConfigurarConfirmaciones) {

    var vm = this;

    vm.getTabla = getTabla;

    function Tabla(clave) {
      var lista = {
        listar: listar,
        resultados: [],
        cargando: false,
        acciones: {},
        prefijo: TransformacionString.camelToUnderscore(clave)
      };
      lista.acciones.nuevo = function () {
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_NUEVO',
          tieneTitulo: true,
          cancelar: true
        };
        if (ConfigurarConfirmaciones.alCrear) {
          Dialogos.abrirDialogoConfirmacion(config)
            .result.then(abrirDialogoTransicion)
            .then(function () {
              confirmarEdicion('nuevo');
            });
        } else {
          confirmarEdicion('nuevo');
        }
      };
      lista.acciones.editar = function (row) {
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_EDITAR',
          tieneTitulo: true,
          cancelar: true
        };
        Dialogos.abrirDialogoConfirmacion(config)
          .result.then(abrirDialogoTransicion)
          .then(function () {
            confirmarEdicion(row.id);
          });
      };
      lista.acciones.consultar = function (row) {
        consultar(row.id);
      };
      lista.acciones.eliminar = function (row) {
        var dialogo;
        var config = {
          prefijo: lista.prefijo + '.DIALOGO_ELIMINAR',
          tieneTitulo: true,
          cancelar: true
        };
        Dialogos.abrirDialogoConfirmacion(config)
          .result.then(function () {
            dialogo = abrirDialogoCargando();
            return true;
          })
          .then(function () {
            return eliminar(clave, row).$promise;
          }).then(function () {
            Dialogos.abrirDialogoConfirmacion({
              prefijo: lista.prefijo + '.DIALOGO_ELIMINADO',
              tieneTitulo: true,
              cancelar: false
            });
            lista.tableParams.reload();
            dialogo.close();
          });
      };
      return CamposListado.getCampos(clave).then(function (data) {
        return agregarAccionesPorDefecto(data).then(function (data) {
          lista.cols = data;

          return Orden.getOrdenPorDefecto(clave).then(function (orden) {
            lista.filtro = Filtro.getFiltro(lista);
            lista.filtrosActivos = false;
            lista.mostrarFiltros = mostrarFiltros;
            lista.ordenPorDefecto = orden;
            lista.tableParams = crearTabla(clave, lista);
            return AccionesGenerales.getAcciones(clave).then(function (acciones) {
              lista.accionesGenerales = acciones;
              return lista;
            });
          });
        });
      });
    }

    function crearTabla(clave, lista) {
      var parametros = Paginacion.getParametros(clave);
      // console.log(lista);
      parametros.sorting = lista.ordenPorDefecto;
      // console.log(parametros);
      return new NgTableParams(parametros, {
        filterDelay: Filtro.getDelay(),
        paginationMaxBlocks: Paginacion.getBloquesMaximos() ? Paginacion.getBloquesMaximos() : 3,
        paginationMinBlocks: Paginacion.getBloquesMinimos(),
        getData: function (params) {
          lista.cargando = true;
          lista.resultados = lista.listar(clave, params, lista.filtro.filtro).then(function (resultados) {
            lista.cargando = false;
            return resultados;
          });
          return lista.resultados;
        }
      });
    }

    function listar(clave, params, filtro) {
      this.cargando = true;
      var parametros = params.parameters();
      var query = {};
      if (ConfigurarListados.filtra) {
        Filtro.generarFiltros(query, parametros.filter, filtro);
      }
      if (ConfigurarListados.ordena) {
        Orden.generarOrden(query, parametros.sorting);
      }
      if (ConfigurarListados.pagina) {
        Paginacion.generarPaginacion(query, parametros.page, parametros.count);
      }
      if (ConfigurarListados.pagina) {
        return $injector.get(clave).get(query).$promise.then(function (data) {
          params.total(data.totalElements);
          return data.content;
        }, function (error) {
          console.log(error);
          if (error.data.status === 403) {
            Dialogos.abrirDialogoInformacion({
              prefijo: '',
              tieneTitulo: true,
              textoDinamico: 'No tiene permisos suficientes'
            });
            $state.go('app');
          }
          return [];
        });
      } else {
        return $injector.get(clave).query(query).$promise.then(function (data) {
          return data;
        });
      }
    }

    function getTabla(clave) {
      return new Tabla(clave);
    }

    function abrirDialogoTransicion() {
      return Dialogos.abrirDialogoTransicionEstados();
    }

    function abrirDialogoCargando() {
      return Dialogos.abrirDialogoCargando();
    }

    function confirmarEdicion(id) {
      $state.go('^.editar', {
        id: id
      });
    }

    function consultar(id) {
      $state.go('^.editar', {
        id: id,
        soloLectura: true
      });
    }

    function eliminar(clave, row) {
      return $injector.get(clave).remove({
        codigo: row.id
      });
    }

    function mostrarFiltros() {
      this.filtrosActivos = !this.filtrosActivos;
    }

    function agregarAccionesPorDefecto(columnas) {
      var columnaAcciones = undefined;
      columnas.forEach(function (columna) {
        if (columna.tipo === 'celda-acciones') {
          columnaAcciones = columna;
        }
      });
      if (!columnaAcciones) {
        columnaAcciones = {
          "tipo": "celda-acciones",
          "title": "Acciones",
          "acciones": []
        };
        columnas.push(columnaAcciones);
      }
      return AccionesGenerales.getAccionesTabla().then(function (acciones) {
        acciones.forEach(function (accion) {
          var accionEncontrada = undefined;
          columnaAcciones.acciones.forEach(function (accionExistente) {
            if (accionExistente.accion === accion.accion) {
              accionEncontrada = accionExistente;
            }
          });
          if (!accionEncontrada) {
            columnaAcciones.acciones.push(accion);
          }
        });
        return columnas;
      });
    }

  }

})();

'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
angular.module('integragenerico')
  .directive('celdaAcciones', function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'integragenerico/components/lista-elementos/celda-acciones.html',
      scope: true,
      controllerAs: 'vm',
      controller: function($scope, $parse, Autenticacion1) {
        var vm = this;
        vm.ejecutarAccion = ejecutarAccion;
        vm.mostrarImp = mostrarImp;
        vm.tienePermisosImp = tienePermisosImp;

        // 			console.log($scope.tabla);
        function compilarAcciones() {
          $scope.col.acciones.forEach(function(accion) {

            accion.ejecutarAccion = $parse(accion.accion);

            var parseado = $parse(accion.mostrar);

            accion.mostrarImp = function($scope) {
              return accion.mostrar ? parseado($scope) : true;
            };
          });
        }
        compilarAcciones();

        function ejecutarAccion(accion) {
          accion.ejecutarAccion($scope);
        }

        function mostrarImp(accion) {
          return accion.mostrarImp($scope);
        }

        function tienePermisosImp(accion) {
          return accion.permisos ? Autenticacion1.tienePermisos(accion.permisos) : true;
        }

      }
    };
  });

'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
 angular.module('integragenerico')
 .directive('celdaEstatica', function($compile) {
 	return {
 		restrict: 'E',
 		replace: true,
 		scope: true,
 		link: function(scope, element) {
// 			console.log('Compilando');
 			var campo = scope.$eval('col');
 			var valor = 'row.' + campo.field;
 			if (campo.filtros && campo.filtros.length > 0) {
 				valor += '|' + campo.filtros.join('|');
 			}
 			element.html($compile(angular.element('<span>{{' + valor + '}}</span>'))(scope));
 		}
 	};
 });
'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
angular.module('integragenerico')
  .directive('celdaIcono', function ($parse) {
    return {
      restrict: 'E',
      replace: true,
      scope: true,
      templateUrl: 'integragenerico/components/lista-elementos/celda-icono.html',
      cotrollerAs: 'vm',
      controller: function ($scope, $parse) {
        // console.log($scope.col);
        // console.log($scope.row);
        var vm = this;
        var parseado = $parse($scope.col.field);
        var parseadoIcono = $parse($scope.col.campoIcono);

        $scope.$watch('row', function (newValue) {
          $scope.color = parseado(newValue);
          if ($scope.col.campoIcono) {
            $scope.icono = parseadoIcono(newValue);
          } else {
            $scope.icono = $scope.col.icono;
          }
        }, true);
      }
    };
  });

(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name bolsaEmpleo.controller:AboutCtrl
     * @description
     * # AboutCtrl
     * Controller of the bolsaEmpleo
     */
     angular.module('integragenerico')
     .directive('celdaImagen', celdaImagen);

     function celdaImagen() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'integragenerico/components/lista-elementos/celda-imagen.html',
            scope: true,
            controllerAs: 'vm',
            controller: function ($scope, Imagen) {
                var vm = this;
                //console.log($scope.row);
                var pedirImagen = function() {
                    Imagen.get({codigo:$scope.row.id, clase: $scope.col.field}).$promise.then(function(data){
                        vm.imagen=  'data:' + data.tipoImagen + ';base64,' + data.imagen;
                    });
                };
                pedirImagen();
            }
        };
    }
})();
'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
 angular.module('integragenerico')
 .directive('celda', function($compile) {
 	return {
 		restrict: 'A',
 		scope: true,
 		link: function(scope, element) {
 			var campo = scope.$eval('col');
 			var tipo = campo.tipo || 'celda-estatica';
 			element.html($compile(angular.element('<'+tipo+'></'+tipo+'>'))(scope));
 		}
 	};
 });
(function() {
    'use strict';

    angular
    .module('integragenerico').directive('paginacionPersonalizada', paginacionPersonalizada);

    function paginacionPersonalizada() {
    	return {
    		templateUrl: 'integragenerico/components/lista-elementos/paginacion-personalizada.html',
    		scope: {
    			paginacion: '=paginacionPersonalizada'
    		},
    		controller: function($scope) {

    			var self = this;

    			this.paginacion = $scope.paginacion;

    			this.calcularPrimerElemento = calcularPrimerElemento;

    			this.calcularUltimoElemento = calcularUltimoElemento;

    			this.calcularTamanioPagina = calcularTamanioPagina;

    			this.calcularTotalElementos = calcularTotalElementos;

    			function calcularPrimerElemento() {
    				return (self.paginacion.page() - 1) * self.paginacion.count() + 1;
    			}

    			function calcularUltimoElemento() {
    				var minimoTeorico = self.paginacion.page() * self.paginacion.count();
    				return Math.min(minimoTeorico,calcularTotalElementos());
    			}

    			function calcularTamanioPagina() {
    				return self.paginacion.count();
    			}

    			function calcularTotalElementos() {
    				return self.paginacion.total();
    			}

    			$scope.$watch('paginacion.count()', function(newValue) {
//    				console.log('Modelo modificado:' + self.paginacion.count());
    				self.tamanio = newValue;
    			});

    			$scope.$watch('vm.tamanio', function(newValue) {
//    				console.log('Vista modificada:' + newValue);
    				self.paginacion.count(newValue);
    			});

    		},
    		controllerAs: 'vm'
    	};
    }

})();
(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
 	 angular.module('integragenerico')
 	 .directive('tablaElementos', tablaElementos);

 	 function tablaElementos(ListaElementosTemplate) {
 	 	return {
 	 		restrict: 'E',
 	 		replace: true,
 	 		templateUrl: ListaElementosTemplate.getTemplate(),
 	 		scope: {
 	 			tabla: '='
 	 		},
 	 		controllerAs : 'vm',
 	 		controller : function ($scope,ConfigurarListados) {
 	 			var vm = this;
 	 			vm.configurarListados = ConfigurarListados;

 	 			vm.ejecutarAccionGeneral = ejecutarAccionGeneral;

 	 			function ejecutarAccionGeneral(accion) {
 	 				accion.ejecutarAccion($scope);
 	 			}
 	 		}
 	 	};
 	 }

 	})();

(function() {
  'use strict';

  angular
  .module('integragenerico').provider('AccionesGenerales', AccionesGeneralesProvider);

  function AccionesGeneralesProvider() {

   var ubicacionRecurso;
   var ubicacionAccionesColumnas;

   this.$get = function($http,$parse,$injector,$q) {

    function compilarAcciones(acciones) {
      acciones.forEach(function(accion){
        accion.ejecutarAccion = $parse(accion.accion);
      });
    }
    return {
      getAcciones : function (clave, esConsulta) {
        var recurso = $injector.get(clave);
        if (recurso && recurso.json) {
          return $http.get(recurso.json).then(function (data) {
            var acciones = !esConsulta ? data.data['AccionesGenerales'] : data.data['AccionesGeneralesConsulta'];
            if (acciones) {
              compilarAcciones(acciones);
              return acciones;
            }else {
              return [];
            }
          });
        } else {
          return $http.get(ubicacionRecurso).then(function (data) {
            var acciones = data.data[clave];
            if (acciones) {
              compilarAcciones(acciones);
              return acciones;
            }else {
              return [];
            }
          });
        }
      },
      getAccionesTabla : function() {
        if (!ubicacionAccionesColumnas) {
          return $q.when([]);
        } else {
          return $http.get(ubicacionAccionesColumnas).then(function (data) {
            var acciones = data.data;
            if (acciones) {
              compilarAcciones(acciones);
              return acciones;
            }else {
              return [];
            }
          });
        }
      }
    };
  };

  this.setUbicacionRecurso = setUbicacionRecurso;
  this.setUbicacionAccionesColumnas = setUbicacionAccionesColumnas;

  function setUbicacionRecurso(ubicacion) {
    ubicacionRecurso = ubicacion;
  }

  function setUbicacionAccionesColumnas(ubicacion) {
    ubicacionAccionesColumnas = ubicacion;
  }




}

})();

(function() {
    'use strict';

    angular
    .module('integragenerico').provider('CamposEditar', CamposEditarProvider);

    function CamposEditarProvider() {

     var ubicacionRecurso;

     this.$get = function($http, $injector) {
        return {
            getCampos : function (clave) {
                var recurso = $injector.get(clave);
                if (recurso && recurso.json) {
                    return $http.get(recurso.json).then(function (data) {
                        return data.data['CamposEditar'];
                    });

                } else {
                    return $http.get(ubicacionRecurso).then(function (data) {
                        return data.data[clave];
                    });
                }
            }
        };
    };

    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
        ubicacionRecurso = ubicacion;
    }


}

})();
(function() {
    'use strict';

    angular
    .module('integragenerico').provider('CamposListado', CamposListadoProvider);

    function CamposListadoProvider() {

       var ubicacionRecurso;

       this.$get = function($http,$injector) {
        return {
            getCampos : function (clave) {
                var recurso = $injector.get(clave);
                if (recurso && recurso.json) {
                    return $http.get(recurso.json).then(function (data) {
                        return data.data['CamposListado'];
                    });
                } else {
                    return $http.get(ubicacionRecurso).then(function (data) {
                    return data.data[clave];
                });
                }
            }
        };
    };

    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
        ubicacionRecurso = ubicacion;
    }


}

})();
(function() {
  'use strict';

  angular
    .module('integragenerico').provider('ConfigurarConfirmaciones', ConfigurarConfirmacionesProvider);

  function ConfigurarConfirmacionesProvider() {

    var confirmaciones = {
      alGuardar: true,
      alEditar: true,
      alCrear: true,
      alEliminar : true
    };

    this.$get = function() {
      return confirmaciones;
    };
    this.setAlGuardar = setAlGuardar;
    this.setAlEditar = setAlEditar;
    this.setAlCrear = setAlCrear;
    this.setAlEliminar = setAlEliminar;

    function setAlGuardar(alGuardar) {
      confirmaciones.alGuardar = alGuardar;
    }

    function setAlEditar(alEditar) {
      confirmaciones.alEditar = alEditar;
    }

    function setAlCrear(alCrear) {
      confirmaciones.alCrear = alCrear;
    }

    function setAlEliminar(alEliminar) {
      confirmaciones.alEliminar = alEliminar;
    }
  }

})();

(function () {
  'use strict';

  angular
    .module('integragenerico').provider('ConfigurarDialogos', ConfigurarDialogosProvider);

  function ConfigurarDialogosProvider() {

    var configuraciones = {
      backdrop: true
    };

    this.$get = function () {
      return configuraciones;
    };
    this.setBackdrop = setBackdrop;


    function setBackdrop(backdrop) {
      configuraciones.backdrop = backdrop;
    }
  }

})();

(function() {
    'use strict';

    angular
    .module('integragenerico').provider('ConfigurarListados', ConfigurarListadosProvider);

    function ConfigurarListadosProvider() {

        var configuracion = {
            filtra : true,
            pagina : true,
            ordena : true
        };

        this.$get = function() {
            return configuracion;
        };


        this.setFiltra = setFiltra;
        this.setPagina = setPagina;
        this.setOrdena = setOrdena;

        function setFiltra(filtra) {
            configuracion.filtra = filtra;
        }
        function setPagina(pagina) {
            configuracion.pagina = pagina;
        }
        function setOrdena(ordena) {
            configuracion.ordena = ordena;
        }


    }

})();
(function() {
  'use strict';

  angular
    .module('integragenerico').provider('EstadosGenericos', EstadosGenericosProvider);

  function EstadosGenericosProvider($stateProvider) {

    this.$get = function() {

    };

    this.crearEstados = crearEstados;

    function crearEstados(nombreClave, nombreEstado, url) {
      var controladorListar = nombreClave + 'ListaCtrl';
      var controladorEditar = nombreClave + 'EditarCtrl';

      $stateProvider
        .state('app.' + nombreEstado, {
          url: url,
          template: '<ui-view></ui-view>',
          abstract: true
        })
        .state('app.' + nombreEstado + '.lista', {
          url: '/lista',
          templateUrl: 'integragenerico/components/lista-elementos/vista-tabla-elementos.html',
          controller: controladorListar,
          controllerAs: 'vm',
          resolve: {
            tabla: function(ServicioTabla) {
              return ServicioTabla.getTabla(nombreClave);
            }
          }
        })
        .state('app.' + nombreEstado + '.editar', {
          url: '/editar/:id/?soloLectura',
          templateUrl: 'integragenerico/components/editar-elemento/vista-editar-elemento.html',
          controller: controladorEditar,
          controllerAs: 'vm',
          resolve: {
            formulario: function($stateParams, ServicioEdicion) {
              return ServicioEdicion.getFormulario(nombreClave, $stateParams.id, $stateParams.soloLectura);
            }
          }
        });
    }


  }

})();

(function() {
  'use strict';
  angular
    .module('integragenerico').provider('EditarElementosTemplate', EditarElementosTemplateProvider);

  function EditarElementosTemplateProvider() {
    var ubicacionRecurso = 'integragenerico/components/editar-elemento/formulario-editar-elemento.html';
    this.$get = function() {
      return {
        getTemplate: function() {
          return ubicacionRecurso;
        }
      };
    };
    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
      ubicacionRecurso = ubicacion;
    }
  }

})();

(function() {
  'use strict';
  angular
    .module('integragenerico').provider('ListaElementosTemplate', ListaElementosTemplateProvider);

  function ListaElementosTemplateProvider() {
    var ubicacionRecurso = 'integragenerico/components/lista-elementos/tabla-elementos.html';
    this.$get = function() {
      return {
        getTemplate: function() {
          return ubicacionRecurso;
        }
      };
    };
    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
      ubicacionRecurso = ubicacion;
    }
  }

})();

(function() {
    'use strict';

    angular
    .module('integragenerico').provider('Orden', OrdenProvider);

    function OrdenProvider() {

       var ubicacionRecurso;

       this.$get = function($http,$injector) {
        return {
            getOrdenPorDefecto : function (clave) {
                var recurso = $injector.get(clave);
                if (recurso && recurso.json) {
                    return $http.get(recurso.json).then(function (data) {
                        var defecto = data.data['Orden'];
                        var respuesta = {};
                        if (!defecto) {
                            return respuesta;
                        }
                        defecto.forEach(function(orden){
                            respuesta[orden.propiedad] = orden.desc ? 'desc' : 'asc';
                        });
                        return respuesta;
                    });
                } else {
                    return $http.get(ubicacionRecurso).then(function (data) {
                        var defecto = data.data[clave];
                        var respuesta = {};
                        if (!defecto) {
                            return respuesta;
                        }
                        defecto.forEach(function(orden){
                            respuesta[orden.propiedad] = orden.desc ? 'desc' : 'asc';
                        });
                        return respuesta;
                    });
                }
            },
            generarOrden : function (query, sorting) {
                var sort = [];
                for (var key in sorting) {
                    if (sorting.hasOwnProperty(key)) {
                        sort.push(key+','+sorting[key]);
                    }
                }
                if (sort.length === 0) {
					sort.push('id,asc');
				}
                query.sort = sort;
            }
        };
    };

    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
        ubicacionRecurso = ubicacion;
    }


}

})();
(function() {
    'use strict';

    angular
    .module('integragenerico').provider('Patrones', PatronesProvider);

    function PatronesProvider() {

       var ubicacionRecurso;

       this.$get = function($http) {
        return {
            getPatron : function (clave) {
                return $http.get(ubicacionRecurso).then(function (data) {
                    return data.data[clave];
                });
            }
        };
    };

    this.setUbicacionRecurso = setUbicacionRecurso;

    function setUbicacionRecurso(ubicacion) {
        ubicacionRecurso = ubicacion;
    }


}

})();
(function() {
  'use strict';

  angular
  .module('integragenerico.seguridad')
  .service('AlmacenUsuarios', AlmacenUsuarios);
/**
 * @ngdoc service
 * @name negocioGenerico.ServicioAlmacenUsuarios
 * @description
 * # ServicioAlmacenUsuarios
 * Service in the negocioGenerico.
 */
 function AlmacenUsuarios ($base64, $localStorage, $sessionStorage) {

  var getAutenticacion = function() {
    if ($sessionStorage.autenticacion) {
      return $sessionStorage.autenticacion;
    }
    if ($localStorage.autenticacion) {
      return $localStorage.autenticacion;
    }
    return undefined;
  };

  var guardar = function(autenticacion, recordar) {
    if (recordar) {
      $localStorage.autenticacion = autenticacion;
    }else {
      $sessionStorage.autenticacion = autenticacion;
    }
  };

  this.limpiar = function () {
    delete $localStorage.autenticacion;
    delete $sessionStorage.autenticacion;
  };

  this.guardarAutenticacion = function (nombreUsuario, password, recordar) {
    if (nombreUsuario && password) {
      var autenticacion = {};
      autenticacion.nombreUsuario = nombreUsuario;
      autenticacion.password = password;
      autenticacion.encabezado = 'Basic ' + $base64.encode(nombreUsuario + ':' + password);
      guardar(autenticacion, recordar);
    }
  };

  this.guardarUsuario = function (usuario) {
    if (usuario) {
      $sessionStorage.usuario = usuario;
    }
  };

  this.getUsuario = function () {
    return $sessionStorage.usuario;
  };

  this.getAutenticacion = function () {
    return getAutenticacion().authdata;
  };

  this.getAutenticacionBasica = function() {
    if (!validarAutenticacionBasica()) {
      return undefined;
    }
    return getAutenticacion().encabezado;
  };

  var validarAutenticacionBasica = function() {
    return getAutenticacion() && getAutenticacion().encabezado;
  };

  this.usuarioActualizado = function() {
    return !!$sessionStorage.usuario;
  };

  this.usuarioLogueado = function() {
    return getAutenticacion() && getAutenticacion().encabezado;
  };

  this.tienePermiso = function(permiso) {
                var respuesta = false;
                var usuario = this.getUsuario();
                if (usuario !== undefined && usuario.roles !== undefined) {
                    usuario.roles.forEach(function(rol) {
                        rol.permisos.forEach(function(permisoRol) {
                            if (permiso === permisoRol.nombre || permiso.indexOf(permisoRol.nombre) !== -1) {
                                respuesta = true;
                            }
                        });
                    });
                }
                return respuesta;
            }
}

})();

(function () {
    'use strict';

    angular
        .module('integragenerico.seguridad')
        .service('Autenticacion1', Autenticacion);

    function Autenticacion($q, AlmacenUsuarios, Usuario) {

        this.login = function (nombreUsuario, password, recordar) {
            AlmacenUsuarios.guardarAutenticacion(nombreUsuario, password, recordar);
            return actualizarUsuario().$promise;
        };

        var actualizarUsuario = function () {
            return Usuario.login(function (usuario) {
                AlmacenUsuarios.guardarUsuario(usuario);
                AlmacenUsuarios.getUsuario().avatar = Usuario.getImagen(usuario.id);
                return usuario;
            }, function (error) {
                AlmacenUsuarios.limpiar();
                return $q.reject('Error de login');
            });
        };

        this.tienePermiso = function (permiso) {
            return AlmacenUsuarios.tienePermiso(permiso);
        };

        this.tienePermisos = function (permisos) {
            var autorizado = false;
            permisos.forEach(function (permiso) {
                if (AlmacenUsuarios.tienePermiso(permiso)) {
                    autorizado = true;
                }
            });
            return autorizado;
        };

        this.validarUsuario = function () {
            return actualizarUsuario().$promise;
        };
    }
})();

'use strict';

 angular.module('integragenerico.seguridad')
 .service('Autenticacion', function ($q, AlmacenUsuarios, Usuario) {

    var vm = this;

    vm.login = login;

    vm.tienePermiso = tienePermiso;

    vm.validarUsuario = validarUsuario;

    vm.getAutenticacionBasica = getAutenticacionBasica;

    var actualizacion;

    function login(nombreUsuario, password, recordar) {
        AlmacenUsuarios.guardarAutenticacion(nombreUsuario, password, recordar);
        return actualizarUsuario();
    }

    function actualizarUsuario() {
        if (!actualizacion || actualizacion.$$state.status) {
            actualizacion = Usuario.login().$promise.then(function (usuario) {
                AlmacenUsuarios.guardarUsuario(usuario);
                Usuario.getImagen({codigo: usuario.id}).$promise.then(function(imagen){
                  AlmacenUsuarios.getUsuario().avatar = imagen;
                });
                return usuario;
            }, function (error) {
                AlmacenUsuarios.limpiar();
                return $q.reject({mensaje:"El usuario no se pudo loguear",estado:'No logueado'});
            });
            //console.log(actualizacion);
        }
        return actualizacion;
    }

    function tienePermiso(permiso) {
        return AlmacenUsuarios.tienePermiso(permiso);
    }

    function validarUsuario() {
        return actualizarUsuario();
    }

    function getAutenticacionBasica() {
        return AlmacenUsuarios.getAutenticacionBasica();
    }
});

(function() {
  'use strict';

  angular
    .module('integragenerico.seguridad')
    .controller('InfoUsuarioCtrl', InfoUsuarioCtrl);

  /** @ngInject */
  function InfoUsuarioCtrl(AlmacenUsuarios, $timeout, $state, $modal) {
    var vm = this;

    vm.cerrarSesion = function() {
      AlmacenUsuarios.limpiar();
      $timeout(function() {
        $state.go('login');
      });
    };

    vm.cambiarPassword = function() {
      var modalInstance = $modal.open({
        templateUrl: 'integragenerico/components/usuarios/cambiar-pass/cambiar-clave.html',
        controller: 'CambiarClaveCtrl',
        size: 'lg',
        resolve: {
          objetoModificando: function() {
            return AlmacenUsuarios.getUsuario();
          }
        }
      });
    }

    vm.menuVisible = false;

    vm.getImagen = function() {
      if (!AlmacenUsuarios.getUsuario().id || !AlmacenUsuarios.getUsuario().avatar) {
        return undefined;
      } else {
        // console.log(AlmacenUsuarios.getUsuario().avatar);
        return "data:" + AlmacenUsuarios.getUsuario().avatar.tipoImagen + ";base64," + AlmacenUsuarios.getUsuario().avatar.imagen;
      }
    };

    vm.getUsuario = function() {
      return AlmacenUsuarios.getUsuario();
    };

    vm.mostrarMenu = function() {
      vm.menuVisible = !vm.menuVisible;
    };


  }
})();

(function() {
    'use strict';

    angular
    .module('integragenerico.seguridad')
    .factory('Usuario', Usuario);

    function Usuario($resource, URL_SERVIDOR) {
        var Usuario = $resource(URL_SERVIDOR + 'usuarios/:codigo', {
            codigo: '@id'
        }, {
            invertirEstado: {
                url: URL_SERVIDOR + 'usuarios/:codigo/invertir-estado',
                method: 'PUT'
            },
            cambiarPassword: {
                url: URL_SERVIDOR + 'usuarios/cambiar-password',
                method: 'PUT'
            },
            cambiarClave : {
              url: URL_SERVIDOR + 'usuarios/actual/cambiar-password',
              method: 'POST'
            },
            getImagen: {
                url: URL_SERVIDOR + 'usuarios/:codigo/imagen',
                method: 'GET'
            },
            login: {
                url: URL_SERVIDOR + '/usuarios/actual',
                method: 'GET'
            }
        });

        Usuario.prototype.iconoEstado = function() {
            return (this.habilitado !== undefined && this.habilitado) ? 'fa fa-check' : 'fa fa-ban';
        };

        return Usuario;
    }
})();

(function() {
  'use strict';

  angular
  .module('integragenerico')
  .filter('porcentaje', Porcentaje)
  .filter('numeroEntero', NumeroEntero)
  .filter('numeroDecimal', NumeroDecimal);

  /** @ngInject */
  function Porcentaje($filter) {
    return function (valor) {
      return $filter('number')(valor, 2) + ' %';
    };
  }

  function NumeroEntero($filter) {
    return function (valor) {
      return $filter('number')(valor, 0);
    };
  }


  function NumeroDecimal($filter) {
    return function (valor) {
      return $filter('number')(valor, 2);
    };
  }
})();

(function() {
    'use strict';

    angular
    .module('integragenerico')
    .service('TransformacionString', TransformacionString);


    function TransformacionString() {

    	var vm = this;

    	vm.camelToUnderscore = camelToUnderscore;
        
    	function camelToUnderscore(string) {
    		var underscore = string.replace(/[A-Z]/, function(substring) {
    			return '_' + substring;
    		});
    		var mayus = underscore.toUpperCase();
    		if (mayus.indexOf('_') === 0) {
    			mayus = mayus.slice(1);
    		}
    		return mayus;
    	}


    }
    
})();
'use strict';

var util = angular.module('integragenerico');

/**
 * Directiva que detecta los eventos keyDown y KeyPress y si es un Enter (13)
 * llama a la funcion que recibe como parametro.
 * <input type="text" on-enter="actualizarFiltro()">
 */
util.directive('onEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if (event.which === 13) {
        scope.$apply(function () {
          scope.$eval(attrs.onEnter);
        });
        event.preventDefault();
      }
    });
  };
});

util.directive('selectOnFocus', function () {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      element.on('focus', function () {
        this.select();
      });
    }
  };
});

util.filter('reverse', function () {
  return function (items) {
    return items.slice().reverse();
  };
});

util.directive('showErrorDate', [
  '$timeout', 'showErrorsDatetimeConfig', '$interpolate',
  function ($timeout, showErrorsConfig, $interpolate) {
    var getShowSuccess, getTrigger, linkFn;
    getTrigger = function (options) {
      var trigger;
      trigger = showErrorsConfig.trigger;
      if (options && (options.trigger !== null)) {
        trigger = options.trigger;
      }
      return trigger;
    };
    getShowSuccess = function (options) {
      var showSuccess;
      showSuccess = showErrorsConfig.showSuccess;
      if (options && (options.showSuccess !== null)) {
        showSuccess = options.showSuccess;
      }
      return showSuccess;
    };
    linkFn = function (scope, el, attrs, formCtrl) {
      var blurred, inputEl, padre, padreNg, inputName, inputNgEl, options, showSuccess, toggleClasses, trigger;
      blurred = false;
      options = scope.$eval(attrs.showErrors);
      showSuccess = getShowSuccess(options);
      trigger = getTrigger(options);
      inputEl = el[0].querySelector('[name] .form-control');
      padre = el[0].querySelector('[name]');
      padreNg = angular.element(padre);
      inputNgEl = angular.element(inputEl);
      inputName = $interpolate(padreNg.attr('name') || '')(scope);
      if (!inputName) {
        throw "show-errors element has no child input elements with a 'name' attribute and a 'form-control' class";
      }
      inputNgEl.bind(trigger, function () {
        blurred = true;
        return toggleClasses(formCtrl.$invalid);
      });
      scope.$watch(function () {
        return formCtrl.$invalid;
      }, function (invalid) {
        if (!blurred) {
          return;
        }
        return toggleClasses(invalid);
      });
      scope.$on('show-errors-check-validity', function () {
        return toggleClasses(formCtrl[inputName].$invalid);
      });
      scope.$on('show-errors-reset', function () {
        return $timeout(function () {
          el.removeClass('has-error');
          el.removeClass('has-success');
          return blurred = false;
        }, 0, false);
      });
      return toggleClasses = function (invalid) {
        el.toggleClass('has-error', invalid);
        if (showSuccess) {
          return el.toggleClass('has-success', !invalid);
        }
      };
    };
    return {
      restrict: 'A',
      require: '^form',
      compile: function (elem, attrs) {
        if (attrs['showErrorDate'].indexOf('skipFormGroupCheck') === -1) {
          if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
            throw "show-errors element does not have the 'form-group' or 'input-group' class";
          }
        }
        return linkFn;
      }
    };
  }
]).provider('showErrorsDatetimeConfig', function () {
  var _showSuccess, _trigger;
  _showSuccess = false;
  _trigger = 'blur';
  this.showSuccess = function (showSuccess) {
    return _showSuccess = showSuccess;
  };
  this.trigger = function (trigger) {
    return _trigger = trigger;
  };
  this.$get = function () {
    return {
      showSuccess: _showSuccess,
      trigger: _trigger
    };
  };
});

util.directive('showErrorEtiqueta', [
  '$timeout', 'showErrorsEtiquetaConfig', '$interpolate',
  function ($timeout, showErrorsConfig, $interpolate) {
    var getShowSuccess, getTrigger, linkFn;
    getTrigger = function (options) {
      var trigger;
      trigger = showErrorsConfig.trigger;
      if (options && (options.trigger !== null)) {
        trigger = options.trigger;
      }
      return trigger;
    };
    getShowSuccess = function (options) {
      var showSuccess;
      showSuccess = showErrorsConfig.showSuccess;
      if (options && (options.showSuccess !== null)) {
        showSuccess = options.showSuccess;
      }
      return showSuccess;
    };
    linkFn = function (scope, el, attrs, formCtrl) {
      var blurred, inputEl, padre, padreNg, inputName, inputNgEl, options, showSuccess, toggleClasses, trigger;
      blurred = false;
      options = scope.$eval(attrs.showErrors);
      showSuccess = getShowSuccess(options);
      trigger = getTrigger(options);
      inputEl = el[0].querySelector('[name] tags-input');
      padre = el[0].querySelector('[name]');
      padreNg = angular.element(padre);
      inputNgEl = angular.element(inputEl);
      inputName = $interpolate(padreNg.attr('name') || '')(scope);
      if (!inputName) {
        throw "show-errors element has no child input elements with a 'name' attribute and a 'form-control' class";
      }
      inputNgEl.bind(trigger, function () {
        blurred = true;
        return toggleClasses(formCtrl.$invalid);
      });
      scope.$watch(function () {
        return formCtrl.$invalid;
      }, function (invalid) {
        if (!blurred) {
          return;
        }
        return toggleClasses(invalid);
      });
      scope.$on('show-errors-check-validity', function () {
        return toggleClasses(formCtrl[inputName].$invalid);
      });
      scope.$on('show-errors-reset', function () {
        return $timeout(function () {
          el.removeClass('has-error');
          el.removeClass('has-success');
          return blurred = false;
        }, 0, false);
      });
      return toggleClasses = function (invalid) {
        el.toggleClass('has-error', invalid);
        if (showSuccess) {
          return el.toggleClass('has-success', !invalid);
        }
      };
    };
    return {
      restrict: 'A',
      require: '^form',
      compile: function (elem, attrs) {
        if (attrs['showErrorEtiqueta'].indexOf('skipFormGroupCheck') === -1) {
          if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
            throw "show-errors element does not have the 'form-group' or 'input-group' class";
          }
        }
        return linkFn;
      }
    };
  }
]).provider('showErrorsEtiquetaConfig', function () {
  var _showSuccess, _trigger;
  _showSuccess = false;
  _trigger = 'blur';
  this.showSuccess = function (showSuccess) {
    return _showSuccess = showSuccess;
  };
  this.trigger = function (trigger) {
    return _trigger = trigger;
  };
  this.$get = function () {
    return {
      showSuccess: _showSuccess,
      trigger: _trigger
    };
  };
});

/**
 * Directiva que genera la tabla lista de objetos columnas.
 */
util.directive('tablaOrdenable', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      cambiarOrden: '=',
      esOrden: '=',
      columnas: '=',
      elementos: '=',
      acciones: '=',
      footer: '=',
      mostrarDialogoOrden: '='
    },
    templateUrl: 'integragenerico/components/util/tabla_ordenable.html',
    controller: ['$scope', '$parse', function ($scope, $parse) {
      $scope.cambiarOrdenImp = function (nombreCampo) {
        if ($scope.cambiarOrden !== undefined) {
          $scope.cambiarOrden(nombreCampo);
        } else if ($scope.$parent.cambiarOrden !== undefined) {
          $scope.$parent.cambiarOrden(nombreCampo);
        }
      };
      $scope.esOrdenImp = function (nombreCampo, reverso) {
        if ($scope.esOrden !== undefined) {
          return $scope.esOrden(nombreCampo, reverso);
        } else if ($scope.$parent.esOrden !== undefined) {
          return $scope.$parent.esOrden(nombreCampo, reverso);
        } else
          return undefined;
      };
      $scope.devolverValor = function (elemento, columna, campo) {
        var nombreFuncion = campo ? 'devolverValor' + campo : 'devolverValor';
        if (columna[nombreFuncion] === undefined) {
          if (campo) {
            columna[nombreFuncion] = $parse(columna[campo]);
          } else {
            columna[nombreFuncion] = $parse(columna.nombreCampo);
          }

        }
        return columna[nombreFuncion](elemento);
      };

    }]
  };
});

/**
 * Directiva que genera la fila de encabezado de una tabla en base a una lista de
 * objetos encabezado. Y cada uno de esos encabezados es un encabezado ordenable.
 */
util.directive('filaEncabezadoOrdenable', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      cambiarOrden: '=',
      esOrden: '=',
      columnas: '=',
      acciones: '=',
      idTabla: '=',
      mostrarDialogoOrden: '='
    },
    templateUrl: 'integragenerico/components/util/fila_encabezado_ordenable.html',
    controller: ['$scope', function ($scope) {
      $scope.cambiarOrdenImp = function (nombreCampo) {
        if ($scope.cambiarOrden !== undefined) {
          $scope.cambiarOrden(nombreCampo);
        } else if ($scope.$parent.cambiarOrden !== undefined) {
          $scope.$parent.cambiarOrden(nombreCampo);
        }
      };
      $scope.esOrdenImp = function (nombreCampo, reverso) {
        if ($scope.esOrden !== undefined) {
          return $scope.esOrden(nombreCampo, reverso);
        } else if ($scope.$parent.esOrden !== undefined) {
          return $scope.$parent.esOrden(nombreCampo, reverso);
        } else
          return undefined;
      };
    }]
  };
});

/**
 * Directiva que define un encabezado que ordena el contenido de la tabla en base
 * a la columna de ese encabezado. Ademas muestra una flechita indicando que columna
 * y en que direccion se esta ordenando
 */
util.directive('encabezadoOrdenable', function () {
  return {
    restrict: 'A',
    replace: false,
    scope: {
      campo: '=',
      cambiarOrden: '=',
      esOrden: '='
    },
    //        template: '<th>hola</th>'
    templateUrl: 'integragenerico/components/util/encabezado_ordenable.html',
    controller: ['$scope', function ($scope) {
      $scope.cambiarOrdenImp = function (nombreCampo) {
        if ($scope.cambiarOrden !== undefined) {
          $scope.cambiarOrden(nombreCampo);
        } else if ($scope.$parent.cambiarOrden !== undefined) {
          $scope.$parent.cambiarOrden(nombreCampo);
        }
      };
      $scope.esOrdenImp = function (nombreCampo, reverso) {
        if ($scope.esOrden !== undefined) {
          return $scope.esOrden(nombreCampo, reverso);
        } else if ($scope.$parent.esOrden !== undefined) {
          return $scope.$parent.esOrden(nombreCampo, reverso);
        } else
          return undefined;
      };
      $scope.ejecutarAccion = function (campo) {
        if (campo.tipo === 'Seleccionable') {
          return function () {};
        } else {
          return $scope.cambiarOrdenImp(campo.nombreCampo)
        }

      };
    }]
  };
});

util.directive('encabezadoSeleccionable', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      campo: '='
    },
    templateUrl: 'integragenerico/components/util/encabezado_seleccionable.html',
    controller: ['$scope', function ($scope) {
      //                $scope.$watch('campo.todosSeleccionados', function(newValue, oldValue) {
      //                    if (oldValue && !newValue) {
      //                        $scope.funcionSeleccionarTodos(false);
      //                    } else {
      //                        $scope.funcionSeleccionarTodos(true);
      //                    }
      //                });
      //                $scope.$watch('campo.funcionValidarSeleccionarTodos()', function(newValue, oldValue) {
      //                    $scope.campo.todosSeleccionados = newValue;
      //                });
    }]
  };
});

/**
 * Directiva que genera la tabla lista de objetos columnas.
 */
util.directive('paginacionConCantidadItems', function () {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      paginacion: '='
    },
    templateUrl: 'integragenerico/components/util/paginacion_con_cantidad_items.html'
  };
});

/**
 * Directiva que genera el filtro para la tabla.
 */
util.directive('filtroTabla', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      filtro: '='
    },
    templateUrl: 'integragenerico/components/util/filtro_tabla.html'
  };
});

/**
 * Directiva que genera el filtro para la tabla.
 */
util.directive('filtroSeleccion', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      filtro: '='
    },
    templateUrl: 'integragenerico/components/util/filtro-seleccion.html'
  };
});

util.directive('datepickerPopup', function () {
  return {
    restrict: 'EAC',
    require: 'ngModel',
    link: function (scope, element, attr, controller) {
      //remove the default formatter from the input directive to prevent conflict
      controller.$formatters.shift();
    }
  };
});

util.directive('feriado', function ($q) {
  return {
    require: 'ngModel',
    link: function (scope, elm, attrs, ctrl) {
      ctrl.$asyncValidators.feriado = function (modelValue, viewValue) {
        var promesa = $q.defer();
        scope.campo.restricciones.fechaDeshabilitada(modelValue).$promise.then(function (result) {
          if (result.esFeriado) {
            promesa.reject(false);
          } else {
            promesa.resolve(true);
          }
        });
        return promesa.promise;
      };
    }
  };
});

util.directive('elementoSeleccionable', function () {
  return {
    restrict: 'A',
    replace: true,
    scope: {
      elemento: '=',
      columna: '='
    },
    controller: ['$scope', '$parse', function ($scope, $parse) {
      var funcionPrivada = $parse($scope.columna.nombreCampo);
      $scope.funcion = function (nuevoValor) {
        if (angular.isDefined(nuevoValor)) {
          funcionPrivada.assign($scope.elemento, nuevoValor);
        }
        return funcionPrivada($scope.elemento);
      };
      $scope.$watch('valor', function () {
        $scope.funcion($scope.valor);
      });
      $scope.$watch('funcion()', function () {
        $scope.valor = $scope.funcion();
      });
    }],
    templateUrl: 'integragenerico/components/util/elemento-seleccionable.html'
  };
});

util.filter('numberFixedLen', function () {
  return function (n, len) {
    var num = parseInt(n, 10);
    len = parseInt(len, 10);
    if (isNaN(num) || isNaN(len)) {
      return n;
    }
    num = '' + num;
    while (num.length < len) {
      num = '0' + num;
    }
    return num;
  };
});

util.factory('Base64', function () {
  /* jshint ignore:start */

  var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

  return {
    encode: function (input) {
      var output = "";
      var chr1, chr2, chr3 = "";
      var enc1, enc2, enc3, enc4 = "";
      var i = 0;

      do {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
          enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
          enc4 = 64;
        }

        output = output +
          keyStr.charAt(enc1) +
          keyStr.charAt(enc2) +
          keyStr.charAt(enc3) +
          keyStr.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
      } while (i < input.length);

      return output;
    },
    decode: function (input) {
      var output = "";
      var chr1, chr2, chr3 = "";
      var enc1, enc2, enc3, enc4 = "";
      var i = 0;

      // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
      var base64test = /[^A-Za-z0-9\+\/\=]/g;
      if (base64test.exec(input)) {
        window.alert("There were invalid base64 characters in the input text.\n" +
          "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
          "Expect errors in decoding.");
      }
      input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

      do {
        enc1 = keyStr.indexOf(input.charAt(i++));
        enc2 = keyStr.indexOf(input.charAt(i++));
        enc3 = keyStr.indexOf(input.charAt(i++));
        enc4 = keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
          output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
          output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

      } while (i < input.length);

      return output;
    }
  };

  /* jshint ignore:end */
});

(function () {
    'use strict';

    angular
        .module('integragenerico')
        .service('UtilidadesGenerico', UtilidadesGenerico);


    function UtilidadesGenerico() {

        var vm = this;

        vm.eliminarAccionConsultar = eliminarAccionConsultar;

        function eliminarAccionConsultar(columnas) {
            columnas.forEach(function (columna) {
                if (columna.tipo === 'celda-acciones') {
                    columna.acciones.forEach(function (accion) {
                        if (accion.popover === 'Consultar') {
                            accion.mostrar = function () {
                                return false;
                            }
                        }
                    })
                }
            });
        }

    }

})();
(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name bolsaEmpleo.controller:AboutCtrl
	* @description
	* # AboutCtrl
	* Controller of the bolsaEmpleo
	*/
	angular.module('integragenerico')
	.controller('ConjuntoTelefonosCtrl', ConjuntoTelefonosCtrl);

	function ConjuntoTelefonosCtrl($scope) {

		var vm = this;

		vm.agregarTelefono = agregarTelefono;

		vm.quitarTelefono = quitarTelefono;

		$scope.$watch(angular.bind(vm, function () {
			return vm.telefonos;
		}), function() {
			if (!vm.telefonos) {
				vm.telefonos = [];
			}
		});

		function agregarTelefono() {
			vm.telefonos.push({});
		}

		function quitarTelefono(index) {
			vm.telefonos.splice(index,1);
		}

	}

})();

(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
	angular.module('integragenerico')
	.directive('conjuntoTelefonos', conjuntoTelefonos);

	function conjuntoTelefonos() {
		return {
			restrict: 'E',
			templateUrl: 'integragenerico/components/formly/conjunto-telefonos/conjunto-telefonos.html',
			controller: 'ConjuntoTelefonosCtrl as vm',
			bindToController: true,
			scope: {
				telefonos: '=ngModel',
				options: '='
			}
		};
	}

})();

(function() {
  'use strict';

  angular.module('integragenerico')
  .controller('InputDireccionCtrl', InputDireccionCtrl);

  function InputDireccionCtrl($scope, orderByFilter, LugarGeografico, TipoLugarGeografico) {

    var vm = this;

    vm.jerarquia = {};

    vm.titulo = 'Dirección';

    vm.paises = [];
    console.log(vm.model);

    var tipos = TipoLugarGeografico.query();

    LugarGeografico.query({'tipo.nombre': 'País'}).$promise.then(function(data) {
      var paises = orderByFilter(data,'nombre');
      paises.push({
        id: 0,
        nombre: 'otro'
      });
      vm.paises.length = 0;
      angular.copy(paises,vm.paises);
    });

    vm.provincias = [];

    vm.departamentos = [];

    vm.localidades = [];

    $scope.$watch(function() {
      return vm.model.lugar;
    }, actualizarLugarSeleccionado);

    $scope.$watch(function() {
      return vm.jerarquia.pais;
    }, buscarProvincias);

    $scope.$watch(function() {
      return vm.jerarquia.provincia;
    }, buscarDepartamentos);

    $scope.$watch(function() {
      return vm.jerarquia.departamento;
    }, buscarLocalidades);

    $scope.$watch(function() {
      return vm.jerarquia.localidad;
    }, actualizarLugarGeografico);

    $scope.$watch(function() {
      return vm.jerarquia.nuevoPais;
    }, actualizarLugarGeografico);

    $scope.$watch(function() {
      return vm.jerarquia.nuevoDepartamento;
    }, actualizarLugarGeografico);

    $scope.$watch(function() {
      return vm.jerarquia.nuevaLocalidad;
    }, actualizarLugarGeografico);

    function getTipo(nombre) {
      var tipo;
      tipos.forEach(function(elemento) {
        if (elemento.nombre === nombre) {
          tipo = elemento;
        }
      });
      return tipo;
    }

    function actualizarLugarSeleccionado() {
      if (vm.model.lugar.lugar) {
          if (vm.model.lugar.lugar.tipo.nombre === 'Localidad') {
            vm.jerarquia.pais = vm.model.lugar.lugar.padre.padre.padre;
            vm.jerarquia.provincia = vm.model.lugar.lugar.padre.padre;
            vm.jerarquia.departamento = vm.model.lugar.lugar.padre;
            vm.jerarquia.localidad = vm.model.lugar.lugar;
            // console.log(JSON.stringify(vm.jerarquia.provincia));
          }
      }
    }

    function actualizarLugarGeografico() {
      if (!vm.model) return;
      if (!vm.model.lugar) {
        vm.model.lugar = {};
      }
      if (vm.jerarquia.localidad && vm.jerarquia.localidad.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.localidad.nombre;
        vm.model.lugar.lugar = vm.jerarquia.localidad;
        vm.model.lugar.tipo = vm.jerarquia.localidad.tipo;
        vm.model.lugar.padreOriginal = vm.jerarquia.localidad.padre;
        vm.model.lugar.padre = null;
      } else if (vm.jerarquia.departamento && vm.jerarquia.departamento.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = vm.jerarquia.departamento;
        vm.model.lugar.padre = null;
      } else if (vm.jerarquia.provincia && vm.jerarquia.provincia.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = null;
        vm.model.lugar.padre = {
          nombreLugar: vm.jerarquia.nuevoDepartamento,
          lugar: null,
          tipo: getTipo('Departamento'),
          padreOriginal: vm.jerarquia.provincia,
          padre: null
        };
      } else if (vm.jerarquia.pais && vm.jerarquia.pais.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = vm.jerarquia.pais;
        vm.model.lugar.padre = null;
      } else {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = null;
        vm.model.lugar.padre = {
          nombreLugar: vm.jerarquia.nuevoPais,
          lugar: null,
          tipo: getTipo('País'),
          padreOriginal: null,
          padre: null
        };
      }
    }

    function buscarProvincias() {
      if (vm.jerarquia.pais && vm.jerarquia.pais.tieneHijos) {
        var provincias = LugarGeografico.query({'padre.id': vm.jerarquia.pais.id});
        provincias.$promise.then(function(data) {
          provincias = orderByFilter(data,'nombre');
          if (vm.jerarquia.pais.crearHijos) {
            provincias.push({
              id: 0,
              nombre: 'otro'
            });
          }
          angular.copy(provincias,vm.provincias);
          actualizarLugarGeografico();
        });
      } else {
        vm.jerarquia.provincia = undefined;
        vm.provincias.length = 0;
      }
    }

    function buscarDepartamentos() {
      if (vm.jerarquia.provincia && vm.jerarquia.provincia.tieneHijos) {
        var departamentos = LugarGeografico.query({'padre.id': vm.jerarquia.provincia.id});
        departamentos.$promise.then(function(data) {
          departamentos = orderByFilter(data,'nombre');
          if (vm.jerarquia.provincia.crearHijos) {
            departamentos.push({
              id: 0,
              nombre: 'otro'
            });
          }
          angular.copy(departamentos,vm.departamentos);
          actualizarLugarGeografico();
        });
      } else {
        vm.jerarquia.departamento = undefined;
        vm.departamentos.length = 0;
      }
    }

    function buscarLocalidades() {
      if (vm.jerarquia.departamento && vm.jerarquia.departamento.tieneHijos) {
        var localidades = LugarGeografico.query({'padre.id': vm.jerarquia.departamento.id});
        localidades.$promise.then(function(data) {
          localidades = orderByFilter(data,'nombre');
          if (vm.jerarquia.departamento.crearHijos) {
            localidades.push({
              id: 0,
              nombre: 'otro'
            });
          }
          angular.copy(localidades,vm.localidades);
          actualizarLugarGeografico();
        });
      } else {
        vm.jerarquia.localidad = undefined;
        vm.localidades.length = 0;
      }
    }

    vm.campos = [
      {
        key: 'calle',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Calle:',
          placeholder: 'Calle',
          required: true
        }
      },
      {
        key: 'numero',
        type: 'horizontalInput',
        templateOptions: {
          type: 'number',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Número:',
          placeholder: 'Número',
          required: true
        }
      },
      {
        key: 'piso',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Piso:',
          placeholder: 'Piso'
        }
      },
      {
        key: 'departamento',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Departamento:',
          placeholder: 'Departamento'
        }
      },
      {
        key:'pais',
        type:'horizontalSelect',
        model: vm.jerarquia,
        templateOptions:{
          label:'País:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true,
          options: vm.paises,
          ngOptions:'pais as pais.nombre for pais in to.options track by pais.id'
        }
      },
      {
        key:'nuevoPais',
        type:'horizontalInput',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.pais && !vm.jerarquia.pais.id);
        },
        templateOptions:{
          label:'Nombre de País:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true
        }
      },
      {
        key:'provincia',
        type:'horizontalSelect',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.pais && vm.jerarquia.pais.id && vm.jerarquia.pais.tieneHijos);
        },
        templateOptions:{
          label:'Provincia:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true,
          options: vm.provincias,
          ngOptions:'provincia as provincia.nombre for provincia in to.options track by provincia.id'
        }
      },
      {
        key:'departamento',
        type:'horizontalSelect',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.provincia && vm.jerarquia.provincia.id && vm.jerarquia.provincia.tieneHijos);
        },
        templateOptions:{
          label:'Departamento/Partido:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true,
          options: vm.departamentos,
          ngOptions:'departamento as departamento.nombre for departamento in to.options track by departamento.id'
        }
      },
      {
        key:'nuevoDepartamento',
        type:'horizontalInput',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.departamento && !vm.jerarquia.departamento.id);
        },
        templateOptions:{
          label:'Nombre de Partido/Departamento:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true
        }
      },
      {
        key:'localidad',
        type:'horizontalSelect',
        model: vm.jerarquia,
        hideExpression: function() {
            return !(vm.jerarquia.departamento && vm.jerarquia.departamento.id && vm.jerarquia.departamento.tieneHijos);
        },
        templateOptions:{
          label:'Localidad:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true,
          options: vm.localidades,
          ngOptions:'localidad as localidad.nombre for localidad in to.options track by localidad.id'
        }
      },
      {
        key:'nuevaLocalidad',
        type:'horizontalInput',
        model: vm.jerarquia,
        hideExpression: function() {
          if (vm.jerarquia.pais && !vm.jerarquia.pais.id) return false;
          if (vm.jerarquia.provincia && !vm.jerarquia.provincia.id) return false;
          if (vm.jerarquia.departamento && !vm.jerarquia.departamento.id) return false;
          return !(vm.jerarquia.localidad && !vm.jerarquia.localidad.id);
        },
        templateOptions:{
          label:'Nombre de Localidad:',
          ancho:'col-sm-8',
          anchoLabel: 'col-sm-4',
          required: true
        }
      }
    ];

    $scope.$watch('vm.options.templateOptions.disabled', function(newValue){
      vm.campos.forEach(function(campo){
        if (!campo.templateOptions) {
          campo.templateOptions = {};
        }
        campo.templateOptions.disabled = newValue;
      });
    });



  }

})();

(function() {
	'use strict';

	angular.module('integragenerico')
	.directive('inputDireccion', inputDireccion);

	function inputDireccion() {
		return {
			restrict: 'E',
			templateUrl: 'integragenerico/components/formly/direccion/direccion.html',
			controller: 'InputDireccionCtrl as vm',
			bindToController: true,
			scope: {
				model: '=ngModel',
				options: '='
			}
		};
	}

})();

(function () {
  'use strict';

  angular.module('integragenerico')
    .controller('InputDireccionNuevaCtrl', InputDireccionNuevaCtrl);

  function InputDireccionNuevaCtrl($scope, orderByFilter, ServicioListasRelacionadas, TipoLugarGeografico) {

    var vm = this;

    vm.jerarquia = {};

    vm.titulo = 'Dirección Nuevo';

   
    // $scope.$watch(function () {
    //   return vm.model.lugar;
    // }, actualizarLugarSeleccionado);

    // $scope.$watch(function () {
    //   return vm.jerarquia.localidad;
    // }, actualizarLugarGeografico);



    function actualizarLugarGeografico() {
      if (!vm.model) return;
      if (!vm.model.lugar) {
        vm.model.lugar = {};
      }
      if (vm.jerarquia.localidad && vm.jerarquia.localidad.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.localidad.nombre;
        vm.model.lugar.lugar = vm.jerarquia.localidad;
        vm.model.lugar.tipo = vm.jerarquia.localidad.tipo;
        vm.model.lugar.padreOriginal = vm.jerarquia.localidad.padre;
        vm.model.lugar.padre = null;
      } else if (vm.jerarquia.departamento && vm.jerarquia.departamento.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = vm.jerarquia.departamento;
        vm.model.lugar.padre = null;
      } else if (vm.jerarquia.provincia && vm.jerarquia.provincia.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = null;
        vm.model.lugar.padre = {
          nombreLugar: vm.jerarquia.nuevoDepartamento,
          lugar: null,
          tipo: getTipo('Departamento'),
          padreOriginal: vm.jerarquia.provincia,
          padre: null
        };
      } else if (vm.jerarquia.pais && vm.jerarquia.pais.id) {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = vm.jerarquia.pais;
        vm.model.lugar.padre = null;
      } else {
        vm.model.lugar.nombreLugar = vm.jerarquia.nuevaLocalidad;
        vm.model.lugar.lugar = null;
        vm.model.lugar.tipo = getTipo('Localidad');
        vm.model.lugar.padreOriginal = null;
        vm.model.lugar.padre = {
          nombreLugar: vm.jerarquia.nuevoPais,
          lugar: null,
          tipo: getTipo('País'),
          padreOriginal: null,
          padre: null
        };
      }
    }

   

    vm.campos = [
      {
        key: 'calle',
        id: 'calle',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho: 'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Calle:',
          placeholder: 'Calle',
          requiredPadre: true
        }
      },
      {
        key: 'numero',
        id: 'numero',
        type: 'horizontalInput',
        templateOptions: {
          type: 'number',
          ancho: 'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Número:',
          placeholder: 'Número',
          requiredPadre: true
        }
      },
      {
        key: 'piso',
        id: 'piso',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho: 'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Piso:',
          placeholder: 'Piso'
        }
      },
      {
        key: 'departamento',
        id: 'departamento',
        type: 'horizontalInput',
        templateOptions: {
          type: 'text',
          ancho: 'col-sm-8',
          anchoLabel: 'col-sm-4',
          label: 'Departamento:',
          placeholder: 'Departamento'
        }
      }, {
        key: "localidad",
        id : "localidad",
        type: "horizontalTypeahead",
        data: {
          lista: "lazy",
          servicio: "Localidad"
        },
        templateOptions: {
          label: "Localidad:",
          placeholder: "",
          ancho: "col-sm-8",
          anchoLabel: "col-sm-4",
          propiedad: "nombreCompletoMostrar",
          propiedadFiltro: ["nombre", "codigoPostal"],
          ngOptions: "item as item[to.propiedad] for item in to.funcion($viewValue) | limitTo:8",
          espera: 300,
          permitirNuevo : false,
          asincrono: true,
          requiredPadre: true
        }
      }
    ];

    var formulario = {campos : vm.campos};
    ServicioListasRelacionadas.completarListasRelacionadas(formulario);

    $scope.$watch('vm.options.templateOptions.disabled', function (newValue) {
      vm.campos.forEach(function (campo) {
        if (!campo.templateOptions) {
          campo.templateOptions = {};
        }
        campo.templateOptions.disabled = newValue;
      });
    });

    $scope.$watch('vm.options.templateOptions.disabled', function (newValue) {
      vm.campos.forEach(function (campo) {
        if (!campo.templateOptions) {
          campo.templateOptions = {};
        }
        campo.templateOptions.disabled = newValue;
      });
    });

    $scope.$watch('vm.options.templateOptions.required', function (newValue) {
      vm.campos.forEach(function (campo) {
        if (campo.templateOptions && campo.templateOptions.requiredPadre) {
          campo.templateOptions.required = newValue;
        }
      });
    });



  }

})();

(function() {
	'use strict';

	angular.module('integragenerico')
	.directive('inputDireccionNueva', inputDireccionNueva);

	function inputDireccionNueva() {
		return {
			restrict: 'E',
			templateUrl: 'integragenerico/components/formly/direccion-nueva/direccion-nueva.html',
			controller: 'InputDireccionNuevaCtrl as vm',
			bindToController: true,
			scope: {
				model: '=ngModel',
				options: '='
			}
		};
	}

})();

(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
	angular.module('integragenerico')
	.directive('longitudTelefono', longitudTelefono);

	function longitudTelefono() {
		return {
        	restrict: 'A',
        	require: '^form',
        	link: function(scope, element, attrs, parentFormCtrl) {

        		var longitudTelefono

        		scope.$watch(attrs.longitudTelefono, function(newValue) {
        			longitudTelefono = newValue;
        			validar();
        		});

        		scope.$watch('vm.form.prefijo.$viewValue', function(newValue) {
        			validar();
        		})

        		scope.$watch('vm.form.sufijo.$viewValue', function(newValue) {
        			validar();
        		})

        		function validar() {
        			var validez = esValido();
        			if (parentFormCtrl.prefijo) {
        				parentFormCtrl.prefijo.$setValidity('longitudTelefono', validez);
        			}
        			if (parentFormCtrl.sufijo) {
        				parentFormCtrl.sufijo.$setValidity('longitudTelefono', validez);
        			}
        		}

        		function esValido() {
        			if (!scope.vm || !scope.vm.form || !scope.vm.form.prefijo || !scope.vm.form.sufijo) return true;
        			var longitudPrefijo = scope.vm.form.prefijo.$viewValue ? scope.vm.form.prefijo.$viewValue.length : 0;
        			var longitudSufijo = scope.vm.form.sufijo.$viewValue ? scope.vm.form.sufijo.$viewValue.length : 0;
        			return (longitudPrefijo + longitudSufijo === longitudTelefono) || scope.vm.form.prefijo.$pristine || scope.vm.form.sufijo.$pristine;
        		}
        	}
    	};
	}

})();

(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name bolsaEmpleo.controller:AboutCtrl
	* @description
	* # AboutCtrl
	* Controller of the bolsaEmpleo
	*/
	angular.module('integragenerico')
	.controller('InputTelefonoCtrl', InputTelefonoCtrl);

	function InputTelefonoCtrl($scope, orderByFilter, TipoTelefono, CodigoInternacional) {

		var vm = this;

		vm.seleccionarTipo = seleccionarTipo;

		var codigoInternacional;

		iniciar();

		$scope.$watch(getModelo, completarTelefono);

		$scope.$watch(getModelo, actualizarFormatos, true);

		function getModelo() {
			return vm.model;
		}

		function completarTelefono() {
			vm.telefono = vm.model;
			if (!vm.telefono) {
				vm.telefono = {};
				vm.model = vm.telefono;
			}
			completarTipoTelefono();
			completarCodigoInternacional();
		}

		function completarTipoTelefono() {
			if (!vm.telefono.tipo && vm.tipos) {
				seleccionarTipo(vm.tipos[0]);
			}
		}

		function completarCodigoInternacional() {
			if (!vm.telefono.codigoInternacional && codigoInternacional) {
				seleccionarCodigoInternacional(codigoInternacional);
			}
		}

		function actualizarFormatos() {
			if (vm.telefono && vm.telefono.tipo) {
				vm.telefono.formatoHumano =
				(vm.telefono.tipo.maximoPrefijo ? '('
				+ vm.telefono.tipo.prefijoHumano + vm.telefono.prefijo + ')' : '')
				+ vm.telefono.tipo.divisorHumano + vm.telefono.sufijo;
				vm.telefono.formatoInternacional = vm.telefono.tipo.prefijoInternacional
				+ vm.telefono.prefijo + vm.telefono.sufijo;
			}
		}

		function iniciar() {
			CodigoInternacional.query({codigo: '+54'},function(codigos) {
				codigoInternacional = codigos[0];
			});
			TipoTelefono.query(function(tipos) {
				vm.tipos = orderByFilter(tipos,'orden');
				completarTipoTelefono();
			});
		}

		function seleccionarTipo(tipo) {
			vm.telefono.tipo = tipo;
			vm.menuVisible = false;
		}

		function seleccionarCodigoInternacional(codigoInternacional) {
			vm.telefono.codigoInternacional = codigoInternacional;
		}

		vm.mostrarMenu = function() {
      vm.menuVisible = !vm.menuVisible;
    };

	}

})();

(function() {
	'use strict';

	/**
 	 * @ngdoc function
 	 * @name bolsaEmpleo.controller:AboutCtrl
 	 * @description
 	 * # AboutCtrl
 	 * Controller of the bolsaEmpleo
 	 */
	angular.module('integragenerico')
	.directive('inputTelefono', inputTelefono);

	function inputTelefono() {
		return {
			restrict: 'E',
			templateUrl: 'integragenerico/components/formly/telefono/telefono.html',
			controller: 'InputTelefonoCtrl as vm',
			bindToController: true,
			scope: {
				model: '=ngModel',
				options: '='
			}
		};
	}

})();

'use strict';

/**
 * @ngdoc function
 * @name bolsaEmpleo.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the bolsaEmpleo
 */
 angular.module('integragenerico')
 .directive('textoEstatico', function() {
 	return {
 		restrict: 'A',
 		link: function(scope, element, attributes) {
 			attributes.$set('ngBind', attributes.ngModel);
 		}
 	};
 });

'use strict';

 angular.module('integragenerico')
 .directive('celdaBoolean', function($compile) {
   return {
     restrict: 'E',
     replace: true,
     scope: true,
     templateUrl: 'integragenerico/components/lista-elementos/celda-boolean/celda-boolean.html',
     cotrollerAs: 'vm',
     controller: function($scope, $parse) {
       var vm = this;
       $scope.valor = $parse($scope.col.field)($scope.row);
     }
   };
 });

'use strict';

 angular.module('integragenerico')
 .directive('celdaComentario', function($compile) {
   return {
     restrict: 'E',
     replace: true,
     scope: true,
     templateUrl: 'integragenerico/components/lista-elementos/celda-comentario/celda-comentario.html',
     cotrollerAs: 'vm',
     controller: function($scope, $parse) {
       var vm = this;
       $scope.valor = $parse($scope.col.field)($scope.row);
     }
   };
 });

'use strict';

 angular.module('integragenerico')
 .directive('celdaMultiplesFilas', function() {
   return {
     restrict: 'E',
     replace: true,
     scope: true,
     templateUrl: 'integragenerico/components/lista-elementos/celda-multiples-filas/celda-multiples-filas.html',
     cotrollerAs: 'vm',
     controller: function($scope, $parse) {
       var vm = this;
       $scope.valor = $parse($scope.col.field)($scope.row);
       $scope.propiedad = $scope.col.propiedad;
     }
   };
 });

'use strict';

angular.module('integragenerico')
.directive('celdaSeleccion', function($compile) {
 return {
   restrict: 'E',
   replace: true,
   scope: true,
   templateUrl: 'integragenerico/components/lista-elementos/celda-seleccion/celda-seleccion.html',
   cotrollerAs: 'vm',
   controller: function($scope, $parse) {
     var vm = this;
     $scope.valor = $parse($scope.col.field)($scope.row);
     $scope.mostrar = mostrar;

     function obtenerValor() {
      return $parse($scope.col.field)($scope.row);
    }

    var parseado = $parse($scope.col.mostrar ? $scope.col.mostrar : 'true');

    function mostrar() {
      return parseado($scope);
    }

    $scope.$watch(function() {
      return obtenerValor();
    }, function(newValue, oldValue) {
      $scope.valor = newValue;
    });

    $scope.$watch(function(){
      return $scope.valor;
    }, function(newValue, oldValue) {
      $scope.row[$scope.col.field] = newValue;
    });
  }
};
});

'use strict';

/**
* @ngdoc function
* @name bolsaEmpleo.controller:DatosDemandanteCtrl
* @description
* # DatosDemandanteCtrl
* Controller of the bolsaEmpleo
*/
angular.module('integragenerico.seguridad')
.controller('CambiarClaveCtrl', function ($scope, $modalInstance, Usuario, Dialogos, objetoModificando) {
	var vm = $scope;

	vm.clave = {};

	vm.objetoModificando = objetoModificando;

	vm.camposCambiarClave =[
	{
		key:"anterior",
		type:"input",
		className:'col-md-6 col-md-offset-3',
		templateOptions:{
			type:"password",
			label:"Contraseña Anterior:",
			placeholder:"Ingrese su anterior contraseña",
			required:true,
			maxLength:20,
			minlength:8
		}
	},
	{
		key:"clave",
		type:"input",
		className:'col-md-6 col-md-offset-3',
		templateOptions:{
			type:"password",
			label:"Nueva Contraseña:",
			placeholder:"Ingrese su nueva contraseña",
			required:true,
			maxLength:20,
			minlength:8
		}
	},
	{
		key:'repetida',
		type:'input',
		className:'col-md-6 col-md-offset-3',
		validators:{
			passwordIguales:{
				expression:function($viewValue, $modelValue, scope){
					var valor=($viewValue || $modelValue);
					var valor2=scope.model.clave;
					if(valor){
						return compararPassword(valor,valor2);
					}else{
						return true;
					}
				},
				message: '"las contraseñas deben ser iguales"'
			},
		},

		extras:{
			validateOnModelChange:true
		},

		templateOptions:{
			type:"password",
			label:"Repita contraseña:",
			placeholder:"Ingrese nuevamente su contraseña",
			required:true,
			maxLength:20,
			minlength:8
		}
	}
	];

	vm.cambiarClaves=function(form){
		// vm.clave.id = vm.objetoModificando.id;

		Usuario.cambiarClave(vm.clave,function(){
			Dialogos.abrirDialogoInformacion({textoDinamico:'Su contraseña fue actualizada correctamente'});
			$modalInstance.close();
		},function(error){
			console.log(error);
			Dialogos.abrirDialogoInformacion({textoDinamico:error.data.message});
		});
	};

	var compararPassword=function(valor1,valor2){
		if(valor1 !== valor2){
			return false;
		}else{
			return true;
		}
	};


	vm.cancelar = function () {
		$modalInstance.dismiss('Cancelado');
	};

});
